package ch.unibnf.ase.bananasports.test;

import android.test.AndroidTestCase;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.twitter.TwitterHelper;

public class TwitterTest extends AndroidTestCase {
	public void test001() {
		String[] result = new String[2];
		// TwitterHelper.authorize(getContext(), result);
		System.out.println(result[0]);
		System.out.println(result[1]);
	}

	public void test002() {
		BananaDatabase db = new BananaDatabase(this.getContext());
		Settings settings = new SettingsDAO(db).getSettings();
		db.close();

		assertTrue(TwitterHelper.tweet(
				settings.getValue(Key.TWITTER_AUTHTOKEN),
				settings.getValue(Key.TWITTER_AUTHTOKENKEY),
				"bananasports, weeehaaa"));
	}
}
