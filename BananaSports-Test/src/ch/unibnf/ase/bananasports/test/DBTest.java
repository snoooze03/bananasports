package ch.unibnf.ase.bananasports.test;

import java.util.List;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;

public class DBTest extends AndroidTestCase {

	@SmallTest
	public void test01() {
		BananaDatabase db = new BananaDatabase(getContext());
		EntryFileDAO efDAO = new EntryFileDAO(db);
		List<EntryFile> l = efDAO.GetEntryFiles();
		EntryFile ef1 = new EntryFile();
		ef1.gpx = "test";
		ef1.serverid = 10;
		ef1.sport = "Running";
		ef1.username = "blubb";
		efDAO.Insert(ef1);

		EntryFile ef2 = new EntryFile();
		ef2.gpx = "test2";
		ef2.serverid = 1000;
		ef2.sport = "Running";
		ef2.username = "blubb";
		efDAO.Insert(ef2);

		ef1.serverid = 1001;
		efDAO.Update(ef1);

		List<EntryFile> l2 = efDAO.GetEntryFiles();
		Log.v("entries", l2.size() + "");
		EntryFile ef3 = l2.get(l2.size() - 2);
		EntryFile ef4 = l2.get(l2.size() - 1);
		efDAO.LoadTrack(ef3);
		efDAO.LoadTrack(ef4);
		Log.v("gpx ", ef3.gpx);

		assertEquals("blubb", ef3.username);
		assertEquals(1001, ef3.serverid);
		assertEquals("test", ef3.gpx);

		assertEquals("test2", ef4.gpx);
		assertEquals(1000, ef4.serverid);
		assertEquals("blubb", ef4.username);
	}

	@SmallTest
	public void test02() {
		// tests analysisentry
		AnalysisEntry e = new AnalysisEntry();
		e.averagepulse = 175;
		e.distance = 10000;
		e.duration = 8888;
		e.elevationgain = 100;
		e.elevationloss = 99;
		e.idfile = 1;
		e.time = 999;
		e.username = "marc";
		BananaDatabase db = new BananaDatabase(getContext());
		AnalysisEntryDAO dao = new AnalysisEntryDAO(db);
		dao.Insert(e);

		List<AnalysisEntry> l = dao.GetEntryFiles();
		AnalysisEntry e2 = l.get(l.size() - 1);
		assertEquals(175, e2.averagepulse);
		assertEquals(10000, e2.distance);
		assertEquals(8888, e2.duration);
		assertEquals(100, e2.elevationgain);
		assertEquals(99, e2.elevationloss);
		assertEquals(1, e2.idfile);
		assertEquals(999, e2.time);
		assertEquals("marc", e2.username);
	}

	@SmallTest
	public void test03() {
		assertEquals(true, true);
	}

	public void test04() {
		SettingsDAO dao = new SettingsDAO(new BananaDatabase(getContext()));

		Settings settings = Settings.getInstance();
		settings.put("key1", "value1");
		settings.put("key2", "value2");

		dao.Save(settings);

		settings.put("key1", "newvalue1");

		settings = dao.getSettings();

		assertFalse("newvalue1".equals(settings.getValue("key1")));
		assertEquals("value2", settings.getValue("key2"));
	}

	public void test05() {
		// update settings test
		Settings settings = Settings.getInstance();
		settings.put("key05-1", "oldvalue");
		SettingsDAO dao = new SettingsDAO(new BananaDatabase(getContext()));

		dao.Save(settings);

		settings.put("key05-1", "newvalue");
		dao.Save(settings);

		settings = dao.getSettings();
		assertEquals("newvalue", settings.getValue("key05-1"));
	}
}
