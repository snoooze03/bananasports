package mergestuff.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import ch.unibnf.ase.bananasports.MainActivity;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;

/**
 * This class gets the intent of a associated file, then saves it and closes
 * right again.
 * 
 * @author wiedmer
 * 
 */
public class GPXSaveActivity extends Activity {
	private void saveFile(final boolean startMain) {
		final LoadingDialog dialog = new LoadingDialog();
		dialog.show(this.getFragmentManager(), "loading-gpxfile-from-intent");
		new Thread() {
			@Override
			public void run() {
				String filePath = GPXSaveActivity.this.getIntent().getData()
						.getEncodedPath();
				String scheme = GPXSaveActivity.this.getIntent().getData()
						.getScheme();
				Log.v("gpxsave", "scheme=" + scheme);

				int index = filePath.lastIndexOf("/");
				String filename = null;
				if (index != -1) {
					filename = filePath.substring(index + 1);
					if (!filename.endsWith(".gpx")) {
						filename += ".gpx";
					}
				}
				if (filename != null) {

					Log.v("GPXSaveActivity", filePath);

					try {
						InputStream is = null;
						if ("http".equals(scheme)) {
							is = new URL(GPXSaveActivity.this.getIntent()
									.getData().toString()).openStream();
						} else if ("file".equals(scheme)) {
							is = new FileInputStream(new File(filePath));
						}
						FileOutputStream fos = new FileOutputStream(
								GPXSaveActivity.this.getFilesDir()
										.getAbsolutePath() + "/" + filename);
						byte[] buffer = new byte[1024];
						int readBytes;
						while (0 <= (readBytes = is.read(buffer))) {
							fos.write(buffer, 0, readBytes);
						}
						fos.flush();
						fos.close();
						is.close();

						Log.v("GPXSaveActivity", "file imported");
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				GPXSaveActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						dialog.dismiss();
						GPXSaveActivity.this.finish();
						if (startMain) {
							Intent i = new Intent(GPXSaveActivity.this
									.getApplicationContext(),
									MainActivity.class);
							GPXSaveActivity.this.startActivity(i);
						}
					}
				});
			};
		}.start();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setCancelable(false);
		// Get the layout inflater
		builder.setTitle("Import File: " + this.getIntent().getDataString())
				.setMessage(
						"Do you want to import File '"
								+ this.getIntent().getData()
										.getLastPathSegment() + "'?");
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				GPXSaveActivity.this.finish();
				dialog.dismiss();
			}
		})
				.setNeutralButton("Yes & Close",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								GPXSaveActivity.this.saveFile(false);
								dialog.dismiss();
							}
						})
				.setPositiveButton("Import & Start App",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								GPXSaveActivity.this.saveFile(true);
								dialog.dismiss();
							}
						});
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}
}
