package old;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import ch.unibnf.ase.bananasports.R;
import ch.unibnf.ase.bananasports.Tab1Fragment;
import ch.unibnf.ase.bananasports.TrackActivity;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.dialog.SignInDialogFragment;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.gpx.provider.GPXDummyProvider;

public class OldMainWindow extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			TextView textView = new TextView(this.getActivity());
			textView.setGravity(Gravity.CENTER);
			Bundle args = this.getArguments();
			textView.setText(Integer.toString(args.getInt(ARG_SECTION_NUMBER)));
			return textView;
		}
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the primary sections of the app.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public int getCount() {
			return 1;
		}

		@Override
		public Fragment getItem(int i) {
			Fragment fragment;
			if (i == 0) {
				fragment = new Tab1Fragment();
			} else {
				return null;
			}

			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return "FUCK YOU ANDROID!";
			}
			return null;
		}
	}

	public static final String GPXFILE = "GPXFile";

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	private SignInDialogFragment signInDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		BananaDatabase bananaDatabase = new BananaDatabase(this);

		// load Settings
		new SettingsDAO(bananaDatabase).getSettings();

		bananaDatabase.close();

		this.setContentView(R.layout.oldmain);
		// Create the adapter that will return a fragment for each of the three
		// primary sections
		// of the app.
		this.mSectionsPagerAdapter = new SectionsPagerAdapter(
				this.getSupportFragmentManager());

		// Set up the action bar.
		final ActionBar actionBar = this.getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Set up the ViewPager with the sections adapter.
		this.mViewPager = (ViewPager) this.findViewById(R.id.pager);
		this.mViewPager.setAdapter(this.mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab.
		// We can also use ActionBar.Tab#select() to do this if we have a
		// reference to the
		// Tab.
		this.mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		// for (int i = 0; i < this.mSectionsPagerAdapter.getCount(); i++) {
		// // Create a tab with text corresponding to the page title defined by
		// // the adapter.
		// // Also specify this Activity object, which implements the
		// // TabListener interface, as the
		// // listener for when this tab is selected.
		// actionBar.addTab(actionBar.newTab()
		// .setText(this.mSectionsPagerAdapter.getPageTitle(i))
		// .setTabListener(this));
		// }

		if (this.signInDialog == null) {
			// check database if
			boolean loggedOn = false;
			if ("true".equals(Settings.getInstance().getValue(
					Settings.Key.KEEPSIGNEDIN))) {
				Log.v("MainActivity", "sign in data found");
				String username = Settings.getInstance().getValue(Key.USERNAME);
				String password = Settings.getInstance().getValue(Key.PASSWORD);
				if (username != null && password != null) {
					User user = new User(username);
					user.setPassword(password);
					loggedOn = RESTConnector.logonUser(user, 3000);
				}
			}
			if (!loggedOn) {
				this.signInDialog = new SignInDialogFragment();
				this.signInDialog.show(this.getFragmentManager(),
						"SignInDialog");
				this.signInDialog.setFeedback("Please log in");
			} else {
				Log.v("MainActivity", "logon successful");
			}
		}
	}

	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		this.mViewPager.setCurrentItem(tab.getPosition());
	}

	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	public void startMapActivityClick(View view) {
		Intent i = new Intent(this.getApplication(), TrackActivity.class);
		i.putExtra(GPXFILE, new GPXDummyProvider().load());
		this.startActivity(i);
	}

	// ---------- tab1FragmentToastButton_Click
	public void tab1FragmentToastButton_Click(View view) {
		EditText tv = (EditText) this.findViewById(R.id.editText1);
		if (tv != null) {
			tv.setText(Math.random() + "");
			Toast.makeText(view.getContext(), tv.getText().toString(),
					Toast.LENGTH_LONG);
		} else {
			Toast.makeText(view.getContext(),
					"foobar toast button pressed, haha...", Toast.LENGTH_SHORT)
					.show();
		}
	}
}
