package ch.unibnf.ase.bananasports;

import android.view.View;
import android.widget.BaseExpandableListAdapter;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class DeletePartnershipClickListener extends DialogOnClickListener {

	private static final String TITLE = "Delete Partnership";
	private static final String USER_QUERY = "Do you really want to delete the Partnership with %s?";
	private static final String CONFIRM_TEXT = "Delete";

	public DeletePartnershipClickListener(Partnership p,
			BaseExpandableListAdapter a, int groupId, View view) {
		super(p, a, groupId, view);
	}

	public void onClick(View v) {
		Partnership p = (Partnership) this.entity;
		String other = p.isOwnerLocal() ? p.getPartner() : p.getOwner();
		String query = String.format(USER_QUERY, other);
		this.createDialog(TITLE, query, CONFIRM_TEXT, v);
	}

	@Override
	protected void onConfirm() {
		Partnership p = (Partnership) this.entity;
		User user = (User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER);
		if (p.isOwnerLocal()) {
			RESTConnector.deletePartnershipOwner(user, p.getPartner(), 3000);
			p.setOwnerConfirmed(false);
		} else {
			RESTConnector
					.deletePartnershipParticipant(user, p.getOwner(), 3000);
			p.setPartnerConfirmed(false);
		}
		if (!p.hasOwnerConfirmed() && !p.hasPartnerConfirmed()) {
			((PartnershipsAdatper) this.adapter)
					.removePartnership(this.groupId);
		}

		this.adapter.notifyDataSetChanged();
	}
}
