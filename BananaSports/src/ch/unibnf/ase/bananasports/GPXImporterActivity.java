package ch.unibnf.ase.bananasports;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;
import ch.unibnf.ase.bananasports.gpx.GPXDOMParser;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;
import ch.unibnf.ase.bananasports.util.SaveAnalyzeHelper;
import ch.unibnf.ase.bananasports.util.SaveAnalyzeHelper.Sport;

public class GPXImporterActivity extends Activity {

	/** Start this activity with a GPX_IMPORT intent with gpx raw data */
	public static final String GPX_IMPORT = "gpximporter_gpxstring";
	/**
	 * The sport which the gpxstring corresponds to. Use true for running, false
	 * for cycling
	 */
	public static final String GPX_IMPORT_SPORT = "gpximporter_sport";

	private void getSportAndSaveFile(final boolean save) {
		if (save) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setCancelable(false);
			// Get the layout inflater
			builder.setTitle("Sport?")
					.setMessage("Is this running or Cycling?");
			builder.setNeutralButton("Running",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							GPXImporterActivity.this.saveFile(true,
									Sport.Running);
							dialog.dismiss();
						}
					}).setPositiveButton("Cycling",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							GPXImporterActivity.this.saveFile(true,
									Sport.Cycling);
							dialog.dismiss();
						}
					});

			Dialog dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		} else {
			this.saveFile(false, null);
		}
	}

	/**
	 * Handles the case if activity was started by file association
	 */
	private void importFromAction() {
		// Get dialog and ask user to import, or view
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setCancelable(false);
		// Get the layout inflater
		builder.setTitle("Import File: " + this.getIntent().getDataString())
				.setMessage(
						"View / Import GPX File '"
								+ this.getIntent().getData()
										.getLastPathSegment() + "'?");
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						GPXImporterActivity.this.finish();
						dialog.dismiss();
					}
				})
				.setNeutralButton("View only",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								GPXImporterActivity.this
										.getSportAndSaveFile(false);
								dialog.dismiss();
							}
						})
				.setPositiveButton("View & Import file",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								GPXImporterActivity.this
										.getSportAndSaveFile(true);
								dialog.dismiss();
							}
						});

		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	/**
	 * Handles the gpx string
	 * 
	 * @param sport
	 */
	private void importFromActivity(final String gpxString, final Sport sport) {
		final LoadingDialog dialog = new LoadingDialog();
		dialog.show(this.getFragmentManager(), "importing gpxstring");

		new Thread() {
			@Override
			public void run() {

				SaveAnalyzeHelper.save(GPXImporterActivity.this, sport,
						gpxString, true);
				GPXFile gpxFile = GPXDOMParser.parseGPX(
						"track_"
								+ new ISO8601TimeHelper()
										.getStringThreadSafe(System
												.currentTimeMillis()),
						gpxString);

				final Intent i = new Intent(
						GPXImporterActivity.this.getApplication(),
						AnalysisActivity.class);
				i.putExtra(AnalysisActivity.GPXFILE, gpxFile);

				GPXImporterActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						dialog.dismiss();
						GPXImporterActivity.this.startActivity(i);
						GPXImporterActivity.this.finish();
					}
				});
			};
		}.start();

	}

	/**
	 * This method saves a track if argument is true, otherwise only parses and
	 * shows analysis activity
	 * 
	 * @param save
	 * @param sport
	 *            will only be considere if save==true
	 */
	private void saveFile(final boolean save, final Sport sport) {
		final LoadingDialog dialog = new LoadingDialog();
		dialog.show(this.getFragmentManager(), "loading-gpxfile-from-intent");
		new Thread() {

			private String xml = "";
			private String filename;

			@Override
			public void run() {
				String filePath = GPXImporterActivity.this.getIntent()
						.getData().getEncodedPath();
				String scheme = GPXImporterActivity.this.getIntent().getData()
						.getScheme();
				Log.v("gpxsave", "scheme=" + scheme);

				int index = filePath.lastIndexOf("/");
				this.filename = null;
				if (index != -1) {
					this.filename = filePath.substring(index + 1);
					if (!this.filename.endsWith(".gpx")) {
						this.filename += ".gpx";
					}
				}
				if (this.filename != null) {

					Log.v("GPXAssociationActivity", filePath);

					try {
						InputStream is = null;
						if ("http".equals(scheme)) {
							is = new URL(GPXImporterActivity.this.getIntent()
									.getData().toString()).openStream();
						} else if ("file".equals(scheme)) {
							is = new FileInputStream(new File(filePath));
						}

						// wiedmer: use String Buffer here,
						// bytearrayoutputstream.toArray may lead to
						// outofmemoryexception in android..
						StringBuffer sb = new StringBuffer();
						byte[] buf = new byte[4096];
						for (int n; 0 < (n = is.read(buf));) {
							sb.append(new String(buf, 0, n));
						}
						this.xml = sb.toString();

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				final GPXFile f = GPXDOMParser
						.parseGPX(this.filename, this.xml);

				if (save) {
					SaveAnalyzeHelper.save(
							GPXImporterActivity.this.getApplicationContext(),
							sport, this.xml, true);
				}

				final Intent i = new Intent(
						GPXImporterActivity.this.getApplicationContext(),
						AnalysisActivity.class);
				if (!this.xml.isEmpty()) {
					i.putExtra(AnalysisActivity.GPXFILE, f);
				}

				GPXImporterActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						dialog.dismiss();
						GPXImporterActivity.this.startActivity(i);
					}
				});
			};
		}.start();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Import has two cases:
		// 1) File over android.intent.action.VIEW
		// 2) gpx raw string over Intent

		Log.v("gpxSave", "Starting gpx save dialog");

		// if it is from file: VIEW
		String path = this.getIntent().getDataString();
		if (path != null) {
			Log.v("GPXImporterActivity", "Data from action.VIEW");
			this.importFromAction();
		}

		String gpxString = this.getIntent().getExtras().getString(GPX_IMPORT);
		SaveAnalyzeHelper.Sport sport = (this.getIntent().getExtras()
				.getBoolean(GPX_IMPORT_SPORT) ? Sport.Running : Sport.Cycling);
		if (gpxString != null) {
			Log.v("GPXImporterActivity",
					"Data from Activity: " + sport.getText());
			this.importFromActivity(gpxString, sport);
		}
	}

}
