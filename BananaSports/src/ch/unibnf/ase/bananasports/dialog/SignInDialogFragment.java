package ch.unibnf.ase.bananasports.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.R;
import ch.unibnf.ase.bananasports.connector.HTTPResponseCode;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class SignInDialogFragment extends DialogFragment {
	private View thisView;
	private String feedbackText;

	private boolean register = false;
	private String username;
	private String realname;
	private String email;
	private boolean keepSignedIn;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				this.getActivity());
		builder.setCancelable(false);
		// Get the layout inflater
		LayoutInflater inflater = this.getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		this.thisView = inflater.inflate(R.layout.dialog_signin, null);
		builder.setView(this.thisView)
		// Add action buttons
				.setPositiveButton(this.getString(R.string.signin),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								SignInDialogFragment.this.userSignin();
							}
						});
		builder.setNeutralButton(this.getString(R.string.createaccount),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (SignInDialogFragment.this.register) {
							SignInDialogFragment.this.userRegister();
						} else {
							SignInDialogFragment dial = new SignInDialogFragment();
							dial.setFeedback("Please enter all fields");
							dial.setRegister();
							dial.show(SignInDialogFragment.this
									.getFragmentManager(), "register");
						}
					}
				});
		// set value if available
		if (this.keepSignedIn) {
			((CheckBox) SignInDialogFragment.this.thisView
					.findViewById(R.id.signin_keepsignedin)).setChecked(true);
		}
		if (this.username != null) {
			((EditText) SignInDialogFragment.this.thisView
					.findViewById(R.id.signin_username)).setText(this.username);
		}
		if (this.realname != null) {
			((EditText) SignInDialogFragment.this.thisView
					.findViewById(R.id.signin_realname)).setText(this.realname);
		}
		if (this.email != null) {
			((EditText) SignInDialogFragment.this.thisView
					.findViewById(R.id.signin_email)).setText(this.email);
		}
		if (!this.register) {
			this.thisView.findViewById(R.id.signin_email).setVisibility(
					View.GONE);
			this.thisView.findViewById(R.id.signin_realname).setVisibility(
					View.GONE);
		}

		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		((TextView) this.thisView.findViewById(R.id.signin_feedback))
				.setText(this.feedbackText);
		return dialog;
	}

	public void setFeedback(String feedback) {
		this.feedbackText = feedback;
	}

	public void setRegister() {
		this.register = true;
	}

	public void setValues(String username, String realname, String email,
			boolean keepSignedIn) {
		this.username = username;
		this.realname = realname;
		this.email = email;
		this.keepSignedIn = keepSignedIn;
	}

	private void userRegister() {
		Log.v("SignIn", "create account");
		String username = ((EditText) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_username)).getText().toString();
		String password = ((EditText) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_password)).getText().toString();
		String email = ((EditText) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_email)).getText().toString();
		String realname = ((EditText) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_realname)).getText().toString();
		boolean keepSignedIn = ((CheckBox) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_keepsignedin)).isChecked();
		User user = new User(username);
		user.setPassword(password);
		user.setEmail(email);
		user.setRealname(realname);
		HTTPResponseCode response = RESTConnector.register(user, 3000);
		if (!HTTPResponseCode.CREATED.equals(response)) {
			SignInDialogFragment dial = new SignInDialogFragment();
			dial.setFeedback("invalid data");
			dial.setRegister();
			dial.setValues(username, realname, email, keepSignedIn);
			dial.show(this.getFragmentManager(), "register");
		} else {
			HoneyPot.setItem(HoneyPotKey.CURRENT_USER, user);
			Log.v("UserRegister", user.toString());

			// store information
			Settings.getInstance().put(Settings.Key.KEEPSIGNEDIN,
					"" + keepSignedIn);
			Settings.getInstance().put(Settings.Key.USERNAME, username);
			Settings.getInstance().put(Settings.Key.PASSWORD, password);
			BananaDatabase db = new BananaDatabase(this.getActivity());
			new SettingsDAO(db).Save(Settings.getInstance());
			db.close();

		}
	}

	private void userSignin() {
		// sign in the user ...
		Log.v("SignIn", "signin");
		String username = ((EditText) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_username)).getText().toString();
		String password = ((EditText) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_password)).getText().toString();
		boolean keepSignedIn = ((CheckBox) SignInDialogFragment.this.thisView
				.findViewById(R.id.signin_keepsignedin)).isChecked();
		User user = new User(username);
		user.setPassword(password);
		boolean loggedOn = RESTConnector.logonUser(user, 3000);
		SignInDialogFragment.this.dismiss();

		Log.v("SignInDialog", "loggedin: " + loggedOn);
		if (!loggedOn) {
			SignInDialogFragment d = new SignInDialogFragment();
			d.setFeedback("Logon failed");
			d.setValues(username, null, null, loggedOn);
			d.show(this.getFragmentManager(), "retryLogon");
		} else {
			HoneyPot.setItem(HoneyPotKey.CURRENT_USER, user);
			// store information
			Settings.getInstance().put(Settings.Key.KEEPSIGNEDIN,
					"" + keepSignedIn);
			Settings.getInstance().put(Settings.Key.USERNAME, username);
			Settings.getInstance().put(Settings.Key.PASSWORD, password);
			BananaDatabase db = new BananaDatabase(this.getActivity());
			new SettingsDAO(db).Save(Settings.getInstance());
			db.close();
		}
	}
}
