package ch.unibnf.ase.bananasports.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import ch.unibnf.ase.bananasports.R;

public class LoadingDialog extends DialogFragment {
	private View thisView;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				this.getActivity());
		builder.setCancelable(false);
		// Get the layout inflater
		LayoutInflater inflater = this.getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		this.thisView = inflater.inflate(R.layout.dialog_loading, null);
		builder.setView(this.thisView);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		return dialog;
	}
}
