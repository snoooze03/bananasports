package ch.unibnf.ase.bananasports;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.connector.SynchronizeHelper;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;
import ch.unibnf.ase.bananasports.dialog.SignInDialogFragment;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.expandableList.TrackListActivity;
import ch.unibnf.ase.bananasports.gpx.provider.GPXDummyProvider;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class MainActivity extends Activity {

	private SignInDialogFragment signInDialog;

	public void analysebuttonClick(View view) {
		Intent i = new Intent(this.getApplicationContext(),
				AnalysisActivity.class);
		i.putExtra(TrackActivity.GPXFILE, new GPXDummyProvider().load());
		this.startActivity(i);
	}

	public void designbuttonClick(View view) {
		Intent i = new Intent(this.getApplicationContext(),
				DesignTrackActivity.class);
		this.startActivity(i);
	}

	public void fragmentbuttonClick(View view) {
		Intent i = new Intent(MainActivity.this.getBaseContext(),
				TrackListActivity.class);
		MainActivity.this.startActivity(i);
	}

	public void main_testbuttonClick(View view) {
		Intent i = new Intent(this.getApplicationContext(),
				GPXImporterActivity.class);
		i.putExtra(GPXImporterActivity.GPX_IMPORT, "test");
		this.startActivity(i);
		// TODO remove
	}

	public void mybananasbuttonClick(View view) {
		Intent i = new Intent(MainActivity.this.getBaseContext(),
				BananaScreenActivity.class);
		MainActivity.this.startActivity(i);
	}

	public void myprogressbuttonClick(View view) {
		Intent i = new Intent(this.getApplication(), MyProgressActivity.class);
		this.startActivity(i);
	}

	public void mytracksbuttonClick(View view) {
		Intent i = new Intent(MainActivity.this.getApplicationContext(),
				TrackListActivity.class);
		MainActivity.this.startActivity(i);
	}

	public void myuserbuttonClick(View view) {
		Intent i = new Intent(this.getApplicationContext(),
				MyUserActivity.class);
		this.startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.getMenuInflater().inflate(R.menu.settings_menu, menu);
		return true;
	}

	// android bug:
	// public boolean myusersettingsMenuItemClicked(MenuItem menuItem) {
	// Intent i = new Intent(this.getApplicationContext(),
	// MyUserActivity.class);
	// this.startActivity(i);
	// return true;
	// }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.myuserSettings) {
			Intent i = new Intent(this.getApplicationContext(),
					MyUserActivity.class);
			this.startActivity(i);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void socialbuttonClick(View view) {
		Intent i = new Intent(this.getApplicationContext(),
				SocialActivity.class);
		this.startActivity(i);
	}

	public void syncbuttonClick(View view) {
		// SYNC
		final LoadingDialog dialog = new LoadingDialog();
		dialog.show(this.getFragmentManager(), "sync loading");
		new Thread() {
			@Override
			public void run() {
				SynchronizeHelper.synchronizeCurrentUserTracks(
						(User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER),
						MainActivity.this.getApplicationContext(), true);
				MainActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						dialog.dismiss();
					}
				});
			};
		}.start();
	}

	public void workoutmodebuttonClick(View view) {
		Intent i = new Intent(this.getApplicationContext(),
				WorkoutActivity.class);
		i.putExtra(WorkoutActivity.GPXFILE, new GPXDummyProvider().load());
		this.startActivity(i);
	}

	private void logon() {
		if (this.signInDialog == null) {

			BananaDatabase bananaDatabase = new BananaDatabase(this);

			// load Settings
			new SettingsDAO(bananaDatabase).getSettings();

			bananaDatabase.close();

			// check database if
			boolean loggedOn = false;
			if ("true".equals(Settings.getInstance().getValue(
					Settings.Key.KEEPSIGNEDIN))) {
				Log.v("MainActivity", "sign in data found");
				String username = Settings.getInstance().getValue(Key.USERNAME);
				String password = Settings.getInstance().getValue(Key.PASSWORD);
				if (username != null && password != null) {
					User user = new User(username);
					user.setPassword(password);
					loggedOn = RESTConnector.logonUser(user, 3000);
					HoneyPot.setItem(HoneyPotKey.CURRENT_USER, user);
					// offline logon if possible..
					// loggedOn = true;
				}
			}
			if (!loggedOn) {
				this.signInDialog = new SignInDialogFragment();
				this.signInDialog.show(this.getFragmentManager(),
						"SignInDialog");
				this.signInDialog.setFeedback("Please log in");
			} else {
				Log.v("MainActivity", "logon successful");
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.table_layout);

		this.logon();
	}
}
