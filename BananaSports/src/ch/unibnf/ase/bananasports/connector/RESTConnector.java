package ch.unibnf.ase.bananasports.connector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;
import ch.unibnf.ase.bananasports.EntryTimeComparator;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.CyclingEntry;
import ch.unibnf.ase.bananasports.entities.CyclingSubscription;
import ch.unibnf.ase.bananasports.entities.EntityParserFactory;
import ch.unibnf.ase.bananasports.entities.Entry;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.RunningEntry;
import ch.unibnf.ase.bananasports.entities.RunningSubscription;
import ch.unibnf.ase.bananasports.entities.Subscription;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class RESTConnector {

	private static final String SERVER_ADRESS = "http://diufvm31.unifr.ch:8090/CyberCoachServer/resources/";

	private static final String USERS_RESOURCE = "users/";

	private static final String PARTNERSHIPS_SUBURL = "/CyberCoachServer/resources/partnerships/";

	private static final String PARTNERSHIPS_RESOURCE = "partnerships/";

	private static Context context;

	private static ProgressBar progressBar;

	public static HTTPResponseCode confirmPartnership(User participant,
			String ownername, long timeout) {
		try {
			String[] xml = new String[1];
			PUTTask pt = (PUTTask) new PUTTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + ownername + ";"
					+ participant.getUsername(), participant.getUsername(),
					participant.getPassword(), xml).execute(new Partnership()
					.getXML());
			pt.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode createEntry(Partnership partnership,
			User user, Entry entry, long timeout) {
		String[] xml = new String[1];
		try {
			POSTTask pt = (POSTTask) new POSTTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + partnership.getOwner() + ";"
					+ partnership.getPartner() + "/" + entry.getSport() + "/",
					user.getUsername(), user.getPassword(), xml).execute(entry
					.getXML());
			// wait for the result
			pt.get(timeout, TimeUnit.MILLISECONDS);
			String id = pt.getLocationHeaderField().substring(0,
					pt.getLocationHeaderField().length() - 1);
			id = id.substring(id.lastIndexOf("/") + 1);
			entry.serverID = Integer.parseInt(id);
			Log.v("createEntry", id + "   " + pt.getLocationHeaderField());
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode createEntry(User user, Entry entry,
			long timeout) {
		String[] xml = new String[1];
		try {
			POSTTask pt = (POSTTask) new POSTTask(SERVER_ADRESS
					+ USERS_RESOURCE + user.getUsername() + "/"
					+ entry.getSport() + "/", user.getUsername(),
					user.getPassword(), xml).execute(entry.getXML());
			// wait for the result
			pt.get(timeout, TimeUnit.MILLISECONDS);
			String id = pt.getLocationHeaderField().substring(0,
					pt.getLocationHeaderField().length() - 1);
			id = id.substring(id.lastIndexOf("/") + 1);
			entry.serverID = Integer.parseInt(id);
			Log.v("createEntry", id + "   " + pt.getLocationHeaderField());
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode createPartnership(User owner,
			String participantname, long timeout) {
		try {
			String[] xml = new String[1];
			PUTTask pt = (PUTTask) new PUTTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + owner.getUsername() + ";"
					+ participantname, owner.getUsername(),
					owner.getPassword(), xml).execute(new Partnership()
					.getXML());
			pt.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	/**
	 * 
	 * @param user
	 * @param password
	 * @param sport
	 * @param publicvisible
	 */
	public static HTTPResponseCode createSubscription(User user,
			Subscription subscription, long timeout) {
		try {
			PUTTask pt = (PUTTask) new PUTTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername() + "/" + subscription.getSport(),
					user.getUsername(), user.getPassword(), new String[1])
					.execute(subscription.getXML());
			// wait for result
			pt.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode createSubscriptionForPartnership(
			Partnership p, User user, Subscription s, int timeout) {
		try {
			PUTTask pt = (PUTTask) new PUTTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + p.getOwner() + ";"
					+ p.getPartner() + "/" + s.getSport(), user.getUsername(),
					user.getPassword(), new String[1]).execute(s.getXML());
			pt.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;

	}

	public static HTTPResponseCode createUser(User user, long timeout) {

		try {
			String[] xml = new String[1];
			PUTTask pt = (PUTTask) new PUTTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername(), xml).execute(user.getXML());
			pt.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode deletePartnershipOwner(User owner,
			String partnername, long timeout) {
		try {
			String[] xml = new String[1];
			DELETETask dt = (DELETETask) new DELETETask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + owner.getUsername() + ";"
					+ partnername, owner.getUsername(), owner.getPassword(),
					xml).execute();
			dt.get(timeout, TimeUnit.MILLISECONDS);
			return (HTTPResponseCode.getFromCode(dt.getResponseCode()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode deletePartnershipParticipant(
			User participant, String ownername, long timeout) {
		try {
			String[] xml = new String[1];
			DELETETask dt = (DELETETask) new DELETETask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + ownername + ";"
					+ participant.getUsername(), participant.getUsername(),
					participant.getPassword(), xml).execute();
			dt.get(timeout, TimeUnit.MILLISECONDS);
			return (HTTPResponseCode.getFromCode(dt.getResponseCode()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static HTTPResponseCode deleteUser(User user, long timeout) {
		try {
			String[] xml = new String[1];
			DELETETask dt = (DELETETask) new DELETETask(SERVER_ADRESS
					+ USERS_RESOURCE + user.getUsername(), user.getUsername(),
					user.getPassword(), xml).execute();
			dt.get(timeout, TimeUnit.MILLISECONDS);
			return (HTTPResponseCode.getFromCode(dt.getResponseCode()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static CyclingEntry getCyclingEntry(Partnership partnership,
			User user, int id, long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + partnership.getOwner() + ";"
					+ partnership.getPartner() + "/Cycling/" + id,
					user.getUsername(), user.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return EntityParserFactory.createCyclingEntry(xml[0]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static CyclingEntry getCyclingEntry(User user, int id, long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername() + "/Cycling/" + id,
					user.getUsername(), user.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return EntityParserFactory.createCyclingEntry(xml[0]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static CyclingSubscription getCyclingSubscriptionForPartnership(
			Partnership p, long timeout) {
		try {
			String[] xml = new String[1];
			User owner = (User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER);
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + p.getOwner() + ";"
					+ p.getPartner() + "/Cycling/", owner.getUsername(),
					owner.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return getCyclingSubscription(xml);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static void getLongTrack() {
		GETTask gt = (GETTask) new GETTask(
				"http://diufvm31.unifr.ch:8090/CyberCoachServer/resources/users/klaas/Running/8",
				"klaas", "1234", new String[1]).execute();
		gt.setProgressBar(RESTConnector.progressBar);
	}

	public static Partnership getPartnership(String username,
			String partnerName, long timeout) {
		String uri = PARTNERSHIPS_SUBURL + username + ";" + partnerName;
		Partnership p = new Partnership(username, partnerName, uri, true);
		return readPartnership(p, timeout);
	}

	public static List<Partnership> getPartnerships(long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + "?start=0&size=100000", xml)
					.execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			if (gt.getResponseCode() == HTTPResponseCode.OK.getCode()) {
				List<Partnership> partnerships = new ArrayList<Partnership>();

				int start = xml[0].indexOf("<partnership ");
				int end = xml[0].indexOf(">", start + 13);
				while (start != -1) {
					int a = xml[0].indexOf("uri=\"", start) + 5;
					int b = xml[0].indexOf("\"", a + 1);

					String uri = xml[0].substring(a, b);
					int index = PARTNERSHIPS_SUBURL.length() - 1;
					String users = uri.substring(index + 1, uri.length() - 1);
					String ownerName = users.split(";")[0];
					String partnerName = users.split(";")[1];
					// is partnership owned by current user? determine by name
					boolean ownerlocal = ownerName.equals(((User) HoneyPot
							.getItem(HoneyPotKey.CURRENT_USER)).getUsername());
					Partnership p = new Partnership(ownerName, partnerName,
							uri, ownerlocal);

					partnerships.add(p);
					Log.v("Partnerships",
							partnerships.get(partnerships.size() - 1)
									.toString());

					start = xml[0].indexOf("<partnership ", end);
					end = xml[0].indexOf(">", start + 16);
				}
				return partnerships;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static List<Partnership> getPartnerships(String username, int timeout) {
		List<Partnership> ps = getPartnerships(timeout);
		for (Iterator<Partnership> iterator = ps.iterator(); iterator.hasNext();) {
			Partnership p = iterator.next();
			if (!username.equals(p.getOwner())
					&& !username.equals(p.getPartner())) {
				iterator.remove();
			}
		}
		return ps;
	}

	public static List<Partnership> getPartnershipsFull(String username,
			int timeout) {
		List<Partnership> ps = getPartnerships(username, timeout);
		List<Partnership> psNew = new ArrayList<Partnership>();
		for (Iterator<Partnership> iterator = ps.iterator(); iterator.hasNext();) {
			Partnership item = iterator.next();
			psNew.add(readPartnership(item, timeout));
		}
		return psNew;
	}

	public static RunningEntry getRunningEntry(Partnership partnership,
			User user, int id, long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + partnership.getOwner() + ";"
					+ partnership.getPartner() + "/Running/" + id,
					user.getUsername(), user.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return EntityParserFactory.createRunningEntry(xml[0]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static RunningEntry getRunningEntry(User user, int id, long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername() + "/Running/" + id,
					user.getUsername(), user.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return EntityParserFactory.createRunningEntry(xml[0]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static RunningSubscription getRunningSubscriptionForPartnership(
			Partnership p, long timeout) {
		try {
			String[] xml = new String[1];
			User owner = (User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER);
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS
					+ PARTNERSHIPS_RESOURCE + p.getOwner() + ";"
					+ p.getPartner() + "/Running/", owner.getUsername(),
					owner.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return getRunningSubscription(xml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static CyclingSubscription getSubscriptionCycling(User user,
			long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername() + "/Cycling/", user.getUsername(),
					user.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			return getCyclingSubscription(xml);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static RunningSubscription getSubscriptionRunning(User user,
			long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername() + "/Running/", user.getUsername(),
					user.getPassword(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);

			return getRunningSubscription(xml);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static User getUser(String username, long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS + USERS_RESOURCE
					+ username, xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			if (gt.getResponseCode() == HTTPResponseCode.OK.getCode()) {
				User u = EntityParserFactory.createUser(xml[0]);
				return u;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static LinkedList<User> getUsers(long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS + USERS_RESOURCE
					+ "?start=0&size=100000", xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			if (gt.getResponseCode() == HTTPResponseCode.OK.getCode()) {
				LinkedList<User> users = new LinkedList<User>();

				int start = xml[0].indexOf("<user ");
				int end = xml[0].indexOf(">", start + 6);
				while (start != -1) {
					int a = xml[0].indexOf("username=\"", start) + 10;
					int b = xml[0].indexOf("\"", a + 1);

					User u = new User(xml[0].substring(a, b));
					users.add(u);
					Log.v("Users", users.get(users.size() - 1).toString());

					start = xml[0].indexOf("<user username=\"", end);
					end = xml[0].indexOf(">", start + 16);
				}
				return users;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static List<Entry> listAllEntries(Partnership p) {
		p = readPartnership(p, 3000);
		User user = (User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER);
		List<Entry> entries = new ArrayList<Entry>();

		CyclingSubscription cycling = getCyclingSubscriptionForPartnership(p,
				3000);
		for (Integer i : cycling.getSubscriptionServerIds()) {
			Entry entry = getCyclingEntry(user, i, 3000);
			entries.add(entry);
		}

		RunningSubscription running = getRunningSubscriptionForPartnership(p,
				3000);
		for (Integer i : running.getSubscriptionServerIds()) {
			Entry entry = getRunningEntry(user, i, 3000);
			entries.add(entry);
		}

		Collections.sort(entries, new EntryTimeComparator());

		return entries;
	}

	public static boolean logonUser(User user, long timeout) {
		PUTTask task = (PUTTask) new PUTTask(SERVER_ADRESS + USERS_RESOURCE
				+ user.getUsername(), user.getUsername(), user.getPassword(),
				new String[1])
				.execute("<user><publicvisible>2</publicvisible></user>");
		try {
			task.get(timeout, TimeUnit.MILLISECONDS);
			Log.v("responsecode", "" + task.getResponseCode());
			return task.getResponseCode() == 200;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Partnership readPartnership(Partnership p, long timeout) {
		try {
			String[] xml = new String[1];
			GETTask gt = (GETTask) new GETTask(SERVER_ADRESS.split("/Cyber")[0]
					+ p.getUri(), xml).execute();
			gt.get(timeout, TimeUnit.MILLISECONDS);
			if (gt.getResponseCode() == HTTPResponseCode.OK.getCode()) {
				Partnership partnership = EntityParserFactory
						.createPartnership(p, xml[0]);
				return partnership;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static HTTPResponseCode register(User user, long timeout) {
		try {
			String[] xml = new String[1];
			PUTTask task = new PUTTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername(), xml);
			task.execute(user.getXML());
			task.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(task.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	public static void setContext(Context context) {
		RESTConnector.context = context;
	}

	public static void setProgressBar(ProgressBar progressBar) {
		RESTConnector.progressBar = progressBar;
	}

	public static HTTPResponseCode updateUser(User user, long timeout) {

		try {
			String[] xml = new String[1];
			PUTTask pt = (PUTTask) new PUTTask(SERVER_ADRESS + USERS_RESOURCE
					+ user.getUsername(), user.getUsername(),
					user.getOldPassword(), xml).execute(user.getXML());
			pt.get(timeout, TimeUnit.MILLISECONDS);
			return HTTPResponseCode.getFromCode(pt.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HTTPResponseCode.TIMEOUT;
	}

	private static User getCurrentUser(Context context) {
		BananaDatabase db = new BananaDatabase(context);
		SettingsDAO settings = new SettingsDAO(db);
		User user = settings.getCurrentUser();
		db.close();
		return user;
	}

	private static CyclingSubscription getCyclingSubscription(String[] xml) {
		CyclingSubscription subscription = new CyclingSubscription();
		if (xml[0].contains("<entries")) {
			int start = xml[0].indexOf("<entrycycling");
			int end = xml[0].indexOf("/>", start);

			while (start != -1) {
				int a = xml[0].indexOf("id=\"", start) + 4;
				int b = xml[0].indexOf("\"", a + 1);
				subscription.getSubscriptionServerIds().add(
						Integer.parseInt(xml[0].substring(a, b)));

				Log.v("CyclingSubscription", xml[0].substring(a, b));

				start = xml[0].indexOf("<entrycycling", end);
				end = xml[0].indexOf("/>", start);
			}
		}
		return subscription;
	}

	private static RunningSubscription getRunningSubscription(String[] xml) {
		RunningSubscription subscription = new RunningSubscription();
		if (xml[0].contains("<entries")) {
			int start = xml[0].indexOf("<entryrunning");
			int end = xml[0].indexOf("/>", start);

			while (start != -1) {
				int a = xml[0].indexOf("id=\"", start) + 4;
				int b = xml[0].indexOf("\"", a + 1);
				subscription.getSubscriptionServerIds().add(
						Integer.parseInt(xml[0].substring(a, b)));

				Log.v("RunningSubscription", xml[0].substring(a, b));

				start = xml[0].indexOf("<entryrunning", end);
				end = xml[0].indexOf("/>", start);
			}
		}
		return subscription;
	}

	static void showMessage(String title, String message) {
		if (RESTConnector.context != null) {
			AlertDialog dialog = new AlertDialog.Builder(RESTConnector.context)
					.create();
			dialog.setTitle(title);
			dialog.setTitle(message);
			dialog.show();
		}
	}

	private RESTConnector() {
	}
}
