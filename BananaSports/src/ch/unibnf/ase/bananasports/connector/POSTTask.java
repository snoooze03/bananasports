package ch.unibnf.ase.bananasports.connector;

import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;
import ch.unibnf.ase.bananasports.util.Helper;

public class POSTTask extends AsyncTask<String, Void, String> {
	public static final String TYPE = "text/xml";

	private String resourcePath;
	private String username;
	private String password;
	private String[] xml;
	private String response;
	private int responseCode;

	private String responseLocation;

	public POSTTask(String resourcePath, String username, String password,
			String[] xml) {
		this.resourcePath = resourcePath;
		this.username = username;
		this.password = password;
		this.xml = xml;
	}

	public POSTTask(String resourcePath, String[] xml) {
		this.resourcePath = resourcePath;
		this.xml = xml;
	}

	@Override
	protected String doInBackground(String... params) {
		try {

			Log.v("POSTTask", "started");
			// open connection
			HttpURLConnection connection = (HttpURLConnection) ((new URL(
					this.resourcePath).openConnection()));

			// send request
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			if (this.username != null) {
				connection.setRequestProperty("Authorization", Helper
						.createAuthorizationHeaderValue(this.username,
								this.password));
			}
			connection.addRequestProperty("Content-Type", TYPE);
			connection.addRequestProperty("Accept", TYPE);
			Log.v("POSTTask", params[0]);
			connection.getOutputStream().write(params[0].getBytes());
			connection.getOutputStream().flush();
			this.response = connection.getResponseMessage() + " "
					+ connection.getResponseCode();
			this.responseCode = connection.getResponseCode();
			Log.v("POSTTask", this.response);
			this.responseLocation = connection.getHeaderField("Location");
			StringBuffer stringBuffer = new StringBuffer();
			int readBytes;
			byte[] buffer = new byte[4096];
			while (0 <= (readBytes = connection.getInputStream().read(buffer,
					0, buffer.length))) {
				stringBuffer.append(new String(buffer, 0, readBytes));
				// System.out.write(buffer, 0, readBytes);
			}
			this.xml[0] = stringBuffer.toString();
			return this.xml[0];
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	protected String getLocationHeaderField() {
		return this.responseLocation;
	}

	protected int getResponseCode() {
		return this.responseCode;
	}

	@Override
	protected void onPostExecute(String xml) {
		RESTConnector.showMessage("Response", this.response);
		super.onPostExecute(xml);
	}
}