package ch.unibnf.ase.bananasports.connector;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.util.Log;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.entities.CyclingEntry;
import ch.unibnf.ase.bananasports.entities.CyclingSubscription;
import ch.unibnf.ase.bananasports.entities.Entry;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.RunningEntry;
import ch.unibnf.ase.bananasports.entities.RunningSubscription;
import ch.unibnf.ase.bananasports.entities.Subscription;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;
import ch.unibnf.ase.bananasports.util.SaveAnalyzeHelper;

public class SynchronizeHelper {

	private static boolean complete;

	public static boolean isComplete() {
		return complete;
	}

	/**
	 * 
	 * @param user
	 *            logged in user: these tracks must be analyzed and added to db
	 * @param context
	 */
	public static synchronized void synchronizeAllTracks(User currentUser,
			Context context, boolean blocking) {
		complete = false;
		final Executor threadPool = Executors.newFixedThreadPool(5);

		final AtomicInteger runningThreads = new AtomicInteger();

		// part 1: get tracks from server
		// part 2: upload all tracks without ID to server

		Runnable runnable = new Runnable() {

			public void run() {
				// get all users
				LinkedList<User> users = RESTConnector.getUsers(30000);
				// per user: get its tracks
				// if current user: do analysis
				for (final User user : users) {
					Subscription cyclesubs = RESTConnector
							.getSubscriptionCycling(user, 10000);
					if (cyclesubs != null
							&& cyclesubs.getSubscriptionServerIds() != null) {
						final LinkedList<Integer> cyclingIds = cyclesubs
								.getSubscriptionServerIds();
						Runnable cyclingRun = new Runnable() {

							public void run() {
								for (int serverId : cyclingIds) {
									try {
										CyclingEntry ce = RESTConnector
												.getCyclingEntry(user,
														serverId, 30000);
										System.out.println((ce != null ? ce
												.getXML() : "null"));
										System.gc();
									} catch (Error e) {
										// android sucks!
										System.gc();
										e.printStackTrace();
										System.err.println(user.toString()
												+ " cycling: " + serverId);
									}
								}
								runningThreads.decrementAndGet();
							}
						};
						threadPool.execute(cyclingRun);
						runningThreads.incrementAndGet();
					}
					Subscription runsubs = RESTConnector
							.getSubscriptionRunning(user, 10000);
					if (runsubs != null
							&& runsubs.getSubscriptionServerIds() != null) {

						final LinkedList<Integer> runningIds = runsubs
								.getSubscriptionServerIds();
						Runnable runningRun = new Runnable() {

							public void run() {
								for (int serverId : runningIds) {
									try {
										RunningEntry re = RESTConnector
												.getRunningEntry(user,
														serverId, 30000);
										System.out.println((re != null ? re
												.getXML() : "null"));
										System.gc();
									} catch (Error e) {
										// android sucks!
										System.gc();
										e.printStackTrace();
										System.err.println(user.toString()
												+ " cycling: " + serverId);
									}
								}
								runningThreads.decrementAndGet();
							}
						};
						threadPool.execute(runningRun);
						runningThreads.incrementAndGet();
					}
				}
				runningThreads.decrementAndGet();

				while (runningThreads.get() > 0) {
					// System.out.println("" + runningThreads.get());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
				complete = true;
				Log.v("SynchronizeHelper", "complete");

			}
		};
		threadPool.execute(runnable);
		runningThreads.getAndIncrement();
		if (blocking) {
			while (!complete)
				;
		}
	}

	public static synchronized void synchronizeCurrentUserTracks(
			final User currentUser, final Context context, boolean blocking) {
		complete = false;
		final Executor threadPool = Executors.newFixedThreadPool(10);
		final AtomicInteger runningThreads = new AtomicInteger();

		Runnable runnable = new Runnable() {

			public void run() {
				BananaDatabase db = new BananaDatabase(context);
				final EntryFileDAO entryFileDAO = new EntryFileDAO(db);
				final LinkedList<EntryFile> dbEntries = entryFileDAO
						.GetEntryFiles();

				// 1) download
				// 2) upload what is not synced

				// running subscriptions
				final RunningSubscription rs = RESTConnector
						.getSubscriptionRunning(currentUser, 10000);
				if (rs != null && rs.getSubscriptionServerIds() != null) {
					Runnable runningRun = new Runnable() {

						public void run() {
							for (int serverId : rs.getSubscriptionServerIds()) {
								try {
									if (containsServerId(
											currentUser.getUsername(),
											serverId, "Running", dbEntries)) {
										// already in db
										continue;
									}
									RunningEntry re = RESTConnector
											.getRunningEntry(currentUser,
													serverId, 30000);
									if (re != null) {
										SaveAnalyzeHelper.save(context,
												currentUser.getUsername(), re,
												true);
									}
									System.out.println((re != null ? re
											.getXML() : "null"));
									System.gc();
								} catch (Error e) {
									// android sucks! (out of memory catch...)
									System.gc();
									e.printStackTrace();
									System.err.println(currentUser.toString()
											+ " cycling: " + serverId);
								}
							}
							runningThreads.decrementAndGet();
						}
					};
					threadPool.execute(runningRun);
					runningThreads.incrementAndGet();
				}

				// cycling subscriptions
				final CyclingSubscription cs = RESTConnector
						.getSubscriptionCycling(currentUser, 10000);

				if (cs != null && cs.getSubscriptionServerIds() != null) {
					Runnable runningRun = new Runnable() {

						public void run() {
							for (int serverId : cs.getSubscriptionServerIds()) {
								try {
									if (containsServerId(
											currentUser.getUsername(),
											serverId, "Cycling", dbEntries)) {
										continue;
										// already in db
									}
									CyclingEntry ce = RESTConnector
											.getCyclingEntry(currentUser,
													serverId, 30000);
									if (ce != null) {
										SaveAnalyzeHelper.save(context,
												currentUser.getUsername(), ce,
												true);
									}
									System.out.println((ce != null ? ce
											.getXML() : "null"));
									System.gc();
								} catch (Error e) {
									// android sucks! (out of memory catch..)
									System.gc();
									e.printStackTrace();
									System.err.println(currentUser.toString()
											+ " cycling: " + serverId);
								}
							}
							runningThreads.decrementAndGet();
						}
					};
					threadPool.execute(runningRun);
					runningThreads.incrementAndGet();
				}

				// Partnerships stuff
				List<Partnership> partnerships = RESTConnector.getPartnerships(
						currentUser.getUsername(), 30000);
				for (final Partnership partnership : partnerships) {
					// running entries
					final RunningSubscription runSubs = RESTConnector
							.getRunningSubscriptionForPartnership(partnership,
									3000);
					if (runSubs != null
							&& runSubs.getSubscriptionServerIds() != null) {
						Runnable runningRun = new Runnable() {

							public void run() {
								for (int serverId : runSubs
										.getSubscriptionServerIds()) {
									try {
										if (containsServerId(
												partnership.getOwner()
														+ ";"
														+ partnership
																.getPartner(),
												serverId, "Running", dbEntries)) {
											// already in db
											continue;
										}
										RunningEntry re = RESTConnector
												.getRunningEntry(partnership,
														currentUser, serverId,
														30000);
										if (re != null) {
											SaveAnalyzeHelper
													.save(context,
															partnership
																	.getOwner()
																	+ ";"
																	+ partnership
																			.getPartner(),
															re, true);
										}
										System.out.println((re != null ? re
												.getXML() : "null"));
										System.gc();
									} catch (Error e) {
										// android sucks! (out of memory catch
										// ...)
										System.gc();
										e.printStackTrace();
										System.err.println(partnership
												.getOwner()
												+ ";"
												+ partnership.getPartner()
												+ " cycling: " + serverId);
									}
								}
								runningThreads.decrementAndGet();
							}
						};
						threadPool.execute(runningRun);
						runningThreads.incrementAndGet();
					}

					// cycling subscriptions
					final CyclingSubscription cyclSubs = RESTConnector
							.getSubscriptionCycling(currentUser, 3000);

					if (cyclSubs != null
							&& cyclSubs.getSubscriptionServerIds() != null) {
						Runnable runningRun = new Runnable() {

							public void run() {
								for (int serverId : cyclSubs
										.getSubscriptionServerIds()) {
									try {
										if (containsServerId(
												partnership.getOwner()
														+ ";"
														+ partnership
																.getPartner(),
												serverId, "Cycling", dbEntries)) {
											continue;
											// already in db
										}
										CyclingEntry ce = RESTConnector
												.getCyclingEntry(partnership,
														currentUser, serverId,
														30000);
										if (ce != null) {
											SaveAnalyzeHelper
													.save(context,
															partnership
																	.getOwner()
																	+ ";"
																	+ partnership
																			.getPartner(),
															ce, true);
										}
										System.out.println((ce != null ? ce
												.getXML() : "null"));
										System.gc();
									} catch (Error e) {
										// android sucks!
										System.gc();
										e.printStackTrace();
										System.err.println(partnership
												.getOwner()
												+ ";"
												+ partnership.getPartner()
												+ " cycling: " + serverId);
									}
								}
								runningThreads.decrementAndGet();
							}
						};
						threadPool.execute(runningRun);
						runningThreads.incrementAndGet();
					}

				}
				for (EntryFile ef : dbEntries) {
					// upload all tracks if necessary
					if (ef.username.equals(currentUser.getUsername())
							&& ef.serverid < 0) {
						entryFileDAO.LoadTrack(ef);
						// must upload
						Entry entry = null;
						if (ef.sport.equals("Running")) {
							RESTConnector.createSubscription(currentUser,
									new RunningSubscription(), 10000);
							entry = new RunningEntry();
						} else {
							RESTConnector.createSubscription(currentUser,
									new CyclingSubscription(), 10000);
							entry = new CyclingEntry();
						}
						entry.setCompressedTrack(ef.gpx);
						entry.entrydate = new ISO8601TimeHelper()
								.getString(System.currentTimeMillis());
						if (ef.username.contains(";")) {
							Partnership p = new Partnership();
							String[] names = ef.username.split(";");
							p.setOwner(names[0]);
							p.setPartner(names[1]);

							RESTConnector.createSubscriptionForPartnership(p,
									currentUser, new RunningSubscription(),
									10000);
							RESTConnector.createSubscriptionForPartnership(p,
									currentUser, new CyclingSubscription(),
									10000);

							RESTConnector.createEntry(p, currentUser, entry,
									100000);
						} else {
							RESTConnector.createEntry(currentUser, entry,
									100000);
						}
						if (entry.serverID > 0) {
							ef.serverid = entry.serverID;
							entryFileDAO.Update(ef);
						}
					}
				}

				runningThreads.decrementAndGet();
				while (runningThreads.get() > 0) {
					// System.out.println("" + runningThreads.get());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
				// close DB
				db.close();
				complete = true;
				Log.v("SynchronizeHelper", "complete");
			}
		};
		threadPool.execute(runnable);
		runningThreads.incrementAndGet();

		if (blocking) {
			while (!complete) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static boolean containsServerId(String owner, int id, String sport,
			LinkedList<EntryFile> list) {
		for (EntryFile ef : list) {
			if (ef.username.equals(owner) && ef.serverid == id
					&& ef.sport.equals(sport)) {
				Log.v("SynchronizeHelper", "found entry in db: " + owner + id
						+ sport);
				return true;
			}
		}
		Log.v("SynchronizeHelper", "not found: " + owner + id + sport);
		return false;
	}
}
