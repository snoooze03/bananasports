package ch.unibnf.ase.bananasports.connector;

import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;
import ch.unibnf.ase.bananasports.util.Helper;

public class GETTask extends AsyncTask<Void, Integer, String> {

	public static final String TYPE = "text/xml";

	private String resourcePath;
	private String username;
	private String password;
	private String response;
	private int responseCode;
	private ProgressBar progressBar;
	private String[] xml;

	public GETTask(String resourcePath, String username, String password,
			String[] xml) {
		this.resourcePath = resourcePath;
		this.username = username;
		this.password = password;
		this.xml = xml;
	}

	public GETTask(String resourcePath, String[] xml) {
		this.resourcePath = resourcePath;
		this.xml = xml;
	}

	public void setProgressBar(ProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	@Override
	protected String doInBackground(Void... params) {
		try {

			Log.v("GETTask", "started");
			// open connection
			HttpURLConnection connection = (HttpURLConnection) ((new URL(
					this.resourcePath).openConnection()));
			Log.v("GETTask", "loading resource: " + this.resourcePath);
			// send request
			connection.setDoOutput(false);
			connection.setRequestMethod("GET");
			if (this.username != null) {
				connection.setRequestProperty("Authorization", Helper
						.createAuthorizationHeaderValue(this.username,
								this.password));
			}
			connection.addRequestProperty("Content-Type", TYPE);
			connection.addRequestProperty("Accept", TYPE);
			this.response = connection.getResponseMessage() + " "
					+ connection.getResponseCode();
			this.responseCode = connection.getResponseCode();
			int contentLength = connection.getContentLength();
			Log.v("GETTask", this.response);
			StringBuffer stringBuffer = new StringBuffer();
			int readBytes;
			byte[] buffer = new byte[4096];
			int totalRead = 0;
			while (0 <= (readBytes = connection.getInputStream().read(buffer,
					0, buffer.length))) {
				stringBuffer.append(new String(buffer, 0, readBytes));
				// System.out.write(buffer, 0, readBytes);
				totalRead += readBytes;
				if (stringBuffer.length() > 1500000) {
					return null;
					// android will have out of memory if we continue..
				}
				this.publishProgress(totalRead, contentLength);
			}
			this.xml[0] = stringBuffer.toString();
			return this.xml[0];
		} catch (Exception ex) {
			// ex.printStackTrace();
		}
		return null;
	}

	protected int getResponseCode() {
		return this.responseCode;
	}

	@Override
	protected void onPostExecute(String xml) {
		RESTConnector.showMessage("Response", this.response);
		super.onPostExecute(xml);
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		if (this.progressBar != null) {
			this.progressBar.setMax(values[1]);
			this.progressBar.setProgress(values[0]);
		}
		Log.v("GETTask", "Progress: " + values[0] + " " + values[1]);
	}
}