package ch.unibnf.ase.bananasports.connector;

import android.util.SparseArray;

public enum HTTPResponseCode {
	/** No real status code. But used to mark a timeout */
	TIMEOUT(0), OK(200), CREATED(201), ACCEPTED(202), BADREQUEST(400), UNAUTHORIZED(
			401), FORBIDDEN(403), NOTFOUND(404), METHODNOTALLOWED(405), CONFLICT(
			409), UNSUPPORTEDMEDIATYPE(415), IMATEAPOT(418), INTERNALSERVERERROR(
			500), NOTIMPLEMENTED(501), BADGATEWAY(502);

	private int code;

	private static SparseArray<HTTPResponseCode> map = new SparseArray<HTTPResponseCode>();
	static {
		for (HTTPResponseCode code : HTTPResponseCode.values()) {
			map.put(code.getCode(), code);
		}
	}

	public static HTTPResponseCode getFromCode(int code) {
		return map.get(code);
	}

	HTTPResponseCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}
}
