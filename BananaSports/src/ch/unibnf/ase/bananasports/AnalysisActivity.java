package ch.unibnf.ase.bananasports;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.gpx.GPXTrackpointEntry;
import ch.unibnf.ase.bananasports.map.GPXOverlay;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class AnalysisActivity extends MapActivity {

	public static final String GPXFILE = "trackactivity_GPXFILE";

	/** Amount of track points to be used for graphing */
	protected static final int MAXTRACKPOINTS = 10000;
	private ImageView imageView1;
	private ImageView imageView2;
	private ImageView imageView3;
	private TextView imagetext1;
	private TextView imagetext2;
	private TextView imagetext3;

	private TextView trackTitle;

	private final Paint greenPaint = new Paint();
	private final Paint blackPaint = new Paint();
	private TextView durationTextView;
	private TextView totalDistanceTextView;
	private TextView averageSpeedTextView;
	private TextView altitudeGainTextView;
	private TextView altitudeLossTextView;
	private MapView mapView;
	private GPXOverlay myOverlay;
	private MapController mapController;
	private TextView stepsTextView;

	private TextView caloriesTextView;

	public AnalysisActivity() {
		this.greenPaint.setColor(0xFF00FF00);
		this.greenPaint.setDither(true);
		this.greenPaint.setStrokeWidth(0);
		this.greenPaint.setStyle(Paint.Style.STROKE);

		this.blackPaint.setColor(0xFF000000);
		this.blackPaint.setDither(true);
		this.blackPaint.setStrokeWidth(0);
		this.blackPaint.setStyle(Paint.Style.STROKE);
	}

	private void analyze(final GPXFile gpx) {

		this.runOnUiThread(new Runnable() {

			public void run() {
				AnalysisActivity.this.trackTitle.setText(gpx.getFilename());
				AnalysisActivity.this.trackTitle.invalidate();

				AnalysisActivity.this.clearAll();
			}
		});

		this.myOverlay.setGPX(gpx);

		if (gpx.getTrack() != null) {
			this.createHeightGraph(gpx.getTrack());

			this.createPaceGraph(gpx.getTrack());
			this.createHeartRateGraph(gpx.getTrack());

		}
	}

	private void clearAll() {

		this.imagetext1.setText("");
		this.imageView1.setImageBitmap(null);
		this.imagetext2.setText("");
		this.imageView2.setImageBitmap(null);
		this.imagetext3.setText("");
		this.imageView3.setImageBitmap(null);

		this.durationTextView.setText("");
		this.totalDistanceTextView.setText("");
		this.averageSpeedTextView.setText("");
		this.altitudeLossTextView.setText("");
		this.altitudeGainTextView.setText("");
		this.stepsTextView.setText("");
		this.caloriesTextView.setText("");
	}

	@SuppressWarnings("unused")
	private Bitmap createDummyGraph() {
		Random random = new Random();
		Bitmap bitmap = Bitmap.createBitmap(1000, 200, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		canvas.save();
		canvas.scale(1f, 1f);

		Paint paint = new Paint();
		paint.setColor(0xFFFF00FF);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		canvas.drawColor(random.nextInt());

		for (int i = 0; i <= 2000; i += 500) {
			canvas.drawRect(i, 0, i + 10, 200, paint);
		}

		// hardware limitations: 2048/2048. hence create image then scale it

		canvas.restore();

		return (bitmap);
	}

	private void createHeartRateGraph(final LinkedList<GPXTrackpointEntry> track) {
		new Thread() {
			@Override
			public void run() {

				// final Bitmap bitmap =
				// AnalysisActivity.this.createDummyGraph();
				long minTime = track.getFirst().getTime();
				long maxTime = track.getLast().getTime();
				long deltaT = maxTime - minTime;

				int maxHR = Integer.MIN_VALUE;
				int minHR = Integer.MAX_VALUE;

				for (int i = 0; i < track.size(); i++) {
					maxHR = Math.max(maxHR, track.get(i).getHeartrate());
					minHR = Math.min(minHR, track.get(i).getHeartrate());
				}

				if (maxHR - minHR == 0) {
					// no pulse data
					return;
				}

				// boundaries
				maxHR += 5;
				minHR -= 5;

				int deltaHR = maxHR - minHR;

				final Bitmap bitmap = Bitmap.createBitmap(1000, 200,
						Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				canvas.drawColor(0xFFFFFFFF);
				canvas.save();
				canvas.scale(1000f / deltaT, 200f / deltaHR);

				int delta = track.size() / MAXTRACKPOINTS;
				if (delta == 0) {
					delta = 1;
				}
				for (int i = 1; i < track.size() + delta - 1; i += delta) {

					Path p = new Path();
					p.moveTo(track.get(i - 1).getTime() - minTime, maxHR
							- track.get(i - 1).getHeartrate());
					p.lineTo(track.get(i).getTime() - minTime, maxHR
							- track.get(i).getHeartrate());
					canvas.drawPath(p, AnalysisActivity.this.greenPaint);
				}

				// min HR line
				Path p = new Path();
				p.moveTo(0, 5);
				p.lineTo(maxTime, 5);
				canvas.drawPath(p, AnalysisActivity.this.blackPaint);
				// max HR line
				p.moveTo(0, maxHR - 5 - minHR);
				p.lineTo(maxTime, maxHR - 5 - minHR);
				canvas.drawPath(p, AnalysisActivity.this.blackPaint);

				canvas.restore();

				canvas.drawText("" + (maxHR - 5), 5, 15,
						AnalysisActivity.this.blackPaint);
				canvas.drawText("" + (minHR + 5), 5, 195,
						AnalysisActivity.this.blackPaint);

				AnalysisActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						AnalysisActivity.this.imageView3.setImageBitmap(bitmap);
						AnalysisActivity.this.imagetext3.setText("Heart Rate");
					}
				});
			}
		}.start();
	}

	private void createHeightGraph(
			final LinkedList<GPXTrackpointEntry> trackpoints) {
		new Thread() {
			@Override
			public void run() {
				double altitudeGain = 0;
				double altitudeLoss = 0;
				long minTime = trackpoints.getFirst().getTime();
				long maxTime = trackpoints.getLast().getTime();
				long dt = maxTime - minTime;
				float minHeight = Integer.MAX_VALUE;
				float maxHeight = Integer.MIN_VALUE;
				for (GPXTrackpointEntry l : trackpoints) {
					if (l.getElevation() > maxHeight) {
						maxHeight = (int) l.getElevation();
					}
					if (l.getElevation() < minHeight) {
						minHeight = (int) l.getElevation();
					}
				}

				if ((maxHeight - minHeight) == 0) {
					// can return
					return;
				}

				maxHeight += 10;
				minHeight -= 10;
				float deltaAlt = maxHeight - minHeight;
				// float deltaAlt = maxHeight;

				Log.v("Analysis: createHeightGraph", "deltaTime=" + dt
						+ " deltaHeight=" + deltaAlt);

				final Bitmap bitmap = Bitmap.createBitmap(1000, 200,
						Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				canvas.drawColor(0xFFFFFFFF);
				canvas.save();
				canvas.scale(1000f / dt, 200f / deltaAlt);

				// canvas.translate(0, -(maxHeight - minHeight) / 2);

				int delta = trackpoints.size() / MAXTRACKPOINTS;
				if (delta == 0) {
					delta = 1;
				}
				for (int i = 1; i < trackpoints.size() + delta - 1; i += delta) {

					Path p = new Path();
					p.moveTo(trackpoints.get(i - 1).getTime() - minTime,
							maxHeight
									- (float) trackpoints.get(i - 1)
											.getElevation());
					p.lineTo(trackpoints.get(i).getTime() - minTime, maxHeight
							- (float) trackpoints.get(i).getElevation());
					canvas.drawPath(p, AnalysisActivity.this.greenPaint);
					// height difference
					double deltaHeight = trackpoints.get(i).getElevation()
							- trackpoints.get(i - 1).getElevation();
					if (0 < deltaHeight) {
						altitudeGain += deltaHeight;
					} else {
						altitudeLoss += deltaHeight;
					}
				}

				// min height line
				Path p = new Path();
				p.moveTo(0, 10);
				p.lineTo(maxTime, 10);
				canvas.drawPath(p, AnalysisActivity.this.blackPaint);
				// max height line
				p.moveTo(0, maxHeight - 10 - minHeight);
				p.lineTo(maxTime, maxHeight - 10 - minHeight);
				canvas.drawPath(p, AnalysisActivity.this.blackPaint);

				canvas.restore();

				canvas.drawText("" + (maxHeight - 10), 5, 15,
						AnalysisActivity.this.blackPaint);
				canvas.drawText("" + (minHeight + 10), 5, 195,
						AnalysisActivity.this.blackPaint);

				final double finalGain = altitudeGain;
				final double finalLoss = altitudeLoss;

				AnalysisActivity.this.runOnUiThread(new Runnable() {
					public void run() {
						DecimalFormat f = new DecimalFormat("#0.00");
						AnalysisActivity.this.altitudeGainTextView.setText(f
								.format(finalGain) + "m");
						AnalysisActivity.this.altitudeLossTextView.setText(f
								.format(finalLoss) + "m");
						AnalysisActivity.this.imageView1.setImageBitmap(bitmap);
						AnalysisActivity.this.imagetext1
								.setText("Height profile");
					}
				});
			}
		}.start();
	}

	private void createPaceGraph(
			final LinkedList<GPXTrackpointEntry> trackpoints) {
		new Thread() {
			@Override
			public void run() {
				double totalDistance = 0;
				long minTime = trackpoints.getFirst().getTime();
				long maxTime = trackpoints.getLast().getTime();
				final long dt = maxTime - minTime;

				final Bitmap bitmap = Bitmap.createBitmap(1000, 200,
						Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				canvas.drawColor(0xFFFFFFFF);
				canvas.save();

				float[] distance = new float[1];

				int delta = trackpoints.size() / MAXTRACKPOINTS;
				if (delta == 0) {
					delta = 1;
				}

				LinkedList<Float> speeds = new LinkedList<Float>();
				LinkedList<Long> times = new LinkedList<Long>();

				float maxSpeed = Float.MIN_VALUE;
				float minSpeed = Float.MAX_VALUE;

				for (int i = 1; i < trackpoints.size() + delta - 1; i += delta) {
					{
						if (i >= trackpoints.size()) {
							i = trackpoints.size() - 1;
						}
						Location.distanceBetween(trackpoints.get(i - 1)
								.getLatitude(), trackpoints.get(i - 1)
								.getLongitude(), trackpoints.get(i)
								.getLatitude(), trackpoints.get(i)
								.getLongitude(), distance);
						totalDistance += distance[0];
						float speed = (distance[0] / 1000f / ((trackpoints.get(
								i).getTime() - trackpoints.get(i - 1).getTime()) / 1000f / 60f / 60f));
						// Log.v("Speed", "" + speed);
						speeds.add(speed);
						times.add(trackpoints.get(i).getTime());
						maxSpeed = Math.max(maxSpeed, speed);
						minSpeed = Math.min(minSpeed, speed);
					}
				}

				int maxS = (int) FloatMath.floor(maxSpeed + 1);
				int minS = (int) FloatMath.floor(minSpeed - 1);

				// as we have all needed values: scale
				canvas.scale(1000f / dt, 200f / (maxS - minS));

				for (int i = 1; i < speeds.size(); i++) {
					Path p = new Path();
					p.moveTo(times.get(i - 1) - minTime, speeds.get(i - 1)
							- minS);
					p.lineTo(times.get(i) - minTime, speeds.get(i) - minS);
					canvas.drawPath(p, AnalysisActivity.this.greenPaint);
				}

				// y-axis: time lines
				// for (int i = 0; i < dt; i += 60000) {
				// Path p = new Path();
				// p.moveTo(i, 0);
				// p.lineTo(i, 200);
				// canvas.drawPath(p, AnalysisActivity.this.paint);
				// }

				// x-axis: speed
				// min speed
				Path p = new Path();
				p.moveTo(0, minS + 1);
				p.lineTo(dt, minS + 1);
				canvas.drawPath(p, AnalysisActivity.this.blackPaint);

				// max speed
				p = new Path();
				p.moveTo(0, maxS - 1);
				p.lineTo(dt, maxS - 1);
				canvas.drawPath(p, AnalysisActivity.this.blackPaint);

				canvas.restore();

				canvas.drawText(maxS + "km/h", 5, 15,
						AnalysisActivity.this.blackPaint);
				canvas.drawText(minS + "km/h", 5, 195,
						AnalysisActivity.this.blackPaint);

				long milliseconds = dt % 1000;
				long seconds = dt / 1000 % 60;
				long minutes = dt / 1000 / 60 % 60;
				long hours = dt / 1000 / 60 / 60;
				final String duration = (String.format(Locale.getDefault(),
						"%02d:%02d:%02d:%03d", hours, minutes, seconds,
						milliseconds));
				DecimalFormat f = new DecimalFormat("#0.00");
				final String distanceString = f.format(totalDistance) + "m";

				// km/h
				double avgSpeed = (totalDistance / 1000d)
						/ (dt / 1000d / 60d / 60d);

				final String averageSpeedString = f.format(avgSpeed) + "km/h";
				final String totalStepsString = (int) ((totalDistance / 0.8f))
						+ " steps";

				final String kcalString = "Running="
						+ (int) (dt / 3600000d * 650) + "kcal, \nCycling="
						+ (int) (dt / 3600000d * 400) + "kcal";
				AnalysisActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						AnalysisActivity.this.imageView2.setImageBitmap(bitmap);
						AnalysisActivity.this.imagetext2.setText("Pace/Speed");
						AnalysisActivity.this.durationTextView
								.setText(duration);
						AnalysisActivity.this.totalDistanceTextView
								.setText(distanceString);
						AnalysisActivity.this.averageSpeedTextView
								.setText(averageSpeedString);
						AnalysisActivity.this.stepsTextView
								.setText(totalStepsString);
						AnalysisActivity.this.caloriesTextView
								.setText(kcalString);
					}
				});
			}
		}.start();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.analysis_activity);
		GPXFile gpx = (GPXFile) this.getIntent().getSerializableExtra(GPXFILE);

		this.trackTitle = (TextView) this
				.findViewById(R.id.analysis_tracktitle);

		this.imageView1 = (ImageView) this
				.findViewById(R.id.analysis_imageView1);
		this.imagetext1 = (TextView) this
				.findViewById(R.id.analysis_imageText1);

		this.imageView2 = (ImageView) this
				.findViewById(R.id.analysis_imageView2);
		this.imagetext2 = (TextView) this
				.findViewById(R.id.analysis_imageText2);

		this.imageView3 = (ImageView) this
				.findViewById(R.id.analysis_imageView3);
		this.imagetext3 = (TextView) this
				.findViewById(R.id.analysis_imageText3);

		this.durationTextView = (TextView) this
				.findViewById(R.id.analysis_durationtextview);
		this.totalDistanceTextView = (TextView) this
				.findViewById(R.id.analysis_totaldistancetextview);
		this.averageSpeedTextView = (TextView) this
				.findViewById(R.id.analysis_averagespeedtextview);
		this.altitudeGainTextView = (TextView) this
				.findViewById(R.id.analysis_altitudegain);
		this.altitudeLossTextView = (TextView) this
				.findViewById(R.id.analysis_altitudeloss);
		this.stepsTextView = (TextView) this
				.findViewById(R.id.analysis_stepcount);
		this.caloriesTextView = (TextView) this
				.findViewById(R.id.analysis_calories);

		this.mapView = (MapView) this.findViewById(R.id.analysis_mapview);
		this.mapView.setBuiltInZoomControls(true);
		this.mapView.setSatellite(true);
		this.myOverlay = new GPXOverlay();
		this.mapView.getOverlays().add(this.myOverlay);
		this.mapController = this.mapView.getController();
		this.mapController.setZoom(16);

		final LoadingDialog dialog = new LoadingDialog();
		dialog.show(this.getFragmentManager(), "AnalysisActivity-loading");

		this.mapView.getOverlays().clear();
		this.mapView.getOverlays().add(this.myOverlay);
		new AsyncTask<GPXFile, Void, Void>() {

			@Override
			protected Void doInBackground(GPXFile... params) {
				AnalysisActivity.this.myOverlay.setGPX(params[0]);
				AnalysisActivity.this.analyze(params[0]);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				dialog.dismiss();
				AnalysisActivity.this.mapView.getController().animateTo(
						AnalysisActivity.this.myOverlay.getCenter());
				AnalysisActivity.this.mapView.getController().zoomToSpan(
						AnalysisActivity.this.myOverlay.getSpanLatDelta(),
						AnalysisActivity.this.myOverlay.getSpanLonDelta());
			};
		}.execute(gpx);
	}
}
