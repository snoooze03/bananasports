package ch.unibnf.ase.bananasports;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class Splashscreen extends Activity {

	private ImageView imageView;
	private Bitmap bitmap;

	@Override
	public void onBackPressed() {
		// do nothing
		return;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.imageView = new ImageView(this);
		this.imageView.setScaleType(ScaleType.CENTER_CROP);
		super.onCreate(savedInstanceState);
		this.setContentView(this.imageView);
		this.bitmap = Bitmap
				.createBitmap(this.getApplicationContext().getResources()
						.getDisplayMetrics().widthPixels, this
						.getApplicationContext().getResources()
						.getDisplayMetrics().heightPixels,
						Bitmap.Config.ARGB_8888);
		// black
		new Canvas(this.bitmap).drawColor(0xFF000000);
		this.imageView.setImageBitmap(this.bitmap);

		new Thread() {
			@Override
			public void run() {
				Canvas canvas = new Canvas(Splashscreen.this.bitmap);
				Resources res = Splashscreen.this.getApplicationContext()
						.getResources();
				Drawable myImage = res.getDrawable(R.drawable.banana);
				Paint paint = new Paint();
				paint.setColor(0xFFFF0000);
				long start = System.currentTimeMillis();
				Random rand = new Random();
				while (System.currentTimeMillis() - start < 5000) {
					int left = rand.nextInt(canvas.getWidth());
					int top = rand.nextInt(canvas.getHeight());
					int width = rand.nextInt(canvas.getWidth() / 4);
					myImage.setBounds(left, top, left + width, top + width);
					myImage.draw(canvas);
					Splashscreen.this.imageView.postInvalidate();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}

				// enough bananas. start main activity
				Intent i = new Intent(
						Splashscreen.this.getApplicationContext(),
						MainActivity.class);
				Splashscreen.this.startActivity(i);

			};
		}.start();
	}
}
