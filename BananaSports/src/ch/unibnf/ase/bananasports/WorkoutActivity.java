package ch.unibnf.ase.bananasports;

import java.util.LinkedList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;
import ch.unibnf.ase.bananasports.gpx.GPXComposer;
import ch.unibnf.ase.bananasports.gpx.GPXDOMParser;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.gpx.GPXTrackpointEntry;
import ch.unibnf.ase.bananasports.map.GPXOverlay;
import ch.unibnf.ase.bananasports.util.GPSRecorder;
import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;
import ch.unibnf.ase.bananasports.util.SaveAnalyzeHelper;
import ch.unibnf.ase.bananasports.util.Stopwatch;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;

public class WorkoutActivity extends MapActivity {

	private class LiveTrackOverlay extends Overlay {

		private static final int POINTS_ON_MAP = 100;
		private final Paint paint = new Paint();

		public LiveTrackOverlay() {
			this.paint.setColor(0xFF00FF00);
			this.paint.setStrokeWidth(2);
			this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
			this.paint.setDither(true);
		}

		@Override
		public void draw(Canvas canvas, MapView mapview, boolean shadow) {
			if (WorkoutActivity.this.trackpoints == null) {
				return;
			}
			if (shadow) {
				return;
			}
			LinkedList<Location> locations = WorkoutActivity.this.trackpoints;
			int delta = 1;

			int points = locations.size();
			delta = points / POINTS_ON_MAP;
			delta = (delta <= 0 ? 1 : delta);
			Point last = null;
			for (int i = 0; i < points; i += delta) {
				Location loc = locations.get(i);
				GeoPoint gp = new GeoPoint((int) (loc.getLatitude() * 1E6),
						(int) (loc.getLongitude() * 1E6));
				Point screenPts = new Point();
				mapview.getProjection().toPixels(gp, screenPts);
				canvas.drawCircle(screenPts.x, screenPts.y, 0.5f, this.paint);
				if (last != null) {
					Path p = new Path();
					p.moveTo(last.x, last.y);
					p.lineTo(screenPts.x, screenPts.y);
					canvas.drawPath(p, this.paint);
				}
				last = screenPts;
			}
			return;
		}
	}

	public static final String GPXFILE = "Workoutactivity_GPXFILE";

	private TextView timerTextView;
	private Stopwatch timer;
	private GPSRecorder gpsRecorder;
	private MapView mapView;

	private MyLocationOverlay myLocationOverlay;
	private LiveTrackOverlay liveTrackOverlay;
	private GPXOverlay plannedTrackOverlay;

	final int MSG_START_TIMER = 0;
	final int MSG_STOP_TIMER = 1;
	final int MSG_UPDATE_TIMER = 2;
	final int MSG_PAUSE_TIMER = 3;

	final int REFRESH_RATE = 50;
	/** trackpoints of current */
	private LinkedList<Location> trackpoints;

	final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case MSG_START_TIMER:
				WorkoutActivity.this.timer.start(); // start timer
				WorkoutActivity.this.gpsRecorder.start();
				WorkoutActivity.this.mHandler
						.sendEmptyMessage(WorkoutActivity.this.MSG_UPDATE_TIMER);
				break;
			case MSG_UPDATE_TIMER:

				WorkoutActivity.this.trackpoints = WorkoutActivity.this.gpsRecorder
						.getLocations();
				long elapsed = WorkoutActivity.this.timer.getElapsedTime();
				WorkoutActivity.this.setTime(elapsed);
				// update again later
				WorkoutActivity.this.mHandler.sendEmptyMessageDelayed(
						WorkoutActivity.this.MSG_UPDATE_TIMER,
						WorkoutActivity.this.REFRESH_RATE);
				break;
			case MSG_STOP_TIMER:
				WorkoutActivity.this.mHandler
						.removeMessages(WorkoutActivity.this.MSG_UPDATE_TIMER); // no
																				// more
																				// updates.
				WorkoutActivity.this.timer.stop();// stop timer
				WorkoutActivity.this.gpsRecorder.stop();
				WorkoutActivity.this.setTime(WorkoutActivity.this.timer
						.getElapsedTime());
				break;
			case MSG_PAUSE_TIMER:
				WorkoutActivity.this.timer.pause();
				WorkoutActivity.this.gpsRecorder.pause();
			default:
				break;
			}
		}
	};

	private final BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent batteryStatus) {
			int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL,
					-1);
			int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE,
					-1);

			float batteryPct = level / (float) scale;
			Toast.makeText(context, "BatteryLevel: " + batteryPct,
					Toast.LENGTH_LONG).show();

			Log.v("BatteryLevel", "" + batteryPct);

			if (batteryPct > 0.5) {
				WorkoutActivity.this.gpsRecorder.adaptSamplingRate(10000);
			}
			if ((0.5 >= batteryPct) && (batteryPct > 0.3)) {
				WorkoutActivity.this.gpsRecorder.adaptSamplingRate(20000);
			}
			if ((0.3 >= batteryPct) && (batteryPct > 0.2)) {
				WorkoutActivity.this.gpsRecorder.adaptSamplingRate(40000);
			}
			if (0.2 >= batteryPct) {
				WorkoutActivity.this.gpsRecorder.adaptSamplingRate(60000);
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.sport_activity);

		this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));

		this.mapView = (MapView) this.findViewById(R.id.sport_mapview);
		this.mapView.setBuiltInZoomControls(true);
		this.liveTrackOverlay = new LiveTrackOverlay();
		this.myLocationOverlay = new MyLocationOverlay(this, this.mapView);
		this.myLocationOverlay.enableMyLocation();
		this.plannedTrackOverlay = new GPXOverlay();
		this.mapView.getOverlays().clear();
		this.mapView.getOverlays().add(this.plannedTrackOverlay);
		this.mapView.getOverlays().add(this.liveTrackOverlay);
		this.mapView.getOverlays().add(this.myLocationOverlay);

		this.timerTextView = (TextView) this.findViewById(R.id.timeTextView);
		this.gpsRecorder = new GPSRecorder(this);
		this.timer = new Stopwatch();

		GPXFile gpx = (GPXFile) this.getIntent().getSerializableExtra(GPXFILE);
		if (gpx != null) {
			final LoadingDialog dialog = new LoadingDialog();
			dialog.show(this.getFragmentManager(), "trackactivity-loadingGPX");
			new AsyncTask<GPXFile, Void, Void>() {

				@Override
				protected Void doInBackground(GPXFile... params) {
					WorkoutActivity.this.plannedTrackOverlay.setGPX(params[0]);
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					dialog.dismiss();
					WorkoutActivity.this.mapView.getController().animateTo(
							WorkoutActivity.this.plannedTrackOverlay
									.getCenter());
					WorkoutActivity.this.mapView
							.getController()
							.zoomToSpan(
									WorkoutActivity.this.plannedTrackOverlay
											.getSpanLatDelta(),
									WorkoutActivity.this.plannedTrackOverlay
											.getSpanLonDelta());
				};
			}.execute(gpx);
		}
	}

	public void pauseButtonClick(View view) {
		this.mHandler.sendEmptyMessage(this.MSG_PAUSE_TIMER);
	}

	public void startButtonClick(View view) {
		this.mHandler.sendEmptyMessage(this.MSG_START_TIMER);
	}

	public void stopButtonClick(View view) {
		this.mHandler.sendEmptyMessage(this.MSG_STOP_TIMER);

		this.showSaveDialog();
	}

	/**
	 * 
	 * @param sport
	 *            true=Running, false=Cycling
	 */
	private void saveTrack(boolean sport) {

		LinkedList<GPXTrackpointEntry> track = new LinkedList<GPXTrackpointEntry>();
		for (Location location : this.trackpoints) {
			GPXTrackpointEntry entry = new GPXTrackpointEntry();
			entry.setElevation(location.getAltitude());
			entry.setTime(location.getTime());
			entry.setLatitude(location.getLatitude());
			entry.setLongitude(location.getLongitude());
			track.add(entry);
		}

		// TODO compose and send gpx file to GPXImporter

		String gpxString = GPXComposer.composeTrackGPX(track);

		GPXFile gpxfile = GPXDOMParser.parseGPX("track_"
				+ new ISO8601TimeHelper().getString(System.currentTimeMillis())
				+ ".gpx", gpxString);
		Intent i = new Intent(this.getApplicationContext(),
				AnalysisActivity.class);
		i.putExtra(AnalysisActivity.GPXFILE, gpxfile);
		this.startActivity(i);

		SaveAnalyzeHelper.save(this.getApplicationContext(),
				(sport ? SaveAnalyzeHelper.Sport.Running
						: SaveAnalyzeHelper.Sport.Cycling), gpxString, true);
	}

	private void setTime(long elapsed) {
		long milliseconds = elapsed % 1000;
		long seconds = elapsed / 1000 % 60;
		long minutes = elapsed / 1000 / 60 % 60;
		long hours = elapsed / 1000 / 60 / 60;
		WorkoutActivity.this.timerTextView.setText(String.format(
				"%02d:%02d:%02d:%03d", hours, minutes, seconds, milliseconds));
	}

	private void showSaveDialog() {
		if (this.trackpoints != null && this.trackpoints.size() > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false);
			builder.setTitle("Close");
			builder.setMessage("Do you want to save your recorded track?");
			builder.setPositiveButton("Yes (Running)",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							Log.v("dialog", "yes");
							WorkoutActivity.this.saveTrack(true);
							WorkoutActivity.this.finish();
						}
					});
			builder.setNeutralButton("Yes (Cycling)",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							Log.v("dialog", "yes");
							WorkoutActivity.this.saveTrack(false);
							WorkoutActivity.this.finish();
						}
					});
			builder.setNegativeButton("No",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							Log.v("dialog", "no");
							WorkoutActivity.this.finish();
						}
					});
			Dialog d = builder.create();
			d.setCanceledOnTouchOutside(false);
			d.show();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	protected void onDestroy() {
		// close that. otherwise we have a resource leak
		this.unregisterReceiver(this.mBatInfoReceiver);
		super.onDestroy();
	}
}
