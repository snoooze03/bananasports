package ch.unibnf.ase.bananasports;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import ch.unibnf.ase.bananasports.entities.Entity;

public abstract class DialogOnClickListener implements OnClickListener {

	private static final String NEGATIVE_TEXT = "Cancel";

	protected Entity entity;

	protected BaseExpandableListAdapter adapter;

	protected int groupId;

	protected View view;

	public DialogOnClickListener(Entity entity, BaseExpandableListAdapter a,
			int groupId, View view) {
		this.entity = entity;
		this.adapter = a;
		this.groupId = groupId;
		this.view = view;
	}

	protected void createDialog(String title, String userQuery,
			String confirmText, View v) {
		AlertDialog.Builder builder = new Builder(v.getContext());
		builder.setTitle(title)
				.setMessage(userQuery)
				.setNegativeButton(NEGATIVE_TEXT,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						})
				.setPositiveButton(confirmText,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								DialogOnClickListener.this.onConfirm();
							}
						}).show();
	}

	protected abstract void onConfirm();

}
