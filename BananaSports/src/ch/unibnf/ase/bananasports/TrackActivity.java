package ch.unibnf.ase.bananasports;

import android.os.AsyncTask;
import android.os.Bundle;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.map.GPXOverlay;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class TrackActivity extends MapActivity {

	public static final String GPXFILE = "trackactivity_GPXFILE";

	private MapView mapView;

	private GPXOverlay gpxOverlay;

	private boolean init = true;

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.maps);

		this.mapView = (MapView) this.findViewById(R.id.mapview);
		this.mapView.setSatellite(true);
		this.mapView.setBuiltInZoomControls(true);

		GPXFile gpx = (GPXFile) this.getIntent().getSerializableExtra(GPXFILE);
		this.gpxOverlay = new GPXOverlay();
		this.mapView.getOverlays().clear();
		this.mapView.getOverlays().add(this.gpxOverlay);
		if (this.init) {
			final LoadingDialog dialog = new LoadingDialog();
			dialog.show(this.getFragmentManager(), "trackactivity-loadingGPX");
			new AsyncTask<GPXFile, Void, Void>() {

				@Override
				protected Void doInBackground(GPXFile... params) {
					TrackActivity.this.gpxOverlay.setGPX(params[0]);
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					dialog.dismiss();
					TrackActivity.this.mapView.getController().animateTo(
							TrackActivity.this.gpxOverlay.getCenter());
					TrackActivity.this.mapView.getController().zoomToSpan(
							TrackActivity.this.gpxOverlay.getSpanLatDelta(),
							TrackActivity.this.gpxOverlay.getSpanLonDelta());
					TrackActivity.this.init = false;
				};

			}.execute(gpx);
		}
	}
}