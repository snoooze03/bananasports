package ch.unibnf.ase.bananasports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import ch.unibnf.ase.bananasports.PartnershipParent.Status;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.expandableList.TrackListChild;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class BananaSocialActivity extends ExpandableListActivity implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -984230804383720414L;
	public static final String ADAPTER = null;

	public static PartnershipParent createPartnershipParent(Partnership p,
			Context context) {
		PartnershipParent parent = new PartnershipParent();
		parent.setPartnerName(p.isOwnerLocal() ? p.getPartner() : p.getOwner());
		parent.setPartnership(p);

		Status status;
		if (p.hasOwnerConfirmed() && p.hasPartnerConfirmed()) {
			status = Status.ACTIVE;
		} else if ((p.isOwnerLocal() && !p.hasOwnerConfirmed())
				|| (!p.isOwnerLocal() && !p.hasPartnerConfirmed())) {
			status = Status.PENDING;
		} else {
			status = Status.AWAITING;
		}
		parent.setStatus(status);

		parent.setChildren(createChildren(p, context));

		return parent;
	}

	private static List<TrackListChild> createChildren(Partnership p,
			Context context) {
		List<TrackListChild> children = new ArrayList<TrackListChild>();
		BananaDatabase bananaDatabase = new BananaDatabase(context);
		EntryFileDAO dao = new EntryFileDAO(bananaDatabase);
		AnalysisEntryDAO analysisDAO = new AnalysisEntryDAO(bananaDatabase);
		for (EntryFile file : dao.GetEntryFiles()) {
			String user1 = p.getOwner() + ";" + p.getPartner();
			String user2 = p.getPartner() + ";" + p.getOwner();

			if (!(user1.equals(file.username) || user2.equals(file.username))) {
				continue;
			}

			TrackListChild child = new TrackListChild();
			AnalysisEntry ae = analysisDAO.getEntryFile(file.getID());
			if (ae == null) {
				continue;
			}
			child.setTime(ae.time);
			child.setAveragePulse(ae.averagepulse);
			child.setDistance(ae.distance);
			child.setDuration(ae.duration);
			child.setElevationLoss(ae.elevationloss);
			child.setEleveationGain(ae.elevationgain);
			child.setIdFile(ae.idfile + "");
			child.setSport(file.sport);
			children.add(child);
		}

		bananaDatabase.close();

		return children;
	}

	private PartnershipsAdatper partnershipAdapter;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.getMenuInflater().inflate(R.menu.add_partnership_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.add_partnership_menu) {
			Intent i = new Intent(this.getApplicationContext(),
					AddPartnershipActivity.class);
			i.putExtra(ADAPTER, this.partnershipAdapter);
			this.startActivity(i);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private List<PartnershipParent> getPartnerships() {
		List<PartnershipParent> groups = new ArrayList<PartnershipParent>();
		String username = ((User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER))
				.getUsername();
		List<Partnership> partnerships = RESTConnector.getPartnershipsFull(
				username, 3000);
		for (Partnership p : partnerships) {
			PartnershipParent parent = createPartnershipParent(p,
					this.getApplicationContext());
			groups.add(parent);
		}
		return groups;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.expandable_list);
		this.setTitle("BananaSports - Partnerships");
		this.partnershipAdapter = new PartnershipsAdatper(this,
				this.getPartnerships());
		this.setListAdapter(this.partnershipAdapter);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		this.partnershipAdapter.notifyDataSetChanged();
	}
}
