package ch.unibnf.ase.bananasports.honeypot;

import java.util.HashMap;

public class HoneyPot {
	private static HashMap<HoneyPotKey, Object> map;

	static {
		map = new HashMap<HoneyPotKey, Object>();
	}

	public static Object getItem(HoneyPotKey key) {
		return map.get(key);
	}

	public static void setItem(HoneyPotKey key, Object object) {
		map.put(key, object);
	}
}
