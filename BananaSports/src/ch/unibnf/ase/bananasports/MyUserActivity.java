package ch.unibnf.ase.bananasports;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.connector.HTTPResponseCode;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;
import ch.unibnf.ase.bananasports.twitter.TwitterHelper;

public class MyUserActivity extends Activity {
	public void bananasportsSettingsSaveClick(View view) {
		EditText password = (EditText) this
				.findViewById(R.id.settings_bananasports_password);
		CheckBox keepLoggedIn = (CheckBox) this
				.findViewById(R.id.settings_bananasports_keeploggedin);
		CheckBox autosynch = (CheckBox) this
				.findViewById(R.id.settings_bananasports_autosync);
		BananaDatabase db = new BananaDatabase(this);
		SettingsDAO dao = new SettingsDAO(db);
		Settings settings = dao.getSettings();

		// has pw changed?
		String oldpassword = settings.getValue(Key.PASSWORD);
		if (oldpassword.equals(password.getText().toString())) {
			// nothing to do
		} else {
			// change pw online: if ok, write to db, else fail!
			User user = (User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER);
			user.setOldPassword(user.getPassword());
			user.setPassword(password.getText().toString());
			if (HTTPResponseCode.OK == RESTConnector.updateUser(user, 3000)) {
				// ok, we can write this value to db
				settings.put(Key.PASSWORD, user.getPassword());
			}
		}
		// keep signedin
		settings.put(Key.KEEPSIGNEDIN, keepLoggedIn.isChecked() + "");
		settings.put(Key.AUTOSYNCH, autosynch.isChecked() + "");
		dao.Save(settings);
		db.close();
	}

	public void garminconnectSettingsSaveClick(View view) {
		String gcUsername = ((EditText) this
				.findViewById(R.id.settings_garminconnect_username)).getText()
				.toString();
		String gcPassword = ((EditText) this
				.findViewById(R.id.settings_garminconnect_password)).getText()
				.toString();

		if (gcUsername.equals("cow") || gcUsername.equals("emu")) {
			this.easteregg(gcUsername);
			return;
		}

		BananaDatabase db = new BananaDatabase(this);
		SettingsDAO dao = new SettingsDAO(db);
		Settings settings = dao.getSettings();

		settings.put(Key.GARMINCONNECT_USERNAME, gcUsername);
		settings.put(Key.GARMINCONNECT_PASSWORD, gcPassword);

		dao.Save(settings);
		db.close();

	}

	public void twitterSettingsSaveClick(View view) {
		// save key
		final String[] result = new String[2];
		new Thread() {
			@Override
			public void run() {
				TwitterHelper.authorize(MyUserActivity.this, result);

				BananaDatabase db = new BananaDatabase(MyUserActivity.this);
				SettingsDAO dao = new SettingsDAO(db);
				Settings settings = dao.getSettings();

				settings.put(Key.TWITTER_AUTHTOKEN, result[0]);
				settings.put(Key.TWITTER_AUTHTOKENKEY, result[1]);

				dao.Save(settings);
				db.close();
				MyUserActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						((TextView) MyUserActivity.this
								.findViewById(R.id.settings_twitter_linked))
								.setText("Twitter account linked");
						((TextView) MyUserActivity.this
								.findViewById(R.id.settings_twitter_linked))
								.invalidate();
					}
				});
				TwitterHelper.tweet(result[0], result[1],
						"now using bananasports. Yay!");
			}
		}.start();
	}

	private void easteregg(String s) {
		ImageView view = new ImageView(this);
		view.setImageDrawable(this.getResources().getDrawable(
				s.equals("cow") ? R.drawable.cow : R.drawable.emu));
		new AlertDialog.Builder(this).setView(view).create().show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.myuser_activity);

		BananaDatabase db = new BananaDatabase(this);
		SettingsDAO dao = new SettingsDAO(db);
		Settings settings = dao.getSettings();
		db.close();

		// load values
		((TextView) this.findViewById(R.id.settings_bananasports_username))
				.setText(settings.getValue(Key.USERNAME));
		((EditText) this.findViewById(R.id.settings_bananasports_password))
				.setText(settings.getValue(Key.PASSWORD));
		((CheckBox) this.findViewById(R.id.settings_bananasports_keeploggedin))
				.setChecked(Boolean.parseBoolean(settings
						.getValue(Key.KEEPSIGNEDIN)));
		((CheckBox) this.findViewById(R.id.settings_bananasports_autosync))
				.setChecked(Boolean.parseBoolean(settings
						.getValue(Key.AUTOSYNCH)));

		((EditText) this.findViewById(R.id.settings_garminconnect_username))
				.setText(settings.getValue(Key.GARMINCONNECT_USERNAME));
		((EditText) this.findViewById(R.id.settings_garminconnect_password))
				.setText(settings.getValue(Key.GARMINCONNECT_PASSWORD));

		String twitterAuth = settings.getValue(Key.TWITTER_AUTHTOKEN);
		if (twitterAuth != null) {
			((TextView) this.findViewById(R.id.settings_twitter_linked))
					.setText("Twitter account linked");
		} else {
			((TextView) this.findViewById(R.id.settings_twitter_linked))
					.setText("Twitter not linked");
		}
	}
}
