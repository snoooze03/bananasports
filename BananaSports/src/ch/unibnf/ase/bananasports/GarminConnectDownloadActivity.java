package ch.unibnf.ase.bananasports;

import java.util.LinkedList;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.dialog.LoadingDialog;
import ch.unibnf.ase.bananasports.garminconnect.GarminConnectHelper;
import ch.unibnf.ase.bananasports.garminconnect.GarminTrack;

public class GarminConnectDownloadActivity extends Activity {

	private LinearLayout mainContainer;

	private String gcUsername = "";

	private String gcPassword = "";

	private Button createButton(final GarminTrack track) {
		Button button = new Button(this);
		button.setText(track.getName());
		button.setGravity(Gravity.LEFT);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 1);
		button.setLayoutParams(lp);
		button.setText(String.format(
				Locale.getDefault(),
				"Name: %s\nDescription: %s\nTimestamp: %s\nCalories: %s\nAltitude Gain: %s\nAltitude loss: %s\nDistance: %s\nDuration: %s",
				track.getName(), track.getDescription(), track.getTimestamp(),
				track.getCalories(), track.getAltitudeGain(),
				track.getAltitudeLoss(), track.getDistance(),
				track.getDuration()));
		button.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				final LoadingDialog dialog = new LoadingDialog();
				dialog.show(
						GarminConnectDownloadActivity.this.getFragmentManager(),
						"importing gpx file");
				new Thread() {
					@Override
					public void run() {
						try {
							final String gpx = GarminConnectHelper
									.getGPX(GarminConnectDownloadActivity.this.gcUsername,
											GarminConnectDownloadActivity.this.gcPassword,
											track);
							// Log.v("gc-social", gpx);

							GarminConnectDownloadActivity.this
									.runOnUiThread(new Runnable() {

										public void run() {
											Intent i = new Intent(
													GarminConnectDownloadActivity.this,
													GPXImporterActivity.class);
											i.putExtra(
													GPXImporterActivity.GPX_IMPORT,
													gpx);
											if (track.isCycling()) {
												i.putExtra(
														GPXImporterActivity.GPX_IMPORT_SPORT,
														false);
											}
											if (track.isRunning()) {
												i.putExtra(
														GPXImporterActivity.GPX_IMPORT_SPORT,
														true);
											}
											GarminConnectDownloadActivity.this
													.startActivity(i);
											dialog.dismiss();
										}
									});
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}.start();
			}
		});
		return button;
	}

	private void loadTracks() {
		final LoadingDialog dialog = new LoadingDialog();
		dialog.show(this.getFragmentManager(), "garminconnect tracks loading");
		new AsyncTask<Void, Void, LinkedList<GarminTrack>>() {
			@Override
			protected LinkedList<GarminTrack> doInBackground(Void... params) {
				BananaDatabase db = new BananaDatabase(
						GarminConnectDownloadActivity.this
								.getApplicationContext());
				Settings settings = new SettingsDAO(db).getSettings();
				GarminConnectDownloadActivity.this.gcUsername = settings
						.getValue(Key.GARMINCONNECT_USERNAME);
				GarminConnectDownloadActivity.this.gcPassword = settings
						.getValue(Key.GARMINCONNECT_PASSWORD);
				db.close();
				return GarminConnectHelper.GetTracks(
						GarminConnectDownloadActivity.this.gcUsername,
						GarminConnectDownloadActivity.this.gcPassword);
			}

			@Override
			protected void onPostExecute(LinkedList<GarminTrack> result) {
				for (GarminTrack track : result) {
					GarminConnectDownloadActivity.this.mainContainer
							.addView(GarminConnectDownloadActivity.this
									.createButton(track));
				}
				dialog.dismiss();
			};
		}.execute();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.garminconnectdownload_activity);

		this.mainContainer = (LinearLayout) this
				.findViewById(R.id.garminconnectdownload_main);

		this.loadTracks();
	}
}
