package ch.unibnf.ase.bananasports.map;

import java.util.LinkedList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.gpx.GPXRoutepointEntry;
import ch.unibnf.ase.bananasports.gpx.GPXTrackpointEntry;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class GPXOverlay extends Overlay {

	/**
	 * At the time i use 1 minute
	 */
	private static final long CLOSE_RANGE = 60000;
	private LinkedList<GeoPoint> list;
	private final Paint paint;
	private GeoPoint center;
	private int spanLatDelta;
	private int spanLonDelta;

	public GPXOverlay() {
		this.paint = new Paint();
		this.paint.setColor(0xFFFF0000);
		this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
		this.paint.setDither(true);
		this.paint.setStrokeWidth(2);
	}

	@Override
	public void draw(Canvas canvas, MapView mapview, boolean shadow) {
		if (shadow) {
			return;
		}
		if (this.list == null) {
			return;
		}
		Point last = null;
		for (GeoPoint gp : this.list) {
			Point screenPts = new Point();
			mapview.getProjection().toPixels(gp, screenPts);
			canvas.drawCircle(screenPts.x, screenPts.y, 1, this.paint);
			if (last != null) {
				Path p = new Path();
				p.moveTo(last.x, last.y);
				p.lineTo(screenPts.x, screenPts.y);
				canvas.drawPath(p, this.paint);
			}
			last = screenPts;
		}
		return;
	}

	public GeoPoint getCenter() {
		return this.center;
	}

	public int getSpanLatDelta() {
		return this.spanLatDelta;
	}

	public int getSpanLonDelta() {
		return this.spanLonDelta;
	}

	/**
	 * Do not forget to call {@link #MapView.invalidate} after setting!
	 * 
	 * @param gpx
	 */
	public void setGPX(GPXFile gpx) {
		this.list = this.createPoints(gpx);
		this.calculateCenterAndSpan();
	}

	private void calculateCenterAndSpan() {
		// set min and max for two points
		int nwLat = -90 * 1000000;
		int nwLng = 180 * 1000000;
		int seLat = 90 * 1000000;
		int seLng = -180 * 1000000;
		// find bounding lats and lngs
		for (GeoPoint point : this.list) {
			nwLat = Math.max(nwLat, point.getLatitudeE6());
			nwLng = Math.min(nwLng, point.getLongitudeE6());
			seLat = Math.min(seLat, point.getLatitudeE6());
			seLng = Math.max(seLng, point.getLongitudeE6());
		}
		GeoPoint center = new GeoPoint((nwLat + seLat) / 2, (nwLng + seLng) / 2);
		// add padding in each direction
		int spanLatDelta = (int) (Math.abs(nwLat - seLat) * 1.1);
		int spanLngDelta = (int) (Math.abs(seLng - nwLng) * 1.1);

		// fit map to points
		this.center = center;
		this.spanLatDelta = spanLatDelta;
		this.spanLonDelta = spanLngDelta;
	}

	private LinkedList<GeoPoint> createPoints(GPXFile gpx) {
		LinkedList<GeoPoint> points = new LinkedList<GeoPoint>();

		if (gpx.getTrack() != null) {
			GPXTrackpointEntry last = null;
			for (GPXTrackpointEntry entry : gpx.getTrack()) {
				// always make sure, the last point is painted
				if (entry != gpx.getTrack().getLast() && last != null
						&& (entry.getTime() - last.getTime()) < CLOSE_RANGE) {
					continue;
				}
				points.add(new GeoPoint((int) (entry.getLatitude() * 1E6),
						(int) (entry.getLongitude() * 1E6)));
				last = entry;
			}
		} else if (gpx.getRoute() != null) {
			for (GPXRoutepointEntry entry : gpx.getRoute()) {
				points.add(new GeoPoint((int) (entry.getLatitude() * 1E6),
						(int) (entry.getLongitude() * 1E6)));
			}
		}

		return points;
	}
}