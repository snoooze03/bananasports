package ch.unibnf.ase.bananasports.twitter;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import ch.unibnf.ase.bananasports.R;

public class TwitterHelper {
	public static final String CONSUMERKEY = "KxRTpPsPkBc714e8VEKLOQ";
	public static final String CONSUMERSECRET = "KNtCpIQBTMy8NDBTK4dMcoE7EREBxOJz8gh3wRJYuCs";

	private static String pin;

	/**
	 * Authorizes the user (and this app) to access twitter
	 * 
	 * @param result
	 *            supply 2 slot string array: result[0]=auththoken,
	 *            result[1]=authtokensecret
	 */
	public static boolean authorize(Activity activity, String[] result) {
		try {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(CONSUMERKEY)
					.setOAuthConsumerSecret(CONSUMERSECRET);
			// .setOAuthAccessToken("**************************************************")
			// .setOAuthAccessTokenSecret("******************************************");
			TwitterFactory tf = new TwitterFactory(cb.build());
			Twitter twitter = tf.getInstance();
			RequestToken requestToken = twitter.getOAuthRequestToken();
			Log.v("twitter", requestToken.getAuthorizationURL());
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken
					.getAuthorizationURL()));
			activity.startActivity(i);

			pin = null;
			AccessToken accessToken = null;
			while (accessToken == null) {
				readPin(activity);
				while (pin == null) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				try {
					if (pin.length() > 0) {
						accessToken = twitter.getOAuthAccessToken(requestToken,
								pin);
					} else {
						accessToken = twitter.getOAuthAccessToken(requestToken);
					}
				} catch (TwitterException ex) {
					ex.printStackTrace();
					accessToken = null;
				}
			}
			Log.v("twitterhelpre", accessToken.toString());
			result[0] = accessToken.getToken();
			result[1] = accessToken.getTokenSecret();
			return true;
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean tweet(String token, String tokensecret, String tweet) {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey(CONSUMERKEY)
				.setOAuthConsumerSecret(CONSUMERSECRET)
				.setOAuthAccessToken(token)
				.setOAuthAccessTokenSecret(tokensecret);
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		try {
			twitter.updateStatus(tweet);
			return true;
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return false;
	}

	private static void readPin(final Activity activity) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				LayoutInflater inflater = activity.getLayoutInflater();
				final View myView = inflater.inflate(
						R.layout.singleeditview_dialog, null);
				final EditText textbox = (EditText) myView
						.findViewById(R.id.pinEditView);
				builder.setTitle("Enter Pin");
				builder.setMessage("Enter your twitter pin, or hit OK if none given");
				builder.setView(myView);

				builder.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								TwitterHelper.pin = textbox.getText()
										.toString();
							}
						});
				final AlertDialog alert = builder.create();
				alert.setCancelable(false);
				alert.setCanceledOnTouchOutside(false);
				alert.show();
			}
		});

	}
}
