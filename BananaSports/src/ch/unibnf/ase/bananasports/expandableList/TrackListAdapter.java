package ch.unibnf.ase.bananasports.expandableList;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.R;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.garminconnect.GarminConnectHelper;
import ch.unibnf.ase.bananasports.util.Helper;

public class TrackListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<TrackListParent> groups;
	private TrackListActivity activity;

	public TrackListAdapter(Context context, List<TrackListParent> groups) {
		this.context = context;
		this.groups = groups;
		this.activity = (TrackListActivity) context;
	}

	public Object getChild(int groupPosition, int childPosition) {
		return this.groups.get(groupPosition).getChild();
	}

	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	public int getChildrenCount(int groupPosition) {
		return this.groups.get(groupPosition).getChild() == null ? 0 : 1;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		String groupId = this.groups.get(groupPosition).getId();
		TrackListChild child = this.groups.get(groupPosition).getChild();
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.child_row, null);
		}

		TextView sport = (TextView) view.findViewById(R.id.child_row_sport);
		sport.setTag("sport_" + groupPosition + "." + child.getId());
		sport.setText(this.groups.get(groupPosition).getChild().getSport());

		TextView label = (TextView) view.findViewById(R.id.child_row_time);
		label.setTag("label_time_" + groupId + "." + child.getId());
		label.setText(R.string.time_label);
		TextView value = (TextView) view
				.findViewById(R.id.child_row_time_value);
		value.setTag("value_time_" + groupId + "." + child.getId());
		value.setText(child.getTime());

		label = (TextView) view.findViewById(R.id.child_row_duration);
		label.setTag("label_duration_" + groupId + "." + child.getId());
		label.setText(R.string.duration_label);
		value = (TextView) view.findViewById(R.id.child_row_duration_value);
		value.setTag("value_duration_" + groupId + "." + child.getId());
		value.setText(child.getDuration());

		label = (TextView) view.findViewById(R.id.child_row_distance);
		label.setTag("label_distance_" + groupId + "." + child.getId());
		label.setText(R.string.distance_label);
		value = (TextView) view.findViewById(R.id.child_row_distance_value);
		value.setTag("value_distance_" + groupId + "." + child.getId());
		value.setText(child.getDistance());

		label = (TextView) view.findViewById(R.id.child_row_elevation_gain);
		label.setTag("label_alt_gain_" + groupId + "." + child.getId());
		label.setText(R.string.alt_gain_label);
		value = (TextView) view
				.findViewById(R.id.child_row_elevation_gain_value);
		value.setTag("value_alt_gain_" + groupId + "." + child.getId());
		value.setText(child.getEleveationGain());

		label = (TextView) view.findViewById(R.id.child_row_elevation_loss);
		label.setTag("label_alt_loss_" + groupId + "." + child.getId());
		label.setText(R.string.alt_loss_label);
		value = (TextView) view
				.findViewById(R.id.child_row_elevation_loss_value);
		value.setTag("value_alt_loss_" + groupId + "." + child.getId());
		value.setText(child.getElevationLoss());

		label = (TextView) view.findViewById(R.id.child_row_avg_pulse);
		label.setTag("label_avg_pulse_" + groupId + "." + child.getId());
		label.setText(R.string.avg_pulse_label);
		value = (TextView) view.findViewById(R.id.child_row_avg_pulse_value);
		value.setTag("value_avg_pulse_" + groupId + "." + child.getId());
		value.setText(child.getAveragePulse());

		Button shareButton = (Button) view
				.findViewById(R.id.child_row_share_button);
		shareButton.setOnClickListener(this.activity.new ShareClickListener(
				groupPosition));

		Button analyseButton = (Button) view
				.findViewById(R.id.child_row_analyse_button);
		analyseButton
				.setOnClickListener(this.activity.new AnalyseClickListener(
						groupPosition));
		if (child.getIdFile() != null) {
			final int id = Integer.parseInt(child.getIdFile());
			Button garminButton = (Button) view
					.findViewById(R.id.child_row_uploadgarmin);
			garminButton.setActivated(true);
			garminButton.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					try {
						BananaDatabase db = new BananaDatabase(v.getContext());
						final Settings settings = new SettingsDAO(db)
								.getSettings();
						EntryFileDAO dao = new EntryFileDAO(db);
						final EntryFile ef = dao.getFileById(id);
						dao.LoadTrack(ef);
						db.close();
						new Thread() {
							@Override
							public void run() {
								String[] result = new String[2];
								GarminConnectHelper.upload(
										settings.getValue(Key.GARMINCONNECT_USERNAME),
										settings.getValue(Key.GARMINCONNECT_PASSWORD),
										Helper.unzipBase64String(ef.gpx),
										result);
								try {
									// open the garmin connect page in browser
									// on success of upload
									Intent i = new Intent(Intent.ACTION_VIEW,
											Uri.parse(result[1]));
									((Activity) TrackListAdapter.this.context)
											.startActivity(i);
								} catch (Exception e) {
								}
							}
						}.start();

					} catch (Exception ex) {
					}
				}
			});
		} else {
			((Button) view.findViewById(R.id.child_row_analyse_button))
					.setActivated(false);
		}
		return view;
	}

	public Object getGroup(int groupPosition) {
		return this.groups.get(groupPosition);
	}

	public int getGroupCount() {
		return this.groups.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View view,
			ViewGroup parent) {
		TrackListParent group = this.groups.get(groupPosition);
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.parent_row, null);
		}
		TextView tv = (TextView) view.findViewById(R.id.row_name);
		tv.setText(group.getName());

		return view;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return this.groups.get(groupPosition) != null && childPosition < 2;
	}

	public void startShareActivityClick(View view) {
		BananaDatabase db = new BananaDatabase(view.getContext());
		db.close();
		System.out.println();

		// Intent i = new Intent(view.getContext(), ShareTrackActivity.class);
		// i.putExtra(ID_FILE, value)
	}

}
