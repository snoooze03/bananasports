package ch.unibnf.ase.bananasports.expandableList;

import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.R;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.User;

public class ShareTrackActivity extends Activity implements OnItemClickListener {

	private List<Partnership> partnerships;
	private ArrayAdapter<String> adatper;

	public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
		final String partnerName = (String) ((TextView) view).getText();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setCancelable(false);
		builder.setTitle("Share Track").setMessage(
				"Share Track with user " + partnerName
						+ "? (The track will be uploaded at the next sync)");
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						ShareTrackActivity.this.finish();
						dialog.dismiss();
					}
				})
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						BananaDatabase db = new BananaDatabase(
								ShareTrackActivity.this);
						User user = new SettingsDAO(db).getCurrentUser();
						db.close();
						ShareTrackActivity.this.shareTrack(partnerName, user);
						ShareTrackActivity.this.finish();
						dialog.dismiss();
					}
				}).show();
	}

	private ArrayAdapter<String> createAdapter() {
		BananaDatabase db = new BananaDatabase(this.getApplicationContext());
		User user = new SettingsDAO(db).getCurrentUser();
		db.close();

		this.partnerships = RESTConnector.getPartnerships(user.getUsername(),
				3000);
		for (Iterator<Partnership> i = this.partnerships.iterator(); i
				.hasNext();) {
			Partnership p = i.next();
			p = RESTConnector.readPartnership(p, 3000);
			if (!p.hasOwnerConfirmed() || !p.hasPartnerConfirmed()) {
				i.remove();
			}
		}
		String[] partners = new String[this.partnerships.size()];
		for (Partnership p : this.partnerships) {
			partners[this.partnerships.indexOf(p)] = p.isOwnerLocal() ? p
					.getPartner() : p.getOwner();
		}
		return this.adatper = new ArrayAdapter<String>(
				this.getApplicationContext(),
				android.R.layout.simple_dropdown_item_1line, partners);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.share_partnership_activity);
		AutoCompleteTextView searchBar = (AutoCompleteTextView) this
				.findViewById(R.id.partnership_search_autocomplete);
		searchBar.setAdapter(this.createAdapter());
		searchBar.setOnItemClickListener(this);
		searchBar.setHint("Search Partnership");
	}

	protected void shareTrack(String partnerName, User user) {
		Partnership p = this.partnerships.get(this.adatper
				.getPosition(partnerName));
		BananaDatabase db = new BananaDatabase(this.getApplicationContext());
		EntryFileDAO dao = new EntryFileDAO(db);
		EntryFile entryFile = dao.getFileById(this.getIntent().getIntExtra(
				TrackListActivity.ID_FILE, -1));
		dao.LoadTrack(entryFile);

		EntryFile newEntryFile = new EntryFile();
		newEntryFile.sport = entryFile.sport;
		newEntryFile.gpx = entryFile.gpx;
		String pname = p.getOwner() + ";" + p.getPartner();
		newEntryFile.username = pname;
		dao.Insert(newEntryFile);

		AnalysisEntryDAO anaDao = new AnalysisEntryDAO(db);
		AnalysisEntry anaEntry = anaDao.getEntryFile(entryFile.getID());
		AnalysisEntry newEntry = new AnalysisEntry();
		newEntry.averagepulse = anaEntry.averagepulse;
		newEntry.distance = anaEntry.distance;
		newEntry.duration = anaEntry.duration;
		newEntry.elevationgain = anaEntry.elevationgain;
		newEntry.elevationloss = anaEntry.elevationloss;
		newEntry.time = anaEntry.time;
		newEntry.idfile = newEntryFile.getID();
		newEntry.username = pname;
		anaDao.Insert(newEntry);

		db.close();
	}
}
