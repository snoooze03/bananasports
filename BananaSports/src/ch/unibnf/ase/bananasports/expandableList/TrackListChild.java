package ch.unibnf.ase.bananasports.expandableList;

import java.io.Serializable;
import java.util.Locale;

import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;

public class TrackListChild implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5421699608873579653L;
	private String id;
	private String idFile;
	private long distance;
	private long time;
	private int eleveationGain;
	private int elevationLoss;
	private long duration;
	private int averagePulse;
	private String sport;

	public String getAveragePulse() {
		return this.averagePulse + " bpm";
	}

	public String getDistance() {
		return this.distance + "m";
	}

	public String getDuration() {
		long minutes = (this.duration / 60) % 60;
		long hours = this.duration / 3600;
		long sec = this.duration % 60;
		return (String.format(Locale.getDefault(), "%dh %2dmin %02dsec", hours,
				minutes, sec));
	}

	public String getElevationLoss() {
		return this.elevationLoss + " m";
	}

	public String getEleveationGain() {
		return this.eleveationGain + " m";
	}

	public String getId() {
		return this.id;
	}

	public String getIdFile() {
		return this.idFile;
	}

	public String getSport() {
		return this.sport;
	}

	public String getTime() {
		return (new ISO8601TimeHelper()).getString(this.time);
	}

	public void setAveragePulse(int averagePulse) {
		this.averagePulse = averagePulse;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public void setElevationLoss(int elevationLoss) {
		this.elevationLoss = elevationLoss;
	}

	public void setEleveationGain(int eleveationGain) {
		this.eleveationGain = eleveationGain;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setIdFile(String idFile) {
		this.idFile = idFile;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
