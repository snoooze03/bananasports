package ch.unibnf.ase.bananasports.expandableList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import ch.unibnf.ase.bananasports.AnalysisActivity;
import ch.unibnf.ase.bananasports.R;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.gpx.GPXDOMParser;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.util.Helper;
import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;

public class TrackListActivity extends ExpandableListActivity {

	public class AnalyseClickListener implements OnClickListener {

		private int position;

		public AnalyseClickListener(int postition) {
			this.position = postition;
		}

		public void onClick(View v) {
			Intent i = new Intent(
					TrackListActivity.this.getApplicationContext(),
					AnalysisActivity.class);
			EntryFile file = TrackListActivity.this.entries.get(this.position);
			BananaDatabase db = new BananaDatabase(
					TrackListActivity.this.getApplicationContext());
			if (!new EntryFileDAO(db).LoadTrack(file)) {
				return;
			}
			String gpx = Helper.unzipBase64String(file.gpx);
			GPXFile gpxFile = GPXDOMParser.parseGPX(
					"track_"
							+ new ISO8601TimeHelper()
									.getStringThreadSafe(System
											.currentTimeMillis()), gpx);
			i.putExtra(AnalysisActivity.GPXFILE, gpxFile);
			TrackListActivity.this.startActivity(i);

		}
	}

	public class ShareClickListener implements OnClickListener {

		private int position;

		public ShareClickListener(int position) {
			this.position = position;

		}

		public void onClick(View v) {
			Intent i = new Intent(
					TrackListActivity.this.getApplicationContext(),
					ShareTrackActivity.class);
			EntryFile file = TrackListActivity.this.entries.get(this.position);
			i.putExtra(TrackListActivity.ID_FILE, file.getID());
			TrackListActivity.this.startActivity(i);
		}

	}

	public static final String ID_FILE = "ID_FILE";

	private TrackListAdapter trackListAdapter;

	private List<EntryFile> entries;

	public void startShareActivityClick(View view) {

		// TODO Check how to reach correct track
		// i.putExtra(TrackActivity.GPXFILE, new GPXDummyProvider().load());
		System.out.println();
	}

	private TrackListChild createChild(EntryFile e, AnalysisEntry entry) {
		TrackListChild child = new TrackListChild();
		child.setAveragePulse(entry.averagepulse);
		child.setDistance(entry.distance);
		child.setDuration(entry.duration);
		child.setElevationLoss(entry.elevationloss);
		child.setEleveationGain(entry.elevationgain);
		child.setSport(e.sport);
		child.setTime(entry.time);
		child.setIdFile("" + entry.idfile);
		return child;
	}

	private List<TrackListParent> getEntries() {
		List<TrackListParent> parents = new ArrayList<TrackListParent>();
		BananaDatabase db = new BananaDatabase(this.getApplicationContext());
		AnalysisEntryDAO dao = new AnalysisEntryDAO(db);
		User user = new SettingsDAO(db).getCurrentUser();
		for (EntryFile e : this.entries) {
			AnalysisEntry entry = dao.getEntryFile(e.getID());
			if (entry == null || !user.getUsername().equals(entry.username)) {
				continue;
			}
			TrackListParent parent = new TrackListParent();
			parent.setDate(entry.time);
			parent.setChild(this.createChild(e, entry));
			parent.setName(e.sport
					+ ", "
					+ new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY)
							.format(entry.time));
			parents.add(parent);
		}
		db.close();
		return parents;
	}

	private void init() {
		BananaDatabase db = new BananaDatabase(this.getApplicationContext());
		this.entries = new EntryFileDAO(db).GetEntryFiles();
		db.close();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.expandable_list);
		this.init();

		// try {
		// Date minDate = new SimpleDateFormat("yyyy-MM-dd")
		// .parse("1950-12-01");
		// Date maxDate = new Date();
		// RangeSeekBar<Long> seekBar = new RangeSeekBar<Long>(
		// minDate.getTime(), maxDate.getTime(), this);
		// seekBar.setOnRangeSeekBarChangeListener(new
		// OnRangeSeekBarChangeListener<Long>() {
		//
		// public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar,
		// Long minValue, Long maxValue) {
		// Log.i(this.getClass().getCanonicalName(),
		// "User selected new date range: MIN="
		// + new Date(minValue) + ", MAX="
		// + new Date(maxValue));
		// }
		//
		// });
		//
		// LinearLayout layout = (LinearLayout) this
		// .findViewById(R.id.expandable_list);
		// LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.WRAP_CONTENT);
		// params.topMargin = 10;
		// layout.addView(seekBar, 0, params);
		//
		// } catch (ParseException e) {
		// e.printStackTrace();
		// }

		this.trackListAdapter = new TrackListAdapter(this, this.getEntries());
		this.setListAdapter(this.trackListAdapter);
	}
}
