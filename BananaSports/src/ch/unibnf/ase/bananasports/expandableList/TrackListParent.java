package ch.unibnf.ase.bananasports.expandableList;

public class TrackListParent {

	private String id;
	private String name;
	private long date;
	private TrackListChild child;
	private String sport;

	public TrackListChild getChild() {
		return this.child;
	}

	public long getDate() {
		return this.date;
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getSport() {
		return this.sport;
	}

	public void setChild(TrackListChild child) {
		this.child = child;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

}
