package ch.unibnf.ase.bananasports;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.entities.Partnership;

public class ButtonListAdapter extends ArrayAdapter<Partnership> {

	private int layoutResourceId;
	private Context context;
	private Partnership[] data;

	public ButtonListAdapter(Context context, int textViewResourceId,
			Partnership[] data) {
		super(context, textViewResourceId, data);
		this.layoutResourceId = textViewResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		PartnershipHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) this.context)
					.getLayoutInflater();
			row = inflater.inflate(this.layoutResourceId, parent, false);
			holder = new PartnershipHolder();
			holder.txtTitle = (TextView) row
					.findViewById(R.id.button_list_item_text);
			holder.button = (Button) row
					.findViewById(R.id.button_list_item_button);

			row.setTag(holder);
		} else {
			holder = (PartnershipHolder) row.getTag();
		}

		Partnership p = this.data[position];
		holder.txtTitle.setText(this.getText(p));
		boolean confirmLocally = !p.isOwnerLocal() && !p.hasPartnerConfirmed();
		holder.button
				.setText(confirmLocally ? R.string.confirm_partnership_label
						: R.string.delete_label);
		// holder.button
		// .setOnClickListener(confirmLocally ? new
		// ConfirmParthershipClickListener(
		// p, this) : new DeletePartnershipClickListener(p, this));
		return row;
	}

	private String getText(Partnership p) {
		String text = p.isOwnerLocal() ? p.getPartner() : p.getOwner();
		if (!p.hasPartnerConfirmed()) {
			if (p.isOwnerLocal()) {
				text += " (awaiting confirmation)";
			} else {
				text += " (pending)";
			}
		}
		return text;
	}

	static class PartnershipHolder {
		TextView txtTitle;
		Button button;
	}

}
