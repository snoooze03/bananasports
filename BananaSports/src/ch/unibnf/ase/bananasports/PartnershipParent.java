package ch.unibnf.ase.bananasports;

import java.io.Serializable;
import java.util.List;

import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.expandableList.TrackListChild;

public class PartnershipParent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9221869807378430521L;
	private String partnerName;
	private Status status;
	private List<TrackListChild> children;
	private Partnership partnership;

	public List<TrackListChild> getChildren() {
		return this.children;
	}

	public String getPartnerName() {
		return this.partnerName;
	}

	public Partnership getPartnership() {
		return this.partnership;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setChildren(List<TrackListChild> children) {
		this.children = children;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public void setPartnership(Partnership partnership) {
		this.partnership = partnership;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public enum Status {
		PENDING(" (pending)"), AWAITING(" (awaiting confirmation)"), ACTIVE("");

		private String text;

		private Status(String text) {
			this.text = text;
		}

		public String text() {
			return this.text;
		}
	}

}
