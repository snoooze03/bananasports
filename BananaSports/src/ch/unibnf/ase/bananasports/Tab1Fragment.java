package ch.unibnf.ase.bananasports;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.util.Helper;

public class Tab1Fragment extends Fragment {

	private class CreateSubscriptionButtonListener implements OnClickListener {
		public void onClick(View v) {
			// String username = ((EditText) Tab1Fragment.this.thisFragment
			// .findViewById(R.id.usernameEditText)).getText().toString();
			// String password = ((EditText) Tab1Fragment.this.thisFragment
			// .findViewById(R.id.passwordEditText)).getText().toString();

			// RESTConnector.createSubscription(v.getContext(), username,
			// password, Subscriptions.RUNNING, 2);
			//
			// RESTConnector.logon("007", "1234");

			RESTConnector
					.setProgressBar((ProgressBar) Tab1Fragment.this.thisFragment
							.findViewById(R.id.progressBar));
			RESTConnector.getLongTrack();
		}
	}

	private class CreateUserButtonListener implements OnClickListener {

		public void onClick(View view) {
			String username = ((EditText) Tab1Fragment.this.thisFragment
					.findViewById(R.id.usernameEditText)).getText().toString();
			String email = ((EditText) Tab1Fragment.this.thisFragment
					.findViewById(R.id.emailEditText)).getEditableText()
					.toString();
			String realname = ((EditText) Tab1Fragment.this.thisFragment
					.findViewById(R.id.realnameEditText)).getText().toString();
			String password = ((EditText) Tab1Fragment.this.thisFragment
					.findViewById(R.id.passwordEditText)).getText().toString();
			int publicvisible = 2;

			Log.v("Tab1Fragment.createUserButton", username);
			Log.v("Tab1Fragment.createUserButton", email);
			Log.v("Tab1Fragment.createUserButton", realname);
			Log.v("Tab1Fragment.createUserButton", password);
			Log.v("Tab1Fragment.createUserButton", publicvisible + "");

			User user = new User(username);
			user.setEmail(email);
			user.setPassword(password);
			user.setRealname(realname);

			RESTConnector.createUser(user, 3000);
		}
	}

	private class DBButtonClickListener implements OnClickListener {

		public void onClick(View v) {
			BananaDatabase db = new BananaDatabase(
					Tab1Fragment.this.thisFragment.getContext());
			EntryFileDAO efDAO = new EntryFileDAO(db);
			efDAO.GetEntryFiles();
			EntryFile ef1 = new EntryFile();
			ef1.gpx = "test";
			ef1.serverid = 999;
			ef1.sport = "Running";
			ef1.username = "blubb";
			efDAO.Insert(ef1);

			EntryFile ef2 = new EntryFile();
			ef2.gpx = "test2";
			ef2.serverid = 1000;
			ef2.sport = "Running";
			ef2.username = "blubb";
			efDAO.Insert(ef2);

			ef1.serverid = 1001;
			efDAO.Update(ef1);

			List<EntryFile> l2 = efDAO.GetEntryFiles();
			EntryFile ef3 = l2.get(0);
			efDAO.LoadTrack(ef3);
			Log.v("gpx ", ef3.gpx);
		}
	}

	private class FromBase64ButtonListener implements OnClickListener {

		public void onClick(View v) {
			EditText editText = ((EditText) Tab1Fragment.this.thisFragment
					.findViewById(R.id.base64ZipEditText));
			String s = editText.getText().toString();
			String res = Helper.unzipBase64String(s);
			editText.setText(res);
		}
	}

	@SuppressWarnings("unused")
	private class ToastButtonListener implements OnClickListener {
		public void onClick(View v) {
			Log.v("toastbutton", Helper.createAuthorizationHeaderValue(
					"userdude", "password"));
		}
	}

	private class ToBase64ButtonListener implements OnClickListener {

		public void onClick(View v) {
			EditText editText = ((EditText) Tab1Fragment.this.thisFragment
					.findViewById(R.id.base64ZipEditText));
			String s = editText.getText().toString();
			String res = Helper.base64ZipString(s);
			editText.setText(res);
		}
	}

	private View thisFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		this.thisFragment = inflater.inflate(R.layout.tab1fragment, container,
				false);

		Button createUserButton = (Button) this.thisFragment
				.findViewById(R.id.createUserButton);
		createUserButton.setOnClickListener(new CreateUserButtonListener());

		Button createSubscriptionButton = (Button) this.thisFragment
				.findViewById(R.id.createSubscriptionButton);
		createSubscriptionButton
				.setOnClickListener(new CreateSubscriptionButtonListener());

		Button toBase64Button = (Button) this.thisFragment
				.findViewById(R.id.base64zipButton);
		toBase64Button.setOnClickListener(new ToBase64ButtonListener());

		Button fromBase64Button = (Button) this.thisFragment
				.findViewById(R.id.fromBase64Button);
		fromBase64Button.setOnClickListener(new FromBase64ButtonListener());

		Button dbButton = (Button) this.thisFragment
				.findViewById(R.id.dbButton);
		dbButton.setOnClickListener(new DBButtonClickListener());

		return this.thisFragment;
	}
}
