package ch.unibnf.ase.bananasports.database.dao;

import java.util.HashMap;
import java.util.Iterator;

public class Settings {

	public enum Key {
		KEEPSIGNEDIN("keepsignedin"), USERNAME("username"), PASSWORD("password"), GARMINCONNECT_USERNAME(
				"gcUsername"), GARMINCONNECT_PASSWORD("gcPassword"), TWITTER_AUTHTOKEN(
				"twitterToken"), TWITTER_AUTHTOKENKEY("twitterTokenKey"), AUTOSYNCH(
				"autosynch");

		private String key;

		Key(String key) {
			this.key = key;
		}

		private String getKey() {
			return this.key;
		}
	}

	private static Settings settings;
	private static HashMap<String, String> map;

	/**
	 * Get singleton instance of Settings. You should use
	 * {@link SettingsDAO#getSettings()} to retrieve from database
	 * 
	 * @return Singleton instance of Settings
	 */
	public static synchronized Settings getInstance() {
		if (settings == null) {
			Settings.settings = new Settings();
		}
		return Settings.settings;
	};

	// singleton
	private Settings() {
		Settings.map = new HashMap<String, String>();
	}

	public String getValue(Key key) {
		return this.getValue(key.getKey());
	}

	/**
	 * Get the value for the given key
	 * 
	 * @param key
	 * @return
	 */
	public String getValue(String key) {
		return Settings.map.get(key);
	}

	public void put(Key key, String value) {
		this.put(key.getKey(), value);
	}

	/**
	 * Add key-value pair to Settings
	 * 
	 * @param key
	 * @param value
	 */
	public void put(String key, String value) {
		Settings.map.put(key, value);
	}

	// default
	Iterator<String> getKeyIterator() {
		return Settings.map.keySet().iterator();
	}
}
