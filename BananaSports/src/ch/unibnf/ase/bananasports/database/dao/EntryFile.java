package ch.unibnf.ase.bananasports.database.dao;

import java.util.Locale;

public class EntryFile {

	// default modifier. this should only be set by db
	int id;
	/** server id of this entry */
	public int serverid = -1;
	/** Sport of this entry */
	public String sport;
	/** owner username */
	public String username;
	/** gpx string compressed */
	public String gpx;

	public int getID() {
		return this.id;
	}

	@Override
	public String toString() {
		return String
				.format(Locale.getDefault(),
						"EntryFile: (id=%d, serverid=%s, username=%s, gpx=%s, sport=%s",
						this.id,
						(this.serverid != -1 ? "" + this.serverid : ""),
						(this.username != null ? this.username : ""),
						(this.gpx != null ? this.gpx : ""),
						(this.sport != null ? this.sport : ""));
	}
}
