package ch.unibnf.ase.bananasports.database.dao;

import java.util.LinkedList;

import android.database.Cursor;
import ch.unibnf.ase.bananasports.database.BananaDatabase;

public class AnalysisEntryDAO {

	private final BananaDatabase bananaDatabase;

	public AnalysisEntryDAO(BananaDatabase database) {
		this.bananaDatabase = database;
	}

	public AnalysisEntry getEntryFile(int idFile) {
		String query = "SELECT _id, username, idfile, time, distance, elevationgain, elevationloss, duration, averagepulse FROM analysisentries "
				+ "WHERE idfile='" + idFile + "'";
		Cursor cursor = this.bananaDatabase.getReadableDatabase().rawQuery(
				query, null);
		if (cursor.moveToFirst()) {
			AnalysisEntry entry = createAnalysisEntry(cursor);
			cursor.close();
			return entry;

		}
		cursor.close();
		return null;
	}

	public LinkedList<AnalysisEntry> GetEntryFiles() {
		Cursor cursor = this.bananaDatabase
				.getReadableDatabase()
				.rawQuery(
						"SELECT _id, username, idfile, time, distance, elevationgain, elevationloss, duration, averagepulse FROM analysisentries;",
						null);
		LinkedList<AnalysisEntry> entries = null;

		if (cursor.isBeforeFirst()) {
			entries = new LinkedList<AnalysisEntry>();
			while (cursor.moveToNext()) {
				entries.add(createAnalysisEntry(cursor));
			}
			cursor.close();
		}
		cursor.close();
		return entries;
	}

	public synchronized void Insert(AnalysisEntry analysisEntry)
			throws IllegalArgumentException {
		if (analysisEntry.id == 0) {
			this.bananaDatabase
					.getWritableDatabase()
					.execSQL(
							"INSERT INTO analysisentries (username, idfile, time, distance, elevationgain, elevationloss, duration, averagepulse) VALUES (?,?,?,?,?,?,?,?);",
							new Object[] { analysisEntry.username,
									analysisEntry.idfile, analysisEntry.time,
									analysisEntry.distance,
									analysisEntry.elevationgain,
									analysisEntry.elevationloss,
									analysisEntry.duration,
									analysisEntry.averagepulse });
			// get index
			Cursor c = this.bananaDatabase.getReadableDatabase().rawQuery(
					"SELECT max(_id) FROM analysisentries;", null);
			c.moveToFirst();
			analysisEntry.id = c.getInt(0);
			c.close();
		} else {
			throw new IllegalArgumentException("Entry already exists");
		}
	}

	private AnalysisEntry createAnalysisEntry(Cursor cursor) {
		AnalysisEntry ef = new AnalysisEntry();
		ef.id = cursor.getInt(0);
		ef.username = cursor.getString(1);
		ef.idfile = cursor.getInt(2);
		ef.time = cursor.getLong(3);
		ef.distance = cursor.getInt(4);
		ef.elevationgain = cursor.getInt(5);
		ef.elevationloss = cursor.getInt(6);
		ef.duration = cursor.getInt(7);
		ef.averagepulse = cursor.getInt(8);
		return ef;
	}
}
