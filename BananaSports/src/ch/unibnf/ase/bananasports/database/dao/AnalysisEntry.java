package ch.unibnf.ase.bananasports.database.dao;

public class AnalysisEntry {
	// default modifier - no change
	int id;
	/** index of corresponding fileentry */
	public int idfile;
	/** username of owner */
	public String username;
	/** distance in meter */
	public int distance;
	/** elevation gain in meter */
	public int elevationgain;
	/** elevation loss in meter */
	public int elevationloss;
	/** duration in seconds */
	public int duration;
	/** average pulse */
	public int averagepulse;
	/** time in milliseconds */
	public long time;
}
