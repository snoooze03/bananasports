package ch.unibnf.ase.bananasports.database.dao;

import java.util.Iterator;

import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.entities.User;

public class SettingsDAO {

	private BananaDatabase bananaDatabase;

	public SettingsDAO(BananaDatabase bananaDatabase) {
		this.bananaDatabase = bananaDatabase;
	}

	public User getCurrentUser() {
		Settings settings = this.getSettings();
		String username = settings.getValue(Key.USERNAME);
		User user = new User(username);
		user.setPassword(settings.getValue(Key.PASSWORD));
		return user;
	}

	/**
	 * Reads entries from database to Settings. Will overwrite all existing
	 * values
	 * 
	 * @return Settings instance with all entries read from database
	 */
	public synchronized Settings getSettings() {
		Settings settings = Settings.getInstance();
		Cursor cursor = this.bananaDatabase.getReadableDatabase().rawQuery(
				"SELECT key,value FROM settings;", null);

		while (cursor.moveToNext()) {
			settings.put(cursor.getString(0), cursor.getString(1));
			Log.v("Settings-DB entry",
					cursor.getString(0) + "_" + cursor.getString(1));
		}
		cursor.close();
		return settings;
	}

	public synchronized void Save(Settings settings) {
		for (Iterator<String> i = settings.getKeyIterator(); i.hasNext();) {
			String key = i.next();
			String value = settings.getValue(key);
			Log.v("Settings. Saving", key + "_" + value);
			// try insert: on fail update
			try {
				this.bananaDatabase.getWritableDatabase().execSQL(
						"INSERT INTO settings (key, value) VALUES (?,?);",
						new Object[] { key, value });
			} catch (SQLException ex) {
				// failed: update
				this.bananaDatabase.getWritableDatabase().execSQL(
						"UPDATE settings SET value=? WHERE key=?;",
						new Object[] { value, key });
			}

		}
	}
}
