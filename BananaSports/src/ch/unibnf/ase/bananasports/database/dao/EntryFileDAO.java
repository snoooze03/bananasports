package ch.unibnf.ase.bananasports.database.dao;

import java.util.LinkedList;

import android.database.Cursor;
import ch.unibnf.ase.bananasports.database.BananaDatabase;

public class EntryFileDAO {

	private BananaDatabase bananaDatabase;

	public EntryFileDAO(BananaDatabase bananaDatabase) {
		this.bananaDatabase = bananaDatabase;
	}

	/**
	 * Get the database entries. The tracks are not loaded. Use
	 * 
	 * @return
	 */
	public LinkedList<EntryFile> GetEntryFiles() {
		Cursor cursor = this.bananaDatabase.getReadableDatabase().rawQuery(
				"SELECT _id, serverid, sport, username FROM entryfiles;", null);
		LinkedList<EntryFile> entries = null;

		if (cursor.isBeforeFirst()) {
			entries = new LinkedList<EntryFile>();
			while (cursor.moveToNext()) {
				EntryFile ef = new EntryFile();
				ef.id = cursor.getInt(0);
				ef.serverid = cursor.getInt(1);
				ef.sport = cursor.getString(2);
				ef.username = cursor.getString(3);
				entries.add(ef);
			}
		}
		cursor.close();
		return entries;
	}

	public EntryFile getFileById(int i) {
		Cursor cursor = this.bananaDatabase.getReadableDatabase().rawQuery(
				"SELECT _id, serverid, sport, username FROM entryfiles where _id = '"
						+ i + "';", null);
		if (cursor.moveToFirst()) {
			EntryFile ef = new EntryFile();
			ef.id = cursor.getInt(0);
			ef.serverid = cursor.getInt(1);
			ef.sport = cursor.getString(2);
			ef.username = cursor.getString(3);
			return ef;
		}
		cursor.close();
		return null;
	}

	public synchronized void Insert(EntryFile entryfile)
			throws IllegalArgumentException {
		if (entryfile.gpx == null || entryfile.username == null) {
			throw new IllegalArgumentException(
					"gpx file or username must not be null");
		}
		if (entryfile.id != 0) {
			throw new IllegalArgumentException(
					"You cannot insert an existing entry");
		}
		this.bananaDatabase
				.getWritableDatabase()
				.execSQL(
						"INSERT INTO entryfiles (serverid, sport, username, gpx) VALUES (?,?,?,?);",
						new Object[] { entryfile.serverid, entryfile.sport,
								entryfile.username, entryfile.gpx });

		Cursor c = this.bananaDatabase.getReadableDatabase().rawQuery(
				"SELECT max(_id) FROM entryfiles", null);
		c.moveToFirst();
		entryfile.id = c.getInt(0);
		c.close();
	}

	public boolean LoadTrack(EntryFile entryFile) {
		Cursor cursor = this.bananaDatabase.getReadableDatabase().rawQuery(
				"SELECT gpx from entryfiles WHERE _id = ?",
				new String[] { entryFile.id + "" });
		if (cursor.moveToFirst()) {
			entryFile.gpx = cursor.getString(0);
			return true;
		}
		return false;
	}

	public synchronized void Update(EntryFile entryfile)
			throws IllegalArgumentException {
		if (entryfile.id == 0) {
			throw new IllegalArgumentException(
					"you cannot update a non existing entry");
		}
		if (entryfile.gpx == null || entryfile.serverid == -1
				|| entryfile.sport == null || entryfile.username == null) {
			throw new IllegalArgumentException("entry values must not be null");
		}
		this.bananaDatabase
				.getWritableDatabase()
				.execSQL(
						"UPDATE entryfiles SET serverid=?, sport=?, username=?, gpx=? WHERE _id=?;",
						new Object[] { entryfile.serverid, entryfile.sport,
								entryfile.username, entryfile.gpx, entryfile.id });
	}

}
