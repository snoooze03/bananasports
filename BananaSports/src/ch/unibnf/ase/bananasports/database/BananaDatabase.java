package ch.unibnf.ase.bananasports.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BananaDatabase extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "BananaDatabase";
	public static final int VERSION = 1;

	public static final String CREATE_ENTRYFILE = "CREATE TABLE entryfiles (_id INTEGER PRIMARY KEY AUTOINCREMENT, serverid INT, sport TEXT, username TEXT, gpx TEXT);";

	public static final String CREATE_ANALYSIS = "CREATE TABLE analysisentries (_id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, idfile INTEGER, time INTEGER, distance INTEGER, elevationgain INTEGER, elevationloss INTEGER, duration INTEGER, averagepulse INTEGER);";

	public static final String CREATE_SETTINGS = "CREATE TABLE settings (key VARCHAR NOT NULL PRIMARY KEY, value VARCHAR NOT NULL);";

	public BananaDatabase(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
		Log.v("databaseName", context.getDatabasePath(DATABASE_NAME).toString());
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// called if database does not exist
		db.execSQL(CREATE_ENTRYFILE);
		db.execSQL(CREATE_ANALYSIS);
		db.execSQL(CREATE_SETTINGS);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		wipeDatabase(db);
		onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		wipeDatabase(db);
		onCreate(db);
	}

	private void wipeDatabase(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS entryfiles;");
		db.execSQL("DROP TABLE IF EXISTS analysisentries;");
		db.execSQL("DROP TABLE IF EXISTS settings;");
		Log.v("database", "tables dropped");

	}

}
