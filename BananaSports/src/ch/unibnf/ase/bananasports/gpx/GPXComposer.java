package ch.unibnf.ase.bananasports.gpx;

import java.util.LinkedList;

import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;

public class GPXComposer {

	public static final String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<gpx version=\"1.1\" creator=\"Garmin Connect\"\r\n  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\"\r\n  xmlns=\"http://www.topografix.com/GPX/1/1\"\r\n  xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"\r\n  xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
	public static final String END = "</gpx>";

	public static final String TRACK = "<trk>";
	public static final String ENDTRACK = "</trk>";

	public static final String TRACKSEGMENT = "<trkseg>";
	public static final String ENDTRACKSEGMENT = "</trkseg>";

	public static final String TRACKPOINT = "<trkpt lon=\"%s\" lat=\"%s\">";
	public static final String TRACKPOINTBEGIN = "<trkpt";
	public static final String ENDTRACKPOINT = "</trkpt>";

	public static final String ROUTE = "<rte>";
	public static final String ENDROUTE = "</rte>";

	public static final String ROUTEPOINT = "<rtept lon=\"%s\" lat=\"%s\">";
	public static final String ROUTEPOINTBEGIN = "<rtept";
	public static final String ENDROUTEPOINT = "</rtept>";

	public static final String ELEVATION = "<ele>";
	public static final String ENDELEVATION = "</ele>";

	public static final String TIME = "<time>";
	public static final String ENDTIME = "</time>";

	public static final String LAT = "lat=\"";
	public static final String LON = "lon=\"";
	public static final String ENDHYPHEN = "\"";

	public static final String EXTENSIONS = "<extensions>";
	public static final String ENDEXTENSIONS = "</extensions>";

	public static final String GARMINTRACKPOINTEXTENSION = "<gpxtpx:TrackPointExtension>";
	public static final String ENDGARMINTRACKPOINTEXTENSION = "</gpxtpx:TrackPointExtension>";

	public static final String HEARTRATE = "<gpxtpx:hr>";
	public static final String ENDHEARTRATE = "</gpxtpx:hr>";

	/**
	 * Compose GPX String for a route. This GPX is garmin-conform
	 * 
	 * @param route
	 * @return
	 */
	public static String composeRouteGPX(LinkedList<GPXRoutepointEntry> route) {
		if (route == null || route.isEmpty()) {
			return null;
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append(HEADER);
		buffer.append(ROUTE);

		for (GPXRoutepointEntry entry : route) {
			buffer.append(String.format(ROUTEPOINT, "" + entry.getLongitude(),
					"" + entry.getLatitude()));
			buffer.append(ENDROUTEPOINT);
		}

		buffer.append(ENDROUTE);
		buffer.append(END);
		return buffer.toString();
	}

	/**
	 * Compose GPX String for a track. This GPX is garmin conform
	 * 
	 * @param track
	 * @return
	 */
	public static String composeTrackGPX(LinkedList<GPXTrackpointEntry> track) {
		if (track == null || track.isEmpty()) {
			return null;
		}
		ISO8601TimeHelper th = new ISO8601TimeHelper();
		StringBuffer buffer = new StringBuffer();
		buffer.append(HEADER);
		buffer.append(TRACK);
		buffer.append(TRACKSEGMENT);
		for (GPXTrackpointEntry entry : track) {
			buffer.append(String.format(TRACKPOINT, "" + entry.getLongitude(),
					"" + entry.getLatitude()));
			if (entry.getElevation() != GPXEntry.NOELEVATION) {
				buffer.append(ELEVATION);
				buffer.append(entry.getElevation());
				buffer.append(ENDELEVATION);
			}
			if (entry.getTime() != GPXEntry.NOTIME) {
				buffer.append(TIME);
				buffer.append(th.getStringThreadSafe(entry.getTime()));
				buffer.append(ENDTIME);
			}
			if (entry.getHeartrate() != GPXEntry.NOHEARTRATE) {
				buffer.append(EXTENSIONS);
				buffer.append(GARMINTRACKPOINTEXTENSION);
				buffer.append(HEARTRATE);
				buffer.append(entry.getHeartrate());
				buffer.append(ENDHEARTRATE);
				buffer.append(ENDGARMINTRACKPOINTEXTENSION);
				buffer.append(ENDEXTENSIONS);
			}
			buffer.append(ENDTRACKPOINT);
		}
		buffer.append(ENDTRACKSEGMENT);
		buffer.append(ENDTRACK);
		buffer.append(END);
		return buffer.toString();
	}

}
