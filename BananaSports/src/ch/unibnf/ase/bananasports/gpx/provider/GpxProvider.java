package ch.unibnf.ase.bananasports.gpx.provider;

import ch.unibnf.ase.bananasports.gpx.GPXFile;

public interface GpxProvider {

	public GPXFile load();

	public void store(GPXFile gpxFile);

}
