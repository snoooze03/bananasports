package ch.unibnf.ase.bananasports.gpx;

import java.io.Serializable;
import java.util.Locale;

public class GPXTrackpointEntry implements GPXEntry, Serializable {
	private static final long serialVersionUID = 3546641666174587866L;
	private double longitude;
	private double latitude;
	private double elevation = NOELEVATION;
	private long time = NOTIME;
	private int heartrate = NOHEARTRATE;

	public double getElevation() {
		return this.elevation;
	}

	public int getHeartrate() {
		return this.heartrate;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public long getTime() {
		return this.time;
	}

	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	public void setHeartrate(int heartrate) {
		this.heartrate = heartrate;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setTime(long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(),
				"lon=%f, lat=%f, elevation=%f, time=%d,heartreate=%d",
				this.longitude, this.latitude, this.elevation, this.time,
				this.heartrate);
	}
}
