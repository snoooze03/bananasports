package ch.unibnf.ase.bananasports.gpx;

import java.io.StringReader;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;

public class GPXDOMParser {
	public static final String TRACKPOINT = "trkpt";
	public static final String ROUTEPOINT = "rtept";
	public static final String LONGITUDE = "lon";
	public static final String LATITUDE = "lat";
	public static final String ELEVATION = "ele";
	public static final String TIME = "time";
	public static final String HEARTRATE = "gpxtpx:hr";

	public static GPXFile parseGPX(String filename, String gpx) {
		Document doc = null;
		try {
			InputSource is = new InputSource(new StringReader(gpx));

			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(is);
			GPXFile gpxFile = new GPXFile(filename);
			// parse
			gpxFile.setTrack(GPXDOMParser.parseTrack(doc
					.getElementsByTagName(TRACKPOINT)));

			gpxFile.setRoute(GPXDOMParser.parseRoute(doc
					.getElementsByTagName(ROUTEPOINT)));

			return gpxFile;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private static LinkedList<GPXRoutepointEntry> parseRoute(
			NodeList routepoints) {
		if (routepoints.getLength() <= 0) {
			return null;
		}
		LinkedList<GPXRoutepointEntry> route = new LinkedList<GPXRoutepointEntry>();
		for (int i = 0; i < routepoints.getLength(); i++) {
			try {
				GPXRoutepointEntry routepoint = new GPXRoutepointEntry();
				Element e = (Element) routepoints.item(i);
				routepoint.setLongitude(Double.parseDouble(e
						.getAttribute(LONGITUDE)));
				routepoint.setLatitude(Double.parseDouble(e
						.getAttribute(LATITUDE)));
				route.add(routepoint);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return route;
	}

	private static LinkedList<GPXTrackpointEntry> parseTrack(
			NodeList trackPoints) {
		if (trackPoints.getLength() <= 0) {
			return null;
		}
		ISO8601TimeHelper th = new ISO8601TimeHelper();
		LinkedList<GPXTrackpointEntry> track = new LinkedList<GPXTrackpointEntry>();
		System.out.println(trackPoints.getLength() + "");
		for (int i = 0; i < trackPoints.getLength(); i++) {
			try {
				GPXTrackpointEntry trackpoint = new GPXTrackpointEntry();
				Element e = (Element) trackPoints.item(i);
				trackpoint.setLongitude(Double.parseDouble(e
						.getAttribute(LONGITUDE)));
				trackpoint.setLatitude(Double.parseDouble(e
						.getAttribute(LATITUDE)));
				NodeList cl = e.getElementsByTagName(TIME);
				if (cl.getLength() > 0) {
					trackpoint.setTime(th.getMillisecondsThreadSafe(cl.item(0)
							.getTextContent()));
				}
				cl = e.getElementsByTagName(ELEVATION);
				if (cl.getLength() > 0) {
					trackpoint.setElevation(Double.parseDouble(cl.item(0)
							.getTextContent()));
				}
				cl = e.getElementsByTagName(HEARTRATE);
				if (cl.getLength() > 0) {
					trackpoint.setHeartrate(Integer.parseInt(cl.item(0)
							.getTextContent()));
				}
				track.add(trackpoint);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return track;
	}
}
