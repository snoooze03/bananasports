package ch.unibnf.ase.bananasports.gpx;

public interface GPXEntry {
	public final double NOELEVATION = Double.NEGATIVE_INFINITY;
	public final long NOTIME = Long.MIN_VALUE;
	public final int NOHEARTRATE = Integer.MIN_VALUE;

	public double getLatitude();

	public double getLongitude();

}
