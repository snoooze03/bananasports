package ch.unibnf.ase.bananasports.gpx;

import java.io.Serializable;

public class GPXRoutepointEntry implements GPXEntry, Serializable {
	private static final long serialVersionUID = -2151380885473625806L;
	private double longitude;
	private double latitude;

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
