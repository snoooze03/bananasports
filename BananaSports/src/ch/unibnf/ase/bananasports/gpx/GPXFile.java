package ch.unibnf.ase.bananasports.gpx;

import java.io.Serializable;
import java.util.LinkedList;

public class GPXFile implements Serializable {
	// TODO METADATA & track info
	private static final long serialVersionUID = -5990527179946488757L;
	private LinkedList<GPXTrackpointEntry> track;
	private LinkedList<GPXRoutepointEntry> route;
	private String filename;

	public GPXFile(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return this.filename;
	}

	public LinkedList<GPXRoutepointEntry> getRoute() {
		return this.route;
	}

	public LinkedList<GPXTrackpointEntry> getTrack() {
		return this.track;
	}

	public void setRoute(LinkedList<GPXRoutepointEntry> route) {
		this.route = route;
	}

	public void setTrack(LinkedList<GPXTrackpointEntry> track) {
		this.track = track;
	}

}
