package ch.unibnf.ase.bananasports;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class SocialActivity extends ListActivity {

	private class SocialMenuOnItemClickListener implements OnItemClickListener {

		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			switch (position) {
			case BANANA_SOCIAL_POS:
				SocialActivity.this.startBananaSocialClick(SocialActivity.this
						.getListView());
				break;
			case TWITTER_POS:
				SocialActivity.this.startTwitterSocialClick(SocialActivity.this
						.getListView());
				break;
			case GARMIN_CONNECT_POS:
				SocialActivity.this.startGarminSocialClick(SocialActivity.this
						.getListView());
				break;
			}
		}

	}

	protected static final int BANANA_SOCIAL_POS = 0;
	protected static final int TWITTER_POS = 2;
	protected static final int GARMIN_CONNECT_POS = 1;

	public void startBananaSocialClick(View view) {
		Intent i = new Intent(this, BananaSocialActivity.class);
		this.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.menu_list);

		String[] itemsList = this.getResources().getStringArray(
				R.array.social_menu_labels);
		this.setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, itemsList));
		LinearLayout layout = (LinearLayout) this
				.findViewById(R.id.menu_list_view);
		ListView view = (ListView) layout.getChildAt(0);
		view.setOnItemClickListener(new SocialMenuOnItemClickListener());
	}

	protected void startGarminSocialClick(ListView listView) {
		Intent i = new Intent(this, GarminConnectDownloadActivity.class);
		this.startActivity(i);

	}

	protected void startTwitterSocialClick(ListView listView) {
		// TODO Auto-generated method stub

	}
}
