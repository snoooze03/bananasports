package ch.unibnf.ase.bananasports.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Highly optimized time parser for ISO8601times. You should use the ThreadSafe
 * versions of the methods when calling in different threads. As DateFormat is
 * super inefficient!
 * 
 * @author wiedmer
 * 
 */
public class ISO8601TimeHelper {

	private final ThreadLocal<DateFormat[]> threadLocalFormats = new ThreadLocal<DateFormat[]>() {
		@Override
		protected DateFormat[] initialValue() {
			DateFormat f1 = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");
			f1.setTimeZone(TimeZone.getTimeZone("UTC"));
			DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			f2.setTimeZone(TimeZone.getTimeZone("UTC"));
			return new DateFormat[] { f1, f2 };
		};
	};
	private final ThreadLocal<Integer> threadLocalIndex = new ThreadLocal<Integer>() {
		@Override
		protected Integer initialValue() {
			return 0;
		};
	};
	private final ThreadLocal<Integer> threadLocalErrors = new ThreadLocal<Integer>() {
		@Override
		protected Integer initialValue() {
			return 0;
		};
	};

	private final DateFormat ISO_8601_DATE_TIME = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");

	// needed if utc timezone is marked by 'Z' and not +0000
	private final DateFormat ISO_8601_DATE_TIME_UTC = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	private final DateFormat[] formats = new DateFormat[2];

	// to optimize: on enough errors switch the formatter order
	private int errors = 0;

	private int order = 0;

	public ISO8601TimeHelper() {
		this.ISO_8601_DATE_TIME.setTimeZone(TimeZone.getTimeZone("UTC"));
		this.ISO_8601_DATE_TIME_UTC.setTimeZone(TimeZone.getTimeZone("UTC"));
		this.formats[0] = this.ISO_8601_DATE_TIME;
		this.formats[1] = this.ISO_8601_DATE_TIME_UTC;
	}

	/**
	 * Using threadpools this is very slow.
	 * 
	 * You should use {@link #getMillisecondsThreadSafe(String)} which is faster
	 * and more secure
	 * 
	 * @param datetime
	 * @return
	 */
	public long getMilliseconds(String datetime) {
		Date date;
		try {
			date = this.formats[this.order].parse(datetime);
			return date.getTime();
		} catch (ParseException e) {
			try {
				date = this.formats[1 - this.order].parse(datetime);
				// second succeded.
				this.errors++;
				if (this.errors > 3) {
					// second was better the last n times: switch
					this.order = 1 - this.order;
					this.errors = 0;
				}
				return date.getTime();
			} catch (ParseException ex) {
				e.printStackTrace();
				ex.printStackTrace();
			}
		}
		return -1;
	}

	/**
	 * Using a threadpool, this is massively faster. as Objects will not be
	 * created in each thread
	 * 
	 * @param datetime
	 * @return
	 */
	public long getMillisecondsThreadSafe(String datetime) {
		int index = this.threadLocalIndex.get();
		int errors = this.threadLocalErrors.get();
		try {
			return this.threadLocalFormats.get()[index].parse(datetime)
					.getTime();
		} catch (ParseException e) {
			try {
				if (++errors > 3) {
					this.threadLocalIndex.set(1 - index);
					errors = 0;
				}
				this.threadLocalErrors.set(errors);
				return this.threadLocalFormats.get()[1 - index].parse(datetime)
						.getTime();

			} catch (ParseException ex) {

			}
		}
		return -1;
	}

	public String getString(long milliseconds) {
		return this.ISO_8601_DATE_TIME_UTC.format(milliseconds);
	}

	public synchronized String getStringThreadSafe(long milliseconds) {
		return this.threadLocalFormats.get()[1].format(milliseconds);
	}
}
