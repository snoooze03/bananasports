package ch.unibnf.ase.bananasports.util;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.Settings;
import ch.unibnf.ase.bananasports.database.dao.Settings.Key;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.Entry;
import ch.unibnf.ase.bananasports.gpx.GPXDOMParser;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.gpx.GPXTrackpointEntry;

/**
 * Class to help you saving tracks and analyze them
 * 
 * @author wiedmer
 * 
 */
public class SaveAnalyzeHelper {
	public enum Sport {
		Running("Running"), Cycling("Cycling");
		private String text;

		Sport(String text) {
			this.text = text;
		}

		public String getText() {
			return this.text;
		}
	}

	private static Executor pool = Executors.newCachedThreadPool();

	public static void save(final Context context, final Sport sport,
			final String gpxUncompressed, final boolean analyze) {
		if (!gpxUncompressed.contains("<gpx")) {
			// this is no gpx file and will pollute our db.. hence skip
			return;
		}
		pool.execute(new Runnable() {
			public void run() {
				BananaDatabase db = new BananaDatabase(context);
				Settings settings = new SettingsDAO(db).getSettings();
				EntryFile ef = new EntryFile();
				ef.sport = sport.getText();
				ef.gpx = Helper.base64ZipString(gpxUncompressed);
				ef.username = settings.getValue(Key.USERNAME);
				new EntryFileDAO(db).Insert(ef);
				db.close();

				if (analyze) {
					analyze(context, settings.getValue(Key.USERNAME),
							gpxUncompressed, ef.getID());
				}
			}
		});
	}

	public static void save(final Context context, final String owner,
			final Entry entry, final boolean analyze) {
		pool.execute(new Runnable() {
			public void run() {
				if (!entry.GetGPX().contains("<gpx")) {
					// no gpx. skip it
					return;
				}
				BananaDatabase db = new BananaDatabase(context);
				EntryFile ef = new EntryFile();
				ef.gpx = entry.getCompressedTrack();
				ef.serverid = entry.serverID;
				ef.sport = entry.getSport();
				ef.username = owner;
				new EntryFileDAO(db).Insert(ef);
				if (analyze) {
					analyze(context, owner, entry.GetGPX(), ef.getID());
				}
				db.close();
			}
		});
	}

	private static void analyze(Context context, String owner,
			String gpxUncompressed, int id) {
		GPXFile gpxFile = GPXDOMParser.parseGPX("imported", gpxUncompressed);

		if (gpxFile == null || gpxFile.getTrack() == null
				|| gpxFile.getTrack().size() <= 0) {
			return;
		}

		AnalysisEntry entry = new AnalysisEntry();
		entry.idfile = id;
		entry.username = owner;
		entry.time = gpxFile.getTrack().getFirst().getTime();

		entry.duration = (int) (gpxFile.getTrack().getLast().getTime() - gpxFile
				.getTrack().getFirst().getTime()) / 1000;

		int HRbeats = 0;
		int HRentries = 0;

		double elevationGain = 0;
		double elevationLoss = 0;

		double distance = 0;
		float[] result = new float[1];

		for (int i = 1; i < gpxFile.getTrack().size(); i++) {
			GPXTrackpointEntry tp0 = gpxFile.getTrack().get(i - 1);
			GPXTrackpointEntry tp1 = gpxFile.getTrack().get(i);

			if (tp1.getHeartrate() != GPXTrackpointEntry.NOHEARTRATE) {
				HRentries++;
				HRbeats += tp1.getHeartrate();
			}
			double deltaH = tp1.getElevation() - tp0.getElevation();
			elevationGain += Math.max(0, deltaH);
			elevationLoss += Math.min(0, deltaH);

			Location.distanceBetween(tp0.getLatitude(), tp0.getLongitude(),
					tp1.getLatitude(), tp1.getLongitude(), result);

			distance += result[0];
		}
		if (HRentries > 0) {
			entry.averagepulse = HRbeats / HRentries;
		}
		entry.elevationgain = (int) elevationGain;
		entry.elevationloss = (int) elevationLoss;
		entry.distance = (int) distance;

		BananaDatabase db = new BananaDatabase(context);
		new AnalysisEntryDAO(db).Insert(entry);
		db.close();
		Log.v("analysis complete", String.format("%d %d %d %d %d %d %d %s",
				entry.averagepulse, entry.distance, entry.duration,
				entry.elevationgain, entry.elevationloss, entry.idfile,
				entry.time, entry.username));
	}
}
