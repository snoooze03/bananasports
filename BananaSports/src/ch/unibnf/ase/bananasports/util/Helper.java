package ch.unibnf.ase.bananasports.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import android.util.Base64;

public class Helper {

	/**
	 * 
	 * @param compressMe
	 *            string to be compressed and base64 converted
	 * @return base64 represenation of zipped compressMe string
	 */
	public static String base64ZipString(String compressMe) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			GZIPOutputStream compressStream = new GZIPOutputStream(out);
			ByteArrayInputStream in = new ByteArrayInputStream(
					compressMe.getBytes());
			int readBytes;
			byte[] buffer = new byte[1024];
			while (0 <= (readBytes = in.read(buffer))) {
				compressStream.write(buffer, 0, readBytes);
			}
			compressStream.flush();
			compressStream.finish();
			return new String(Base64.encode(out.toByteArray(), Base64.DEFAULT));

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Set this value for the field 'Authorization' in your URLRequest
	 * 
	 * @param user
	 * @param password
	 * @return
	 */
	public static String createAuthorizationHeaderValue(String user,
			String password) {
		String literal = user + ":" + password;
		byte[] authent = Base64.encode(literal.getBytes(), Base64.DEFAULT);
		return "Basic " + new String(authent);
	}

	/**
	 * 
	 * @param decompressMe
	 *            base64 string to be decompressed
	 * @return decompressed string in cleartext
	 */
	public static String unzipBase64String(String decompressMe) {
		try {
			// ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(Base64.decode(
					decompressMe.getBytes(), Base64.DEFAULT));
			GZIPInputStream decompressStream = new GZIPInputStream(in);
			StringBuffer sb = new StringBuffer();
			int readBytes;
			byte[] buffer = new byte[1024];
			while (0 <= (readBytes = decompressStream.read(buffer))) {
				// out.write(buffer, 0, readBytes);
				sb.append(new String(buffer, 0, readBytes));
			}
			// out.flush();
			// return new String(out.toByteArray());
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
