package ch.unibnf.ase.bananasports.util;

import java.io.File;
import java.io.FilenameFilter;

import android.content.Context;
import android.util.Log;

public class FileUtils {
	public static File[] getGPXFiles(Context context) {
		return context.getFilesDir().listFiles(new FilenameFilter() {

			public boolean accept(File dir, String filename) {
				Log.v("Filename", filename);
				return filename.endsWith(".gpx");
			}
		});

	}
}
