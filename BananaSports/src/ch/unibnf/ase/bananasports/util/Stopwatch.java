package ch.unibnf.ase.bananasports.util;

import android.util.Log;

/**
 * Simple Stopwatch with pause function
 * 
 * @author wiedmer
 * 
 */
public class Stopwatch {

	private boolean pause = false;
	private boolean running = false;
	private long startTime;
	private long stopTime;
	private long pauseTime;
	private long pauseDuration = 0;

	public Stopwatch() {

	}

	public synchronized long getElapsedTime() {
		// Log.v("Stopwatch", "gettime");
		if (this.running) {
			if (this.pause) {
				return this.pauseTime - this.startTime - this.pauseDuration;
			} else {
				return System.currentTimeMillis() - this.startTime
						- this.pauseDuration;
			}
		} else {
			return this.stopTime - this.startTime - this.pauseDuration;
		}
	}

	public synchronized void pause() {
		if (!this.running) {
			return;
		}
		if (this.pause) {
			// unpause
			this.pauseDuration += System.currentTimeMillis() - this.pauseTime;
		} else {
			this.pauseTime = System.currentTimeMillis();
		}
		this.pause = !this.pause;
		Log.v("Stopwatch", "pause");
	}

	public synchronized void start() {
		this.startTime = System.currentTimeMillis();
		this.running = true;
		this.pause = false;
		this.pauseDuration = 0;
		Log.v("Stopwatch", "start");
	}

	public synchronized void stop() {
		if (!this.running) {
			return;
		}
		this.stopTime = System.currentTimeMillis();
		if (this.pause) {
			// unpause if it was paused
			pause();
		}
		this.running = false;
		Log.v("Stopwatch", "stop");
	}
}
