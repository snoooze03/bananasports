package ch.unibnf.ase.bananasports.util;

import java.util.LinkedList;

import android.app.AlertDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class GPSRecorder {

	private class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			// Toast.makeText(GPSRecorder.this.context, "location received",
			// Toast.LENGTH_LONG).show();
			// Log.v("GPSRecorder", "position received");
			if (GPSRecorder.this.running && !GPSRecorder.this.paused) {
				// synchronized (GPSRecorder.this) {
				// gps time jumps around... so use system time here
				location.setTime(System.currentTimeMillis());
				GPSRecorder.this.locations.add(location);
				// Log.v("locationlistener: time:", "" + location.getTime());
				// }
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	public static final float MIN_DISTANCE = 5;

	public static final long MIN_TIME = 0;
	private final LocationManager locationManager;

	private final LinkedList<Location> locations;
	private final Context context;
	private final MyLocationListener myLocationListener = new MyLocationListener();
	private boolean running;

	private boolean paused;

	public GPSRecorder(Context context) {
		this.context = context;
		this.locations = new LinkedList<Location>();
		this.locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		this.locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, MIN_TIME, 0,
				this.myLocationListener);
		// check if available
		if (!this.locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			showAlert(this.context, "No GPS Service available",
					"Is GPS deactivated in your settings?");
		}
	}

	public synchronized void adaptSamplingRate(long minTime) {
		this.locationManager.removeUpdates(this.myLocationListener);
		this.locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, minTime, 0,
				this.myLocationListener);

		Toast.makeText(this.context,
				"changed sampling time to: " + (minTime / 1000) + "seconds",
				Toast.LENGTH_LONG).show();
	}

	/**
	 * In order of time
	 * 
	 * @return
	 */
	public synchronized LinkedList<Location> getLocations() {
		LinkedList<Location> list = new LinkedList<Location>();
		// should be ordered
		// Iterator<Location> it = (this.locations).iterator();
		// while (it.hasNext()) {
		// list.add(it.next());
		// }
		list.addAll(this.locations);
		return list;
	}

	public synchronized void pause() {
		this.paused = !this.paused;
	}

	public synchronized void start() {
		this.locations.clear();
		this.running = true;
		this.paused = false;
	}

	public synchronized void stop() {
		this.running = false;
		this.paused = false;
	}

	private void showAlert(Context context, String title, String message) {
		AlertDialog dialog = new AlertDialog.Builder(context).create();
		dialog.setTitle(title);
		dialog.setTitle(message);
		dialog.show();
	}
}
