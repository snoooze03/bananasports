package ch.unibnf.ase.bananasports.garminconnect;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class GarminConnectHelper {

	public static final String postUrl = "http://connect.garmin.com/proxy/upload-service-1.1/json/upload/.gpx";
	public static final String logonUrl = "https://connect.garmin.com/signin/";
	public static final String[] logonPOST = new String[] {
			"login=login&login%3AloginUsernameField=", "&login%3Apassword=",
			"&login%3AsignInButton=Sign+In&javax.faces.ViewState=j_id1" };
	public static final String activitiesUrl = "http://connect.garmin.com/proxy/activity-service-1.1/gpx/activity/";
	public static final String fullGPXArgument = "?full=true";
	public static final String listUrl = "http://connect.garmin.com/proxy/activity-search-service-1.0/json/activities?start=0&limit=100&ignoreNonGPS=true&ignoreUntitled=true";

	public static final String nonRESTActivityUrl = "http://connect.garmin.com/activity/";

	/**
	 * Download a track
	 * 
	 * @param username
	 * @param password
	 * @param garminTrack
	 * @return gpx string
	 */
	public static String getGPX(String username, String password,
			GarminTrack garminTrack) {
		// cookie manager
		CookieManager manager = new CookieManager();
		manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(manager);
		CookieStore cookieJar = manager.getCookieStore();

		if (!logon(cookieJar, username, password)) {
			return null;
		}
		try {
			HttpURLConnection connection = (HttpURLConnection) (new URL(
					activitiesUrl + garminTrack.getInternalId()
							+ fullGPXArgument).openConnection());
			for (HttpCookie cookie : cookieJar.getCookies()) {
				connection.setRequestProperty("Cookie", cookie.toString());
			}

			byte[] buffer = new byte[2048];
			int readBytes;
			StringBuffer sb = new StringBuffer();
			while (0 <= (readBytes = connection.getInputStream().read(buffer))) {
				sb.append(new String(buffer, 0, readBytes));
			}
			return sb.toString();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static LinkedList<GarminTrack> GetTracks(String username,
			String password) {
		// cookie manager
		CookieManager manager = new CookieManager();
		manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(manager);
		CookieStore cookieJar = manager.getCookieStore();

		if (!logon(cookieJar, username, password)) {
			return null;
		}
		try {
			HttpURLConnection connection = (HttpURLConnection) (new URL(listUrl)
					.openConnection());
			for (HttpCookie cookie : cookieJar.getCookies()) {
				connection.setRequestProperty("Cookie", cookie.toString());
			}

			byte[] buffer = new byte[2048];
			int readBytes;
			StringBuffer sb = new StringBuffer();
			while (0 <= (readBytes = connection.getInputStream().read(buffer))) {
				sb.append(new String(buffer, 0, readBytes));
			}

			LinkedList<GarminTrack> tracks = new LinkedList<GarminTrack>();

			JSONObject jsonObj = new JSONObject(sb.toString());
			JSONArray array = jsonObj.getJSONObject("results").getJSONArray(
					"activities");
			for (int i = 0; i < array.length(); i++) {
				try {
					JSONObject activity = (array.getJSONObject(i))
							.getJSONObject("activity");
					GarminTrack track = new GarminTrack(
							activity.getString("activityId"));
					tracks.add(track);
					if (activity.has("gainElevation"))
						track.setAltitudeGain(activity.getJSONObject(
								"gainElevation").getString("withUnit"));
					if (activity.has("lossElevation"))
						track.setAltitudeLoss(activity.getJSONObject(
								"lossElevation").getString("withUnit"));
					if (activity.has("sumEnergy"))
						track.setCalories(activity.getJSONObject("sumEnergy")
								.getString("withUnit"));
					if (activity.has("activityDescription"))
						track.setDescription(activity.getJSONObject(
								"activityDescription").getString("value"));
					if (activity.has("sumElapsedDuration"))
						track.setDuration(activity.getJSONObject(
								"sumElapsedDuration").getString("display"));
					if (activity.has("activityName"))
						track.setName(activity.getJSONObject("activityName")
								.getString("value"));
					if (activity.has("sumDistance"))
						track.setDistance(activity.getJSONObject("sumDistance")
								.getString("withUnit"));
					if (activity.has("beginTimestamp"))
						track.setTimestamp(activity.getJSONObject(
								"beginTimestamp").getString("display"));
					if (activity.has("activityType"))
						track.setActivityType(activity.getJSONObject(
								"activityType").getString("key"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tracks;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	/**
	 * Get URL to open this activity in browser
	 * 
	 * @param garminTrack
	 * @return
	 */
	public static URL getTrackUrl(GarminTrack garminTrack) {
		try {
			return new URL(nonRESTActivityUrl + garminTrack.getInternalId());
		} catch (MalformedURLException ex) {
			return null;
		}
	}

	/**
	 * Uploads a track to garmin connect
	 * 
	 * @param username
	 *            Garmin Connect username
	 * @param password
	 * @param gpx
	 *            GPX data
	 * @param result
	 *            supply 2-slot array for result result[0] = internalId,
	 *            result[2]=url to open track
	 * @return
	 */
	public static boolean upload(String username, String password, String gpx,
			String[] result) {
		try {
			// cookie manager
			CookieManager manager = new CookieManager();
			manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
			CookieHandler.setDefault(manager);
			CookieStore cookieJar = manager.getCookieStore();

			if (!logon(cookieJar, username, password)) {
				return false;
			}

			// send data
			HttpURLConnection connection = (HttpURLConnection) ((new URL(
					postUrl).openConnection()));

			for (HttpCookie cookie : cookieJar.getCookies()) {
				connection.setRequestProperty("Cookie", cookie.toString());
			}

			String boundary = "----boundary" + System.currentTimeMillis();

			// send request
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			// multipart
			connection.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + boundary);

			// file
			connection.getOutputStream().write(
					("--" + boundary + "\r\n").getBytes());
			connection
					.getOutputStream()
					.write(("Content-Disposition: form-data; name=\"data\"; filename=\"bananasportsupload.gpx\"\r\n")
							.getBytes());
			connection.getOutputStream().write(
					"Content-Type: text/xml\r\n".getBytes());
			connection.getOutputStream().write("\r\n".getBytes());

			connection.getOutputStream().write(gpx.getBytes());
			connection.getOutputStream().write("\r\n".getBytes());
			connection.getOutputStream().flush();
			// response
			connection.getOutputStream().write(
					("--" + boundary + "\r\n").getBytes());
			connection
					.getOutputStream()
					.write(("Content-Disposition: form-data; name=\"responseContentType\"\r\n")
							.getBytes());
			connection.getOutputStream().write("\r\n".getBytes());
			connection.getOutputStream().write("text/html\r\n".getBytes());
			connection.getOutputStream().write(
					("--" + boundary + "--\r\n").getBytes());

			connection.getOutputStream().flush();
			int readBytes;
			byte[] buffer = new byte[2048];
			System.out.println(connection.getResponseMessage() + " "
					+ connection.getResponseCode());
			StringBuffer sb = new StringBuffer();
			while (0 <= (readBytes = connection.getInputStream().read(buffer))) {
				sb.append(new String(buffer, 0, readBytes));
			}
			// System.out.println(sb.toString());

			// if the internal id is generated by garmin: success
			Pattern p = Pattern.compile("\"internalId\": [0-9]+",
					Pattern.DOTALL);
			Matcher m = p.matcher(sb.toString());
			if (m.find()) {
				String internalId = m.group().substring(
						m.group().indexOf(" ") + 1);
				result[0] = internalId;
				result[1] = nonRESTActivityUrl + internalId;
				System.out.println(m.group());
				return true;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	private static boolean logon(CookieStore cookieJar, String username,
			String password) {
		try {
			// get cookies from logon page
			HttpURLConnection logonPageConnection = (HttpURLConnection) ((new URL(
					"https://connect.garmin.com/signin/"))).openConnection();
			logonPageConnection.setRequestMethod("GET");

			System.out.println(logonPageConnection.getResponseCode() + "");
			logonPageConnection.getInputStream().close();

			// logon and get session cookie
			HttpsURLConnection logonConnection = (HttpsURLConnection) ((new URL(
					logonUrl))).openConnection();
			for (HttpCookie cookie : cookieJar.getCookies()) {
				logonConnection.setRequestProperty("Cookie", cookie.toString());
				// System.out.println(cookie.toString());
			}

			logonConnection.setRequestMethod("POST");

			logonConnection.setDoInput(true);
			logonConnection.setDoOutput(true);
			logonConnection.getOutputStream()
					.write((logonPOST[0] + (username) + logonPOST[1]
							+ (password) + logonPOST[2]).getBytes());

			logonConnection.getOutputStream().flush();
			logonConnection.getOutputStream().close();
			System.out.println("" + logonConnection.getResponseCode());
			logonConnection.getInputStream().close();

			HttpURLConnection conn = (HttpURLConnection) ((new URL(
					"http://connect.garmin.com/user/username")))
					.openConnection();

			for (HttpCookie cookie : cookieJar.getCookies()) {
				conn.setRequestProperty("Cookie", cookie.toString());
				// System.out.println(cookie.toString());
			}
			byte[] buffer = new byte[1024];
			StringBuffer sb = new StringBuffer();
			int readBytes;
			while (0 <= (readBytes = conn.getInputStream().read(buffer))) {
				sb.append(new String(buffer, 0, readBytes));
			}
			System.out.println(sb.toString());
			if (sb.toString().contains(username)) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Logon failed");
		return false;
	}
}
