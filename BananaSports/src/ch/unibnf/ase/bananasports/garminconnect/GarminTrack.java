package ch.unibnf.ase.bananasports.garminconnect;

public class GarminTrack {
	private String internalId;
	private String name;
	private String description;
	private String timestamp;
	private String calories;
	private String altitudeGain;
	private String altitudeLoss;
	private String duration;
	private String distance;
	private String activityType;

	public GarminTrack(String internalId) {
		this.internalId = internalId;
	}

	public String getAltitudeGain() {
		return this.altitudeGain;
	}

	public String getAltitudeLoss() {
		return this.altitudeLoss;
	}

	public String getCalories() {
		return this.calories;
	}

	public String getDescription() {
		return this.description;
	}

	public String getDistance() {
		return this.distance;
	}

	public String getDuration() {
		return this.duration;
	}

	public String getInternalId() {
		return this.internalId;
	}

	public String getName() {
		return this.name;
	}

	public String getTimestamp() {
		return this.timestamp;
	}

	public boolean isCycling() {
		return this.activityType.contains("ycling");
	}

	public boolean isRunning() {
		return this.activityType.contains("unning");
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public void setAltitudeGain(String altitudeGain) {
		this.altitudeGain = altitudeGain;
	}

	public void setAltitudeLoss(String altitudeLoss) {
		this.altitudeLoss = altitudeLoss;
	}

	public void setCalories(String calories) {
		this.calories = calories;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTimestamp(String uploadDate) {
		this.timestamp = uploadDate;
	}

}
