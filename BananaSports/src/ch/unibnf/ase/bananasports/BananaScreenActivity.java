package ch.unibnf.ase.bananasports;

import java.util.LinkedList;
import java.util.Locale;

import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.User;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;

public class BananaScreenActivity extends Activity {
	
	private static final int MIN_OFFSET = 10;
	private static final int MAX_OFFSET = 25;
	private static final String BASE_TEXT = "You have achieved %d bananas - ";
	
	private int cyclingDuration;
	private int runningDuration;
	private int cyclingDistance;
	private int runningDistance;
	private int cyclingAltGain;
	private int runningAltGain;
	private int cyclingAltLoss;
	private int runningAltLoss;

	private ImageView imageView;
	private Bitmap bitmap;
	private int bananaValue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.banana_screen);
		this.imageView = (ImageView) this
				.findViewById(R.id.bananas_bananaBar_image);
		this.bitmap = Bitmap.createBitmap(getDisplayWidth(), 75, Bitmap.Config.ARGB_8888);

		// black
		new Canvas(this.bitmap).drawColor(0xFF000000);
		this.imageView.setImageBitmap(this.bitmap);
		
		calculateValues();
		
		//Cycling views
		TextView tv = (TextView) findViewById(R.id.bananas_cycling_alt_gain);
		tv.setText(this.cyclingAltGain+" m");
		
		tv = (TextView) findViewById(R.id.bananas_cycling_alt_loss);
		tv.setText(this.cyclingAltLoss+" m");
		
		tv = (TextView) findViewById(R.id.bananas_cycling_distance);
		tv.setText(this.cyclingDistance+" m");
		
		tv = (TextView) findViewById(R.id.bananas_cycling_duration);
		tv.setText(getDurationText(this.cyclingDuration));
		
		
		// Running views
		tv = (TextView) findViewById(R.id.bananas_running_alt_gain);
		tv.setText(this.runningAltGain+" m");
		
		tv = (TextView) findViewById(R.id.bananas_running_alt_loss);
		tv.setText(this.runningAltLoss+" m");
		
		tv = (TextView) findViewById(R.id.bananas_running_distance);
		tv.setText(this.runningDistance+" m");
		
		tv = (TextView) findViewById(R.id.bananas_running_duration);
		tv.setText(getDurationText(this.runningDuration));
		
		setBananaValue();
		
		startPaintingBananas();
	}
	
	private void setBananaValue() {
		String text = String.format(Locale.getDefault(), BASE_TEXT, bananaValue);	
		text += " ";
		if (bananaValue == 0){
			text += getResources().getStringArray(R.array.banana_cheers)[0];
		} else {
			text += getResources().getStringArray(R.array.banana_cheers)[(bananaValue/10)+1];
		}
		
		TextView tv = (TextView) findViewById(R.id.bananas_banner);
		tv.setText(text);
	}

	private String getDurationText(int duration) {
		int hours = duration / 3600;
		int minutes = (duration % 3600) / 60;
		int seconds = duration % 3600 - (minutes*60);
		
		String secondsString = ""+seconds;
		secondsString = (secondsString.length()<2) ? 0+secondsString : secondsString;
		
		String minutesString = ""+minutes;
		minutesString = (minutesString.length()<2) ?  0+minutesString : minutesString;
		
		String hoursString = hours+"";
		hoursString = (hoursString.length()<2) ? 0+hoursString : hoursString;
		
		return hoursString+":"+minutesString+":"+secondsString;
	}

	private void calculateValues() {
		BananaDatabase db = new BananaDatabase(this);
		LinkedList<AnalysisEntry> entries = new AnalysisEntryDAO(db)
				.GetEntryFiles();
		User user = new SettingsDAO(db).getCurrentUser();
		EntryFileDAO dao = new EntryFileDAO(db);
		LinkedList<EntryFile> entryFiles = dao.GetEntryFiles();
		db.close();

		LinkedList<AnalysisEntry> myEntriesRunning = new LinkedList<AnalysisEntry>();
		for (AnalysisEntry entry : entries) {
			if (user.getUsername().equals(entry.username)
					&& "Running"
							.equals(this.getSport(entry.idfile, entryFiles))) {
				myEntriesRunning.add(entry);
			}
		}
		LinkedList<AnalysisEntry> myEntriesCycling = new LinkedList<AnalysisEntry>();
		for (AnalysisEntry entry : entries) {
			if (user.getUsername().equals(entry.username)
					&& "Cycling"
							.equals(this.getSport(entry.idfile, entryFiles))) {
				myEntriesCycling.add(entry);
			}
		}
		
		long oldest = System.currentTimeMillis() - (31L * 86400 * 1000);
		int monthtotaldistance = 0;
		int monthtotaltime = 0;
		int monthelevationgain = 0;
		int monthelevationloss = 0;

		for (AnalysisEntry entry : myEntriesCycling) {
			if (oldest <= entry.time) {
				monthtotaldistance += entry.distance;
				monthtotaltime += entry.duration;
				monthelevationgain += entry.elevationgain;
				monthelevationloss += entry.elevationloss;
			}
		}
		
		this.cyclingDistance = monthtotaldistance;
		this.cyclingDuration = monthtotaltime;
		this.cyclingAltGain = monthelevationgain;
		this.cyclingAltLoss = monthelevationloss;
		
		int valueCycling = monthtotaldistance / 10000;
		
		for (AnalysisEntry entry : myEntriesRunning) {
			if (oldest <= entry.time) {
				monthtotaldistance += entry.distance;
				monthtotaltime += entry.duration;
				monthelevationgain += entry.elevationgain;
				monthelevationloss += entry.elevationloss;
			}
		}
		
		this.runningDistance = monthtotaldistance;
		this.runningDuration = monthtotaltime;
		this.runningAltGain = monthelevationgain;
		this.runningAltLoss = monthelevationloss;
		
		int valueRunning = monthtotaldistance / 1000;
		
		this.bananaValue = ((valueCycling + valueRunning) / 2);
		
	}

	private void startPaintingBananas() {
		if (bananaValue == 0){
			return;
		}
		new Thread() {
			@Override
			public void run() {
				Canvas canvas = new Canvas(BananaScreenActivity.this.bitmap);
				Resources res = BananaScreenActivity.this
						.getApplicationContext().getResources();
				Drawable myImage = res.getDrawable(R.drawable.banana);
				Paint paint = new Paint();
				paint.setColor(0xFFFF0000);
				int left = imageView.getLeft();
				int top = BananaScreenActivity.this.imageView.getTop();
				int width = 64;
				int offset = getDisplayWidth()/bananaValue;
				offset = (offset > MAX_OFFSET) ? MAX_OFFSET : offset;
				offset = 	(offset < MIN_OFFSET) ? MIN_OFFSET : offset;
				for (int i = 0; i < bananaValue; i++) {
					myImage.setBounds(left += offset, top, left + width, 75);
					myImage.draw(canvas);
					BananaScreenActivity.this.imageView.postInvalidate();
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
					}
				}
			};
		}.start();
	}

	private int getDisplayWidth(){
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		return width;
	}
	
	private String getSport(int id, LinkedList<EntryFile> entryFiles) {
		for (EntryFile ef : entryFiles) {
			if (ef.getID() == id) {
				return ef.sport;
			}
		}
		return "";
	}

}
