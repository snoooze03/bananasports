package ch.unibnf.ase.bananasports;

import java.text.DecimalFormat;
import java.util.LinkedList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.gpx.GPXDOMParser;
import ch.unibnf.ase.bananasports.gpx.GPXEntry;
import ch.unibnf.ase.bananasports.gpx.GPXFile;
import ch.unibnf.ase.bananasports.gpx.GPXRoutepointEntry;
import ch.unibnf.ase.bananasports.gpx.provider.GPXDummyProvider;
import ch.unibnf.ase.bananasports.util.Helper;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;

public class DesignTrackActivity extends MapActivity {

	private class MatchingTrackOverlay extends Overlay {
		private final Paint paint = new Paint();
		int color;
		private final int alpha = 0xFF000000;

		public MatchingTrackOverlay() {
			this.color = 0x00FFFF;
			this.paint.setColor(this.alpha | this.color);
			this.paint.setStrokeWidth(4);
			this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
			this.paint.setDither(true);
		}

		@Override
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow,
				long when) {
			if (DesignTrackActivity.this.matchingTrack == null) {
				return false;
			}
			// returns true to draw right again, false otherwise
			this.color = (this.color + 0x00F0F0) % 0x00FFFFFF;
			this.paint.setColor(this.alpha | this.color);

			Point last = null;
			for (GeoPoint gp : DesignTrackActivity.this.matchingTrack) {
				Point screenPts = new Point();
				DesignTrackActivity.this.mapView.getProjection().toPixels(gp,
						screenPts);
				// canvas.drawCircle(screenPts.x, screenPts.y, 0.5f,
				// this.paint);
				if (last != null) {
					Path p = new Path();
					p.moveTo(last.x, last.y);
					p.lineTo(screenPts.x, screenPts.y);
					canvas.drawPath(p, this.paint);
				}
				last = screenPts;
			}
			// immediate repaint
			return true;
		}
	}

	private class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			GeoPoint gp = new GeoPoint((int) (location.getLatitude() * 1E6),
					(int) (location.getLongitude() * 1E6));
			DesignTrackActivity.this.mapController.animateTo(gp);

		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	private class TapMapOverlay extends Overlay {
		Paint red = new Paint();
		Paint blue = new Paint();
		double distance = 0;

		public TapMapOverlay() {
			this.red.setColor(0xFFFF0000);
			this.red.setStrokeWidth(4);
			this.red.setStyle(Paint.Style.FILL_AND_STROKE);
			this.red.setDither(true);

			this.blue.setColor(0xFF0000FF);
			this.blue.setStrokeWidth(4);
			this.blue.setStyle(Paint.Style.FILL_AND_STROKE);
			this.blue.setDither(true);
		}

		@Override
		public boolean draw(Canvas canvas, MapView mapview, boolean shadow,
				long when) {
			Point last = null;
			int marked = DesignTrackActivity.this.mySeekBar.getProgress();
			int processed = 0;
			for (GeoPoint gp : DesignTrackActivity.this.route) {
				Point screenPts = new Point();
				DesignTrackActivity.this.mapView.getProjection().toPixels(gp,
						screenPts);
				canvas.drawCircle(screenPts.x, screenPts.y, 0.5f,
						(processed <= marked ? this.red : this.blue));
				if (last != null) {
					Path p = new Path();
					p.moveTo(last.x, last.y);
					p.lineTo(screenPts.x, screenPts.y);
					canvas.drawPath(p, (processed <= marked ? this.red
							: this.blue));
				}
				last = screenPts;
				processed++;
			}
			// do not draw just again
			return false;
		}

		@Override
		public boolean onTap(GeoPoint geopoint, MapView mapview) {
			synchronized (DesignTrackActivity.this.route) {
				DesignTrackActivity.this.route.add(geopoint);
				if (DesignTrackActivity.this.route.size() > 1) {
					// more than one point, hence add distance
					GeoPoint secondLast = DesignTrackActivity.this.route
							.get(DesignTrackActivity.this.route.size() - 2);
					float[] result = new float[1];
					Location.distanceBetween(secondLast.getLatitudeE6() / 1E6,
							secondLast.getLongitudeE6() / 1E6,
							geopoint.getLatitudeE6() / 1E6,
							geopoint.getLongitudeE6() / 1E6, result);
					this.distance += result[0];
					DesignTrackActivity.this.updateDistanceView(this.distance);
				}
				DesignTrackActivity.this
						.updateSeekBar(DesignTrackActivity.this.route.size());
				// Log.v("MapTap", geopoint.toString());
			}
			return true;
		}

		public void recalculate() {
			double distance = 0;
			if (DesignTrackActivity.this.route != null) {
				synchronized (DesignTrackActivity.this.route) {
					if (DesignTrackActivity.this.route.size() == 0) {
						return;
					}
					double latitude = DesignTrackActivity.this.route.get(0)
							.getLatitudeE6() / 1E6d;
					double longitude = DesignTrackActivity.this.route.get(0)
							.getLongitudeE6() / 1E6d;
					float[] result = new float[1];
					for (int i = 1; i < DesignTrackActivity.this.route.size(); i++) {
						double currentLat = DesignTrackActivity.this.route.get(
								i).getLatitudeE6() / 1E6d;
						double currentLon = DesignTrackActivity.this.route.get(
								i).getLongitudeE6() / 1E6d;
						Location.distanceBetween(latitude, longitude,
								currentLat, currentLon, result);
						latitude = currentLat;
						longitude = currentLon;
						distance += result[0];
					}
					DesignTrackActivity.this
							.updateSeekBar(DesignTrackActivity.this.route
									.size());
				}
			}
			this.distance = distance;
			DesignTrackActivity.this.updateDistanceView(distance);
		}

		public void removeMarkedPoints() {
			// remove points until mark
			int marked = DesignTrackActivity.this.mySeekBar.getProgress();
			// if only one point: delete it
			if (DesignTrackActivity.this.route.size() == 1) {
				this.removeLastPoint();
			}
			for (int pos = DesignTrackActivity.this.route.size() - 1; marked < pos; pos--) {
				this.removeLastPoint();
			}
		}

		private void removeLastPoint() {
			if (DesignTrackActivity.this.route.size() > 0) {
				GeoPoint geopoint = DesignTrackActivity.this.route.removeLast();
				if (DesignTrackActivity.this.route.size() > 0) {
					float[] result = new float[1];
					Location.distanceBetween(DesignTrackActivity.this.route
							.getLast().getLatitudeE6() / 1E6,
							DesignTrackActivity.this.route.getLast()
									.getLongitudeE6() / 1E6, geopoint
									.getLatitudeE6() / 1E6, geopoint
									.getLongitudeE6() / 1E6, result);
					this.distance -= result[0];
				}
			}
			if (DesignTrackActivity.this.route.size() == 0) {
				this.distance = 0;
			}
			DesignTrackActivity.this.updateDistanceView(this.distance);
			DesignTrackActivity.this
					.updateSeekBar(DesignTrackActivity.this.route.size());
		}
	}

	private MapView mapView;
	private MapController mapController;
	private LocationManager locationManager;
	private TapMapOverlay tapMapOverlay;
	private MatchingTrackOverlay matchingTrackOverlay;

	/** route of the designed track */
	LinkedList<GeoPoint> route = new LinkedList<GeoPoint>();
	private SeekBar mySeekBar;

	private TextView maxDistanceProximity;

	private TextView distanceView;

	private CheckBox mySatelliteCheckBox;

	private LinkedList<GeoPoint> matchingTrack;

	private int knownGPXIndex = 0;

	/** All tracks that are known to the app, to be used for searching a track */
	private LinkedList<GPXFile> allTracks = new LinkedList<GPXFile>();

	/** distance to find a related track */
	public static final int MAXDISTANCE = 20;

	public static final int MAX_POINTS_ON_TRACK = 75;

	public void clearMatchingTrackClick(View view) {
		this.matchingTrack = null;
		this.knownGPXIndex = 0;
		this.mapView.invalidate();
	}

	public void findNextMatchingTrackClick(View view) {
		if (this.route == null || this.route.size() == 0) {
			return;
		}
		final int maxDistance = this.getMaxDistance();

		new AsyncTask<GeoPoint, Void, LinkedList<GeoPoint>>() {
			private String lastFoundFile;

			@Override
			protected LinkedList<GeoPoint> doInBackground(GeoPoint... params) {
				Log.v("DesignTrack AsyncTask", "index: "
						+ DesignTrackActivity.this.knownGPXIndex);
				double latitude = params[0].getLatitudeE6() / 1E6d;
				double longitude = params[0].getLongitudeE6() / 1E6d;
				for (; DesignTrackActivity.this.knownGPXIndex < DesignTrackActivity.this.allTracks
						.size(); DesignTrackActivity.this.knownGPXIndex++) {
					LinkedList<? extends GPXEntry> points = DesignTrackActivity.this.allTracks
							.get(DesignTrackActivity.this.knownGPXIndex)
							.getTrack();
					if (points == null) {
						points = DesignTrackActivity.this.allTracks.get(
								DesignTrackActivity.this.knownGPXIndex)
								.getRoute();
					}

					if (points != null) {
						Log.v("DesignTrack AsyncTask", "searching track");
						for (int i = 0; i < points.size(); i++) {
							float[] result = new float[1];
							Location.distanceBetween(latitude, longitude,
									points.get(i).getLatitude(), points.get(i)
											.getLongitude(), result);
							if (result[0] < maxDistance) {

								this.lastFoundFile = DesignTrackActivity.this.allTracks
										.get(DesignTrackActivity.this.knownGPXIndex)
										.getFilename();
								// found, copy rest to list. but skip some
								int delta = (points.size() - i)
										/ MAX_POINTS_ON_TRACK;
								delta = Math.max(1, delta);

								LinkedList<GeoPoint> matching = new LinkedList<GeoPoint>();
								for (int j = i; j < points.size() - 1; j += delta) {
									matching.add(new GeoPoint(
											(int) (points.get(j).getLatitude() * 1E6),
											(int) (points.get(j).getLongitude() * 1E6)));
								}
								// last point must be added!
								matching.add(new GeoPoint(
										(int) (points.get(points.size() - 1)
												.getLatitude() * 1E6),
										(int) (points.get(points.size() - 1)
												.getLongitude() * 1E6)));

								DesignTrackActivity.this.knownGPXIndex++;
								return matching;
							}
						}
					}
				}
				return null;
			}

			@Override
			protected void onPostExecute(LinkedList<GeoPoint> result) {
				// reset to 0 if needed
				if (DesignTrackActivity.this.allTracks.size() > 0) {
					DesignTrackActivity.this.knownGPXIndex %= DesignTrackActivity.this.allTracks
							.size();
				}
				// Log.v("DesignTrackAsynctask", "list is null: "
				// + (result == null));
				DesignTrackActivity.this.matchingTrack = result;
				// force repainting
				DesignTrackActivity.this.mapView.invalidate();
				Toast.makeText(DesignTrackActivity.this,
						"Found file: " + this.lastFoundFile, Toast.LENGTH_LONG)
						.show();
			}
		}.execute(this.route.getLast());
	}

	public int getMaxDistance() {
		int maxDistance = MAXDISTANCE;
		try {
			maxDistance = Integer.parseInt(this.maxDistanceProximity.getText()
					.toString());
			if (maxDistance < MAXDISTANCE) {
				maxDistance = MAXDISTANCE;
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}
		return maxDistance;
	}

	@Override
	public void onBackPressed() {
		if (this.route == null || this.route.size() == 0) {
			this.finish();
			return;
		}
		Log.v("DesignTrackActivity", "back pressed");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Close");
		builder.setMessage("Start Workout from this route?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				DesignTrackActivity.this
						.startWorkout(DesignTrackActivity.this.route);
				DesignTrackActivity.this.finish();
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				DesignTrackActivity.this.finish();
			}
		});
		builder.setNeutralButton("Cancel", null);
		builder.create().show();
		// super.onBackPressed();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.track_mapactivity);
		this.setContentView(R.layout.design_activity);

		this.mapView = (MapView) this.findViewById(R.id.track_mapview);
		this.mapView.setBuiltInZoomControls(true);
		this.mapView.setSatellite(true);
		this.mapController = this.mapView.getController();

		// TODO start a background task which loads all known data
		new Thread() {
			@Override
			public void run() {
				DesignTrackActivity.this.allTracks.clear();
				DesignTrackActivity.this.allTracks.add(new GPXDummyProvider()
						.load());

				BananaDatabase db = new BananaDatabase(DesignTrackActivity.this);

				EntryFileDAO dao = new EntryFileDAO(db);
				for (EntryFile ef : dao.GetEntryFiles()) {
					DesignTrackActivity.this.allTracks.add(GPXDOMParser
							.parseGPX(ef.username + " " + ef.sport + " "
									+ ef.serverid,
									Helper.unzipBase64String(ef.gpx)));
				}
				Log.v("Trackloader", "done");
			};
		}.start();

		this.distanceView = (TextView) this
				.findViewById(R.id.track_distanceTextView);

		this.mySeekBar = (SeekBar) this.findViewById(R.id.track_seekBar);
		this.mySeekBar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						if (fromUser) {
							DesignTrackActivity.this.mapView.invalidate();
						}
					}

					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					public void onStopTrackingTouch(SeekBar seekBar) {
					}
				});

		this.mySatelliteCheckBox = (CheckBox) this
				.findViewById(R.id.track_satellitecheckbox);
		this.mySatelliteCheckBox
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						DesignTrackActivity.this.mapView
								.setSatellite(DesignTrackActivity.this.mySatelliteCheckBox
										.isChecked());
					}
				});

		this.maxDistanceProximity = (TextView) this
				.findViewById(R.id.track_proximity);
		this.maxDistanceProximity.setText("" + MAXDISTANCE);

		this.locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		this.checkLocationProvider();

		this.locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, 0, 0, new MyLocationListener());
		Location loc = this.locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (loc == null) {
			// maybe no gps, try a location from mobile network
			loc = this.locationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		if (loc == null) {
			Toast.makeText(this.mapView.getContext(),
					"Could not fix location...", Toast.LENGTH_LONG).show();
		} else {
			GeoPoint gp = new GeoPoint((int) (loc.getLatitude() * 1E6),
					(int) (loc.getLongitude() * 1E6));

			// go to current location
			this.mapController.animateTo(gp);
			this.mapController.setZoom(17);
			this.mapView.invalidate();
		}
		this.mapView.getOverlays().clear();
		this.tapMapOverlay = new TapMapOverlay();
		this.mapView.getOverlays().add(this.tapMapOverlay);
		this.matchingTrackOverlay = new MatchingTrackOverlay();
		this.mapView.getOverlays().add(this.matchingTrackOverlay);

		MyLocationOverlay locationOverlay = new MyLocationOverlay(this,
				this.mapView);
		locationOverlay.enableMyLocation();
		locationOverlay.enableCompass();
		this.mapView.getOverlays().add(locationOverlay);
	}

	public void removeLastWaypointButtonClick(View view) {
		this.tapMapOverlay.removeMarkedPoints();
		this.mapView.invalidate();
	}

	public void seekbarChanged(View view) {
		this.mapView.invalidate();
	}

	public void useMatchingTrackClick(View view) {
		if (this.matchingTrack != null) {
			synchronized (this.route) {
				this.route.addAll(this.matchingTrack);
			}
			this.matchingTrack = null;
			this.tapMapOverlay.recalculate();
		}
		this.mapView.invalidate();

	}

	private void checkLocationProvider() {
		boolean network = this.locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		boolean gps = this.locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!gps) {
			if (network) {
				// no gps but network
				this.showAlertMessage(
						"No GPS,  but network-location found. This is VERY INACCURATE",
						"GPS disabled");
			} else {
				// no gps and no network
				this.showAlertMessage(
						"No GPS and no network-location found. No location possible. The app will not work. Enable GPS & network location in your settings",
						"GPS disabled");
			}
		}
	}

	private void showAlertMessage(String message, String title) {
		AlertDialog dialog = new AlertDialog.Builder(this.mapView.getContext())
				.create();
		dialog.setTitle(title);
		dialog.setTitle(message);
		dialog.show();
	}

	private void startWorkout(LinkedList<GeoPoint> route) {
		LinkedList<GPXRoutepointEntry> list = new LinkedList<GPXRoutepointEntry>();
		for (GeoPoint gp : route) {
			GPXRoutepointEntry rp = new GPXRoutepointEntry();
			rp.setLatitude(gp.getLatitudeE6() / 1E6d);
			rp.setLongitude(gp.getLongitudeE6() / 1E6d);
			list.add(rp);
		}
		GPXFile file = new GPXFile("designer");
		file.setRoute(list);
		Intent i = new Intent(this.getApplicationContext(),
				WorkoutActivity.class);
		i.putExtra(WorkoutActivity.GPXFILE, file);
		this.startActivity(i);
	}

	private void updateDistanceView(double distance) {
		DecimalFormat f = new DecimalFormat("#0.00");
		this.distanceView.setText(f.format(distance) + "m");
	}

	private void updateSeekBar(int max) {
		this.mySeekBar.setMax((max > 0 ? max - 1 : 0));
		this.mySeekBar.setProgress((max > 0 ? max - 2 : 0));
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
