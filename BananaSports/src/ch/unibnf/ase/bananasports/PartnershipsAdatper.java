package ch.unibnf.ase.bananasports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.expandableList.TrackListChild;

public class PartnershipsAdatper extends BaseExpandableListAdapter implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2823584100964833014L;
	private Context context;
	private List<PartnershipParent> groups;

	public PartnershipsAdatper(Context context, List<PartnershipParent> groups) {
		this.context = context;
		this.groups = groups;
	}

	public void addPartnership(PartnershipParent p) {
		List<PartnershipParent> list = new ArrayList<PartnershipParent>();
		list.addAll(groups);
		groups.clear();
		list.add(p);
		groups = list;
		this.notifyDataSetChanged();
	}
	
	public List<PartnershipParent> getGroups(){
		return groups;
	}

	public Object getChild(int groupPosition, int childPosition) {
		return this.groups.get(groupPosition).getChildren().get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public int getChildrenCount(int groupPosition) {
		return this.groups.get(groupPosition).getChildren().size();
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {

		TrackListChild child = this.groups.get(groupPosition).getChildren()
				.get(childPosition);
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.child_row, null);
		}

		TextView sport = (TextView) view.findViewById(R.id.child_row_sport);
		sport.setTag("sport_" + groupPosition + "." + child.getId());
		sport.setText(this.groups.get(groupPosition).getChildren()
				.get(childPosition).getSport());

		TextView label = (TextView) view.findViewById(R.id.child_row_time);
		label.setTag("label_time_" + groupPosition + "." + child.getId());
		label.setText(R.string.time_label);
		TextView value = (TextView) view
				.findViewById(R.id.child_row_time_value);
		value.setTag("value_time_" + groupPosition + "." + child.getId());
		value.setText(child.getTime());

		label = (TextView) view.findViewById(R.id.child_row_duration);
		label.setTag("label_duration_" + groupPosition + "." + child.getId());
		label.setText(R.string.duration_label);
		value = (TextView) view.findViewById(R.id.child_row_duration_value);
		value.setTag("value_duration_" + groupPosition + "." + child.getId());
		value.setText(child.getDuration());

		label = (TextView) view.findViewById(R.id.child_row_distance);
		label.setTag("label_distance_" + groupPosition + "." + child.getId());
		label.setText(R.string.distance_label);
		value = (TextView) view.findViewById(R.id.child_row_distance_value);
		value.setTag("value_distance_" + groupPosition + "." + child.getId());
		value.setText(child.getDistance());

		label = (TextView) view.findViewById(R.id.child_row_elevation_gain);
		label.setTag("label_alt_gain_" + groupPosition + "." + child.getId());
		label.setText(R.string.alt_gain_label);
		value = (TextView) view
				.findViewById(R.id.child_row_elevation_gain_value);
		value.setTag("value_alt_gain_" + groupPosition + "." + child.getId());
		value.setText(child.getEleveationGain());

		label = (TextView) view.findViewById(R.id.child_row_elevation_loss);
		label.setTag("label_alt_loss_" + groupPosition + "." + child.getId());
		label.setText(R.string.alt_loss_label);
		value = (TextView) view
				.findViewById(R.id.child_row_elevation_loss_value);
		value.setTag("value_alt_loss_" + groupPosition + "." + child.getId());
		value.setText(child.getElevationLoss());

		label = (TextView) view.findViewById(R.id.child_row_avg_pulse);
		label.setTag("label_avg_pulse_" + groupPosition + "." + child.getId());
		label.setText(R.string.avg_pulse_label);
		value = (TextView) view.findViewById(R.id.child_row_avg_pulse_value);
		value.setTag("value_avg_pulse_" + groupPosition + "." + child.getId());
		value.setText(child.getAveragePulse());

		Button shareButton = (Button) view
				.findViewById(R.id.child_row_share_button);
		if (shareButton != null) {
			((RelativeLayout) shareButton.getParent()).removeView(shareButton);
		}

		return view;

	}

	public Object getGroup(int groupPosition) {
		return this.groups.get(groupPosition);
	}

	public int getGroupCount() {
		return this.groups.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View view,
			ViewGroup parent) {
		PartnershipParent group = this.groups.get(groupPosition);
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.button_list_item, null);
		}
		TextView tv = (TextView) view.findViewById(R.id.button_list_item_text);
		tv.setText(group.getPartnerName());

		tv = (TextView) view.findViewById(R.id.button_list_item_status);
		tv.setText(group.getStatus().text());

		this.populateButton(view, groupPosition);

		return view;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	public void populateButton(View view, int groupPosition) {
		this.populateOrRemoveButton(
				(Button) view.findViewById(R.id.button_list_item_button),
				this.groups.get(groupPosition).getPartnership(), view,
				groupPosition);
	}

	public void removePartnership(int groupId) {
		this.groups.remove(groupId);
	}

	private void populateOrRemoveButton(Button button, Partnership p,
			View view, int groupId) {
		if (button == null) {
			return;
		}
		boolean confirmLocally = (!p.isOwnerLocal() && !p.hasPartnerConfirmed())
				|| (p.isOwnerLocal() && !p.hasOwnerConfirmed());

		button.setText(confirmLocally ? R.string.confirm_partnership_label
				: R.string.delete_label);
		button.setOnClickListener(confirmLocally ? new ConfirmParthershipClickListener(
				p, this, groupId, view) : new DeletePartnershipClickListener(p,
				this, groupId, view));
	}
	
	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		super.registerDataSetObserver(observer);
	}
}
