package ch.unibnf.ase.bananasports;

import java.util.Comparator;

import ch.unibnf.ase.bananasports.entities.Entry;
import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;

public class EntryTimeComparator implements Comparator<Entry> {

	private ISO8601TimeHelper helper;

	public EntryTimeComparator() {
		super();
		this.helper = new ISO8601TimeHelper();
	}

	public int compare(Entry lhs, Entry rhs) {
		Long lDate = this.helper.getMilliseconds(lhs.entrydate);
		Long rDate = this.helper.getMilliseconds(rhs.entrydate);
		return lDate.compareTo(rDate);
	}

}
