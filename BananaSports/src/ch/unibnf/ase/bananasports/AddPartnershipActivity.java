package ch.unibnf.ase.bananasports;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.User;

public class AddPartnershipActivity extends Activity implements
		OnItemClickListener {

	private LinkedList<User> users;

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long arg3) {

		final String partnerName = (String) ((TextView) view).getText();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setCancelable(false);
		// Get the layout inflater
		builder.setTitle("Partnership Request").setMessage(
				"Request Partnership with user " + partnerName + "?");
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						AddPartnershipActivity.this.finish();
						Intent i = new Intent(getApplicationContext(), BananaSocialActivity.class);
						startActivity(i);
					}
				}).setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						BananaDatabase db = new BananaDatabase(
								AddPartnershipActivity.this);
						User user = new SettingsDAO(db).getCurrentUser();
						db.close();
						AddPartnershipActivity.this.addPartnerShip(partnerName,
								user);
						AddPartnershipActivity.this.finish();
						dialog.dismiss();
						Partnership p = RESTConnector.getPartnership(
								user.getUsername(), partnerName, 3000);
						PartnershipsAdatper adapter = (PartnershipsAdatper) AddPartnershipActivity.this
								.getIntent().getSerializableExtra(
										BananaSocialActivity.ADAPTER);
						if (p != null && adapter != null) {
							adapter.addPartnership(BananaSocialActivity
									.createPartnershipParent(p,
											AddPartnershipActivity.this
													.getApplicationContext()));
						}
					}
				});

		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private ArrayAdapter<String> createAdapter() {
		this.users = RESTConnector.getUsers(3000);
		String[] usernames = new String[this.users.size()];
		for (int i = 0; i < this.users.size(); i++) {
			usernames[i] = this.users.get(i).getUsername();
		}

		return new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, usernames);
	}

	protected void addPartnerShip(String partner, User user) {

		RESTConnector.createPartnership(user, partner, 3000);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.add_partnership_activity);

		AutoCompleteTextView searchBar = (AutoCompleteTextView) this
				.findViewById(R.id.search_user_autocomplete);
		searchBar.setAdapter(this.createAdapter());
		searchBar.setOnItemClickListener(this);
		searchBar.setHint("Search User");

	}

}
