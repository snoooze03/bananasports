package ch.unibnf.ase.bananasports;

import java.util.LinkedList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import ch.unibnf.ase.bananasports.database.BananaDatabase;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntry;
import ch.unibnf.ase.bananasports.database.dao.AnalysisEntryDAO;
import ch.unibnf.ase.bananasports.database.dao.EntryFile;
import ch.unibnf.ase.bananasports.database.dao.EntryFileDAO;
import ch.unibnf.ase.bananasports.database.dao.SettingsDAO;
import ch.unibnf.ase.bananasports.entities.User;

public class MyProgressActivity extends Activity {
	private String getSport(int id, LinkedList<EntryFile> entryFiles) {
		for (EntryFile ef : entryFiles) {
			if (ef.getID() == id) {
				return ef.sport;
			}
		}
		return "";
	}

	private void setValues() {
		BananaDatabase db = new BananaDatabase(this);
		LinkedList<AnalysisEntry> entries = new AnalysisEntryDAO(db)
				.GetEntryFiles();
		User user = new SettingsDAO(db).getCurrentUser();
		EntryFileDAO dao = new EntryFileDAO(db);
		LinkedList<EntryFile> entryFiles = dao.GetEntryFiles();
		db.close();

		LinkedList<AnalysisEntry> myEntriesRunning = new LinkedList<AnalysisEntry>();
		for (AnalysisEntry entry : entries) {
			if (user.getUsername().equals(entry.username)
					&& "Running"
							.equals(this.getSport(entry.idfile, entryFiles))) {
				myEntriesRunning.add(entry);
			}
		}

		this.setValuesRunning(myEntriesRunning);

		LinkedList<AnalysisEntry> myEntriesCycling = new LinkedList<AnalysisEntry>();
		for (AnalysisEntry entry : entries) {
			if (user.getUsername().equals(entry.username)
					&& "Cycling"
							.equals(this.getSport(entry.idfile, entryFiles))) {
				myEntriesCycling.add(entry);
			}
		}

		this.setValuesCycling(myEntriesCycling);

	}

	private void setValuesCycling(LinkedList<AnalysisEntry> myEntries) {
		// total

		int totaldistance = 0;
		int totaltime = 0;
		int elevationgain = 0;
		int elevationloss = 0;
		LinkedList<Long> times = new LinkedList<Long>();
		LinkedList<Integer> heartrates = new LinkedList<Integer>();
		LinkedList<Float> speeds = new LinkedList<Float>();

		long minTime = Long.MAX_VALUE;
		long maxTime = Long.MIN_VALUE;

		for (AnalysisEntry entry : myEntries) {
			totaldistance += entry.distance;
			totaltime += entry.duration;
			elevationgain += entry.elevationgain;
			elevationloss += entry.elevationloss;

			times.add(entry.time);
			heartrates.add(entry.averagepulse);
			speeds.add((float) entry.distance / (float) entry.duration);

			minTime = Math.min(minTime, entry.time);
			maxTime = Math.max(maxTime, entry.time);
		}

		minTime -= 1000;
		maxTime += 1000;
		// paint
		Bitmap pulseBitmap = Bitmap.createBitmap(1000, 250,
				Bitmap.Config.ARGB_8888);
		Canvas pulsecanvas = new Canvas(pulseBitmap);
		pulsecanvas.drawColor(0xFFFFFFFF);
		pulsecanvas.save();
		pulsecanvas.scale(1000f / (maxTime - minTime), 1);
		Paint paint = new Paint();
		paint.setColor(0xFFFF0000);
		for (int i = 0; i < times.size(); i++) {
			pulsecanvas.drawCircle(times.get(i) - minTime,
					250 - heartrates.get(i), 5, paint);
			// pulsecanvas.drawRect(times.get(i) - minTime - 500,
			// 250 - heartrates.get(i), times.get(i) - minTime + 500,
			// 250 - heartrates.get(i) + 5, paint);
		}
		pulsecanvas.restore();
		((ImageView) this
				.findViewById(R.id.myprogress_cycling_heartratedevelopment))
				.setImageBitmap(pulseBitmap);
		((ImageView) this
				.findViewById(R.id.myprogress_cycling_heartratedevelopment))
				.postInvalidate();

		Bitmap speedBitmap = Bitmap.createBitmap(1000, 250,
				Bitmap.Config.ARGB_8888);
		Canvas speedcanvas = new Canvas(speedBitmap);
		speedcanvas.drawColor(0xFFFFFFFF);
		speedcanvas.save();
		speedcanvas.scale(1000f / (maxTime - minTime), 1);
		Paint paint2 = new Paint();
		paint.setColor(0xFF0000FF);
		for (int i = 0; i < times.size(); i++) {
			// m/s * 3.6 = km/h -> scale to 0-250 -> m/s*36
			speedcanvas.drawCircle(times.get(i) - minTime,
					250 - speeds.get(i) * 36, 5, paint2);
		}

		speedcanvas.restore();

		((ImageView) this
				.findViewById(R.id.myprogress_cycling_speeddevelopment))
				.setImageBitmap(speedBitmap);

		((TextView) this.findViewById(R.id.myprogress_cycling_distance))
				.setText(totaldistance + " meters");
		((TextView) this.findViewById(R.id.myprogress_cycling_totaltime))
				.setText(totaltime / 3600f + " hours");
		((TextView) this.findViewById(R.id.myprogress_cycling_elevationgain))
				.setText(elevationgain + " meters");
		((TextView) this.findViewById(R.id.myprogress_cycling_elevationloss))
				.setText(elevationloss + " meters");

		// monthly ( last 31 days)
		long oldest = System.currentTimeMillis() - (31 * 86400 * 1000);
		int monthtotaldistance = 0;
		int monthtotaltime = 0;
		int monthelevationgain = 0;
		int monthelevationloss = 0;

		for (AnalysisEntry entry : myEntries) {
			if (oldest <= entry.time) {
				monthtotaldistance += entry.distance;
				monthtotaltime += entry.duration;
				monthelevationgain += entry.elevationgain;
				monthelevationloss += entry.elevationloss;
			}
		}

		((TextView) this.findViewById(R.id.myprogress_cycling_distance_month))
				.setText(monthtotaldistance + " meters");
		((TextView) this.findViewById(R.id.myprogress_cycling_totaltime_month))
				.setText(monthtotaltime / 3600f + " hours");
		((TextView) this
				.findViewById(R.id.myprogress_cycling_elevationgain_month))
				.setText(monthelevationgain + " meters");
		((TextView) this
				.findViewById(R.id.myprogress_cycling_elevationloss_month))
				.setText(monthelevationloss + " meters");
	}

	private void setValuesRunning(LinkedList<AnalysisEntry> myEntries) {
		// total

		int totaldistance = 0;
		int totaltime = 0;
		int elevationgain = 0;
		int elevationloss = 0;
		LinkedList<Long> times = new LinkedList<Long>();
		LinkedList<Integer> heartrates = new LinkedList<Integer>();
		LinkedList<Float> speeds = new LinkedList<Float>();

		long minTime = Long.MAX_VALUE;
		long maxTime = Long.MIN_VALUE;

		for (AnalysisEntry entry : myEntries) {
			totaldistance += entry.distance;
			totaltime += entry.duration;
			elevationgain += entry.elevationgain;
			elevationloss += entry.elevationloss;

			times.add(entry.time);
			heartrates.add(entry.averagepulse);
			speeds.add((float) entry.distance / (float) entry.duration);

			minTime = Math.min(minTime, entry.time);
			maxTime = Math.max(maxTime, entry.time);
		}

		minTime -= 1000;
		maxTime += 1000;
		// paint
		Bitmap pulseBitmap = Bitmap.createBitmap(1000, 250,
				Bitmap.Config.ARGB_8888);
		Canvas pulsecanvas = new Canvas(pulseBitmap);
		pulsecanvas.drawColor(0xFFFFFFFF);
		pulsecanvas.save();
		pulsecanvas.scale(1000f / (maxTime - minTime), 1);
		Paint paint = new Paint();
		paint.setStrokeWidth(0);
		paint.setColor(0xFFFF0000);
		for (int i = 0; i < times.size(); i++) {
			pulsecanvas.drawCircle(times.get(i) - minTime,
					250 - heartrates.get(i), 5, paint);
		}

		pulsecanvas.restore();
		((ImageView) this
				.findViewById(R.id.myprogress_running_heartratedevelopment))
				.setImageBitmap(pulseBitmap);

		Bitmap speedBitmap = Bitmap.createBitmap(1000, 250,
				Bitmap.Config.ARGB_8888);
		Canvas speedcanvas = new Canvas(speedBitmap);
		speedcanvas.drawColor(0xFFFFFFFF);
		speedcanvas.save();
		speedcanvas.scale(1000f / (maxTime - minTime), 1);
		Paint paint2 = new Paint();
		paint.setColor(0xFF0000FF);
		for (int i = 0; i < times.size(); i++) {
			// m/s * 3.6 = km/h -> scale to 0-250 -> m/s*36
			speedcanvas.drawCircle(times.get(i) - minTime,
					250 - speeds.get(i) * 36, 5, paint2);
		}

		speedcanvas.restore();

		((ImageView) this
				.findViewById(R.id.myprogress_running_speeddevelopment))
				.setImageBitmap(speedBitmap);

		((TextView) this.findViewById(R.id.myprogress_running_distance))
				.setText(totaldistance + " meters");
		((TextView) this.findViewById(R.id.myprogress_running_totaltime))
				.setText(totaltime / 3600f + " hours");
		((TextView) this.findViewById(R.id.myprogress_running_elevationgain))
				.setText(elevationgain + " meters");
		((TextView) this.findViewById(R.id.myprogress_running_elevationloss))
				.setText(elevationloss + " meters");

		// monthly ( last 31 days)
		long oldest = System.currentTimeMillis() - (31 * 86400 * 1000);
		int monthtotaldistance = 0;
		int monthtotaltime = 0;
		int monthelevationgain = 0;
		int monthelevationloss = 0;

		for (AnalysisEntry entry : myEntries) {
			if (oldest <= entry.time) {
				monthtotaldistance += entry.distance;
				monthtotaltime += entry.duration;
				monthelevationgain += entry.elevationgain;
				monthelevationloss += entry.elevationloss;
			}
		}

		((TextView) this.findViewById(R.id.myprogress_running_distance_month))
				.setText(monthtotaldistance + " meters");
		((TextView) this.findViewById(R.id.myprogress_running_totaltime_month))
				.setText(monthtotaltime / 3600f + " hours");
		((TextView) this
				.findViewById(R.id.myprogress_running_elevationgain_month))
				.setText(monthelevationgain + " meters");
		((TextView) this
				.findViewById(R.id.myprogress_running_elevationloss_month))
				.setText(monthelevationloss + " meters");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.myprogress_activity);

		this.setValues();
	}
}
