package ch.unibnf.ase.bananasports.entities;

import java.io.Serializable;
import java.util.List;

public class Partnership implements Entity, Serializable {

	private static final long serialVersionUID = -8437836364459595566L;
	private String uri;
	private boolean partnerConfirmed;
	private List<Subscription> subscriptions;
	private int publicvisible;
	private String owner;
	private String partner;
	private List<String> subscriptionIds;
	private boolean ownerlocal;
	private boolean ownerConfirmed;

	public Partnership() {
	}

	public Partnership(String owner, String partner, String uri,
			boolean ownerlocal) {
		this.owner = owner;
		this.partner = partner;
		this.uri = uri;
		this.ownerlocal = ownerlocal;
	}

	public String getOwner() {
		return this.owner;
	}

	public String getPartner() {
		return this.partner;
	}

	public int getPublicvisible() {
		return this.publicvisible;
	}

	public List<String> getSubscriptionIds() {
		return this.subscriptionIds;
	}

	public List<Subscription> getSubscriptions() {
		return this.subscriptions;
	}

	public String getUri() {
		return this.uri;
	}

	public String getXML() {
		return "<partnership><publicvisible>2</publicvisible></partnership>";
	}

	public boolean hasOwnerConfirmed() {
		return this.ownerConfirmed;
	}

	public boolean hasPartnerConfirmed() {
		return this.partnerConfirmed;
	}

	public boolean isOwnerLocal() {
		return this.ownerlocal;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setOwnerConfirmed(boolean ownerConfirmed) {
		this.ownerConfirmed = ownerConfirmed;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public void setPartnerConfirmed(boolean partnerConfirmed) {
		this.partnerConfirmed = partnerConfirmed;
	}

	public void setPublicvisible(int publicvisible) {
		this.publicvisible = publicvisible;
	}

	public void setSubscriptionIds(List<String> subscriptionsUris) {
		this.subscriptionIds = subscriptionsUris;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
