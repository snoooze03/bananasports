package ch.unibnf.ase.bananasports.entities;

public class CyclingSubscription extends Subscription {

	@Override
	public String getSport() {
		return "Cycling";
	}

}
