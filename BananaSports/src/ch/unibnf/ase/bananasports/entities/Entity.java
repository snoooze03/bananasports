package ch.unibnf.ase.bananasports.entities;

public interface Entity {

	/**
	 * Get xml representation of the object
	 * 
	 * @return
	 */
	public String getXML();
}
