package ch.unibnf.ase.bananasports.entities;

public class CyclingEntry extends Entry implements Entity {

	@Override
	public String getSport() {
		return "Cycling";
	}

	public String getXML() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<entrycycling>");
		buffer.append("<publicvisible>" + this.publicvisible
				+ "</publicvisible>");
		if (this.coursetype != null) {
			buffer.append("<coursetype>" + this.coursetype + "</coursetype>");
		}
		if (this.entrydate != null) {
			buffer.append("<entrydate>" + this.entrydate + "</entrydate>");
		}
		if (this.entrylocation != null) {
			buffer.append("<entrylocation>" + this.entrylocation
					+ "</entrylocation>");
		}
		buffer.append("<numberofrounds>" + this.numberofrounds
				+ "</numberofrounds>");
		if (this.track != null) {
			buffer.append("<track>" + this.track + "</track>");
		}
		buffer.append("</entrycycling>");
		return buffer.toString();
	}
}
