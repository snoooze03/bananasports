package ch.unibnf.ase.bananasports.entities;

public class RunningSubscription extends Subscription {

	@Override
	public String getSport() {
		return "Running";
	}

}
