package ch.unibnf.ase.bananasports.entities;

import java.util.LinkedList;

public abstract class Subscription implements Entity {

	private LinkedList<Integer> serverIds = new LinkedList<Integer>();

	public int publicvisible = 2;

	public abstract String getSport();

	public LinkedList<Integer> getSubscriptionServerIds() {
		return this.serverIds;
	}

	public String getXML() {
		StringBuffer buf = new StringBuffer();
		buf.append("<subscription>");
		buf.append("<publicvisible>" + this.publicvisible + "</publicvisible>");
		buf.append("</subscription>");
		return buf.toString();
	}
}
