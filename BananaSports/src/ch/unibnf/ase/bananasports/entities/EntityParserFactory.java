package ch.unibnf.ase.bananasports.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class EntityParserFactory {

	public static final String USERTAGS = "email;password;publicvisible;realname";

	public static CyclingEntry createCyclingEntry(String xml) {
		CyclingEntry re = new CyclingEntry();
		if (createEntry(xml, re)) {
			return re;
		}
		return null;
	}

	public static boolean createEntry(String xml, Entry entry) {
		try {
			if (xml.contains("publicvisible")) {
				int a = xml.indexOf("<publicvisible>") + 15;
				int b = xml.indexOf("</publicvisible>", a);
				entry.publicvisible = Integer.parseInt(xml.substring(a, b));
			}
			if (xml.contains("coursetype")) {
				int a = xml.indexOf("<coursetype>") + 12;
				int b = xml.indexOf("</coursetype>", a);
				entry.coursetype = (xml.substring(a, b));
			}
			if (xml.contains("numberofrounds")) {
				int a = xml.indexOf("<numberofrounds>") + 16;
				int b = xml.indexOf("</numberofrounds>", a);
				entry.numberofrounds = Integer.parseInt(xml.substring(a, b));
			}
			if (xml.contains("entrylocation")) {
				int a = xml.indexOf("<entrylocation>") + 15;
				int b = xml.indexOf("</entrylocation>", a);
				entry.entrylocation = (xml.substring(a, b));
			}
			if (xml.contains("entrydate")) {
				int a = xml.indexOf("<entrydate>") + 11;
				int b = xml.indexOf("</entrydate>", a);
				entry.entrydate = (xml.substring(a, b));
			}
			if (xml.contains("track")) {
				int a = xml.indexOf("<track>") + 7;
				int b = xml.indexOf("</track>", a);
				entry.track = (xml.substring(a, b));
			}
			if (xml.contains("id=\"")) {
				int a = xml.indexOf("id=\"") + 4;
				int b = xml.indexOf("\"", a);
				entry.serverID = Integer.parseInt(xml.substring(a, b));
			}
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static Partnership createPartnership(Partnership p, String xml) {

		User localUser = (User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER);
		boolean localOwned = localUser.getUsername().equals(p.getOwner());

		int start = xml.indexOf("userconfirmed1=\"") + 16;
		int end = xml.indexOf("\"", start);
		boolean user1Confirmed = "true".equals(xml.substring(start, end));
		start = xml.indexOf("userconfirmed2=\"") + 16;
		end = xml.indexOf("\"", start);
		boolean user2Confirmed = "true".equals(xml.substring(start, end));
			p.setOwnerConfirmed(user1Confirmed);
			p.setPartnerConfirmed(user2Confirmed);
		
		start = xml.indexOf("<publicvisible>") + 15;
		end = xml.indexOf("</publicvisible>", start);
		p.setPublicvisible(Integer.parseInt(xml.substring(start, end)
				.replaceAll("\\s", "")));

		int subscriptionsStart;
		List<String> subscriptionIds = null;
		if ((subscriptionsStart = xml.indexOf("<subscriptions")) != -1) {

			subscriptionIds = new ArrayList<String>();

			start = xml.indexOf("<subscription", subscriptionsStart + 14);
			end = xml.indexOf("/>", start);

			while (start != -1) {
				int a = xml.indexOf("id=\"", start) + 4;
				int b = xml.indexOf("\"", a + 1);

				subscriptionIds.add(xml.substring(a, b));

				start = xml.indexOf("<subscription", end);
				end = xml.indexOf("/>", start);
			}
		}

		p.setSubscriptionIds(subscriptionIds);

		return p;
	}

	public static RunningEntry createRunningEntry(String xml) {
		RunningEntry re = new RunningEntry();
		if (createEntry(xml, re)) {
			return re;
		}
		return null;
	}

	public static Subscription createSubscription(String xml) {
		return null;
	}

	public static User createUser(String xml) {
		// find username
		int start = xml.indexOf("username=\"") + 10;
		int end = xml.indexOf("\"", start);
		String name = xml.substring(start, end);

		User user = new User(name);
		Pattern pattern = Pattern.compile("<email>.+</email>",
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(xml);
		if (matcher.find()) {
			user.setEmail(matcher.group().substring(7,
					matcher.group().length() - 8));
		}

		pattern = Pattern.compile("<realname>.+</realname>",
				Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(xml);
		if (matcher.find()) {
			user.setRealname(matcher.group().substring(10,
					matcher.group().length() - 11));
		}

		pattern = Pattern.compile("<publicvisible>.+</publicvisible>",
				Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(xml);
		if (matcher.find()) {
			user.setPublicvisible(Integer.parseInt(matcher.group().substring(
					15, matcher.group().length() - 16)));
		}
		return user;
	}

	public static Entity parse(Class<? extends Entity> expectedEntity,
			String xml) {
		if (expectedEntity == User.class) {
			return createUser(xml);
		}
		if (expectedEntity == RunningEntry.class) {

		}

		return null;
	}
}
