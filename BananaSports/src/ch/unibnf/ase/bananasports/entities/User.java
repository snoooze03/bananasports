package ch.unibnf.ase.bananasports.entities;

public class User implements Entity {

	private final String username;
	private String email;
	private String realname;
	private int publicvisible;
	private String password;
	private String oldPassword;

	public User(String username) {
		this.username = username;
		this.publicvisible = 2;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof User) {
			User u = (User) o;
			return this.username.equals(u.username);
		}
		return false;
	}

	public String getEmail() {
		return this.email;
	}

	public String getOldPassword() {
		return this.oldPassword;
	}

	public String getPassword() {
		return this.password;
	}

	public int getPublicvisible() {
		return this.publicvisible;
	}

	public String getRealname() {
		return this.realname;
	}

	public String getUsername() {
		return this.username;
	}

	public String getXML() {
		StringBuffer xml = new StringBuffer();
		xml.append("<user>");
		xml.append("<publicvisible>" + this.publicvisible + "</publicvisible>");
		if (this.password != null) {
			xml.append("<password>" + this.password + "</password>");
		}
		if (this.realname != null) {
			xml.append("<realname>" + this.realname + "</realname>");
		}
		if (this.email != null) {
			xml.append("<email>" + this.email + "</email>");
		}
		xml.append("</user>");
		return xml.toString();
	}

	/**
	 * Check if object is valid prior to sending it
	 * 
	 * @return
	 */
	public boolean isValid() {
		boolean valid = (this.username != null);
		valid = valid
				&& this.email != null
				&& this.email.toLowerCase().matches(
						"[a-z0-9.-_]+@[a-z0-9]+\\.[a-z]+");
		valid = valid && this.publicvisible >= 0 && this.publicvisible <= 2;
		valid = valid && this.password != null;
		valid = valid && this.realname != null;

		return valid;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPublicvisible(int publicvisible) {
		this.publicvisible = publicvisible;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	@Override
	public String toString() {
		return this.username + " " + this.email + " " + this.realname + " "
				+ this.password;
	}
}
