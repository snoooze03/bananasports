package ch.unibnf.ase.bananasports.entities;

public class RunningEntry extends Entry {

	/** mandatory field */
	public int courselength = -1;

	@Override
	public String getSport() {
		return "Running";
	}

	public String getXML() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<entryrunning>");
		buffer.append("<publicvisible>" + this.publicvisible
				+ "</publicvisible>");
		buffer.append("<courselength>" + this.courselength + "</courselength>");
		if (this.coursetype != null) {
			buffer.append("<coursetype>" + this.coursetype + "</coursetype>");
		}
		if (this.entrydate != null) {
			buffer.append("<entrydate>" + this.entrydate + "</entrydate>");
		}
		if (this.entrylocation != null) {
			buffer.append("<entrylocation>" + this.entrylocation
					+ "</entrylocation>");
		}
		buffer.append("<numberofrounds>" + this.numberofrounds
				+ "</numberofrounds>");
		if (this.track != null) {
			buffer.append("<track>" + this.track + "</track>");
		}
		buffer.append("</entryrunning>");
		return buffer.toString();
	}
}
