package ch.unibnf.ase.bananasports.entities;

import ch.unibnf.ase.bananasports.util.Helper;

public abstract class Entry implements Entity {
	public int publicvisible = 2;
	/** base64 of gzip. use GetGPX() to get data, setGPX(String gpx) to set */
	protected String track;
	public String coursetype;
	public int numberofrounds = -1;
	public String entrylocation;
	public String entrydate;
	public int serverID = -1;

	public String getCompressedTrack() {
		return this.track;
	}

	public String GetGPX() {
		return Helper.unzipBase64String(this.track);
	}

	/** Return the sport to build the url */
	public abstract String getSport();

	public void setCompressedTrack(String track) {
		this.track = track;
	}

	public void setGPX(String gpx) {
		this.track = Helper.base64ZipString(gpx);
	}
}
