package ch.unibnf.ase.bananasports;

import android.view.View;
import android.widget.BaseExpandableListAdapter;
import ch.unibnf.ase.bananasports.PartnershipParent.Status;
import ch.unibnf.ase.bananasports.connector.RESTConnector;
import ch.unibnf.ase.bananasports.entities.Partnership;
import ch.unibnf.ase.bananasports.entities.User;
import ch.unibnf.ase.bananasports.honeypot.HoneyPot;
import ch.unibnf.ase.bananasports.honeypot.HoneyPotKey;

public class ConfirmParthershipClickListener extends DialogOnClickListener {

	private static final String CONFIRM_TEXT = "Confirm Partnership";
	private static final String USER_QUERY = "Confirm Partnership with %s?";
	private static final String TITLE = "Confirm Partnership";

	public ConfirmParthershipClickListener(Partnership p,
			BaseExpandableListAdapter a, int groupId, View view) {
		super(p, a, groupId, view);
	}

	public void onClick(View v) {
		String query = String.format(USER_QUERY,
				((Partnership) this.entity).getOwner());
		this.createDialog(TITLE, query, CONFIRM_TEXT, v);

	}

	@Override
	protected void onConfirm() {
		RESTConnector.confirmPartnership(
				(User) HoneyPot.getItem(HoneyPotKey.CURRENT_USER),
				((Partnership) this.entity).getOwner(), 3000);
		((PartnershipParent) this.adapter.getGroup(this.groupId))
				.setStatus(Status.ACTIVE);
		((PartnershipsAdatper) this.adapter).populateButton(this.view,
				this.groupId);
		this.adapter.notifyDataSetChanged();

	}

}
