package ch.unibnf.ase.bananasports.playground;

import ch.unibnf.ase.bananasports.garminconnect.GarminConnectHelper;

public class GarminConnectTest {
	private static String gpx = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<gpx version=\"1.1\" creator=\"Garmin Connect\"\r\n  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\"\r\n  xmlns=\"http://www.topografix.com/GPX/1/1\"\r\n  xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"\r\n  xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><trk><trkseg><trkpt lon=\"7.432636666666667\" lat=\"46.95485\"><ele>0.0</ele><time>2012-12-07T22:14:15.000Z</time></trkpt><trkpt lon=\"7.432566666666665\" lat=\"46.95488666666667\"><ele>0.0</ele><time>2012-12-07T22:14:16.000Z</time></trkpt></trkseg></trk></gpx>";

	public static void main(String[] args) {
		// System.out.println(GarminConnectHelper.upload("username", "password",
		// gpx, new String[2]));
		// GarminConnectHelper.getGPX("username", "password", new GarminTrack(
		// "249074597"));
		GarminConnectHelper.GetTracks("username", "password");
	}

}
