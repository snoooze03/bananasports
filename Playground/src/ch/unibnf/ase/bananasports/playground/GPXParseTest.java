package ch.unibnf.ase.bananasports.playground;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import ch.unibnf.ase.bananasports.gpx.GPXDOMParser;
import ch.unibnf.ase.bananasports.gpx.GPXFile;

public class GPXParseTest {

	/**
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws Exception {
		// File file = new File("C:\\Users\\wiedmer\\Desktop\\");
		// GPXReader reader = new GPXReader();
		File file = new File("C:\\Users\\wiedmer\\Desktop\\melchsee.gpx");
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[(int) file.length()];
		fis.read(buffer);
		String gpx = new String(buffer, 0, (int) file.length());
		long start = System.currentTimeMillis();
		// reader.load(file, "melchsee.gpx");

		GPXFile gpxfile = GPXDOMParser.parseGPX("test", gpx);
		long end = System.currentTimeMillis();

		System.out.println("TIME: " + (end - start));
		System.out.println(gpxfile.getTrack().size() + "");

	}
}
