package ch.unibnf.ase.bananasports.playground;

public interface GPXConstants {
	public static final String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<gpx version=\"1.1\" creator=\"Garmin Connect\"\r\n  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\"\r\n  xmlns=\"http://www.topografix.com/GPX/1/1\"\r\n  xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"\r\n  xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
	public static final String END = "</gpx>";

	public static final String TRACK = "<trk>";
	public static final String ENDTRACK = "</trk>";

	public static final String TRACKSEGMENT = "<trkseg>";
	public static final String ENDTRACKSEGMENT = "</trkseg>";

	public static final String TRACKPOINT = "<trkpt lon=\"%s\" lat=\"%s\">";
	public static final String TRACKPOINTBEGIN = "<trkpt";
	public static final String ENDTRACKPOINT = "</trkpt>";

	public static final String ROUTE = "<rte>";
	public static final String ENDROUTE = "</rte>";

	public static final String ROUTEPOINT = "<rtept lon=\"%s\" lat=\"%s\">";
	public static final String ROUTEPOINTBEGIN = "<rtept";
	public static final String ENDROUTEPOINT = "</rtept>";

	public static final String ELEVATION = "<ele>";
	public static final String ENDELEVATION = "</ele>";

	public static final String TIME = "<time>";
	public static final String ENDTIME = "</time>";

	public static final String LAT = "lat=\"";
	public static final String LON = "lon=\"";
	public static final String ENDHYPHEN = "\"";
}
