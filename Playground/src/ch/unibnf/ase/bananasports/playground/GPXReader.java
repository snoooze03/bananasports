package ch.unibnf.ase.bananasports.playground;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ch.unibnf.ase.bananasports.util.ISO8601TimeHelper;

public class GPXReader implements GPXConstants {

	private String gpx;
	private String filename;

	public String getFilename() {
		return this.filename;
	}

	public void load(File directory, String filename) {
		try {
			this.filename = filename;
			File file = new File(directory.getAbsolutePath() + File.separator
					+ filename);
			if (!file.exists()) {
				return;
			}

			StringBuffer buffer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new FileReader(file));
			char[] buf = new char[1024];
			int read;
			while (0 <= (read = reader.read(buf))) {
				buffer.append(buf, 0, read);
				// Log.v("GPXReader", new String(buf, 0, read));
			}
			reader.close();

			this.gpx = buffer.toString();
			parseRoute();

			parseTrack();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void parseRoute() {
		if (!this.gpx.contains(ROUTE) || !this.gpx.contains(ENDROUTE)) {
			// no track in gpx. hence return
			return;
		}
		LinkedList<Vector<Double>> list = new LinkedList<Vector<Double>>();
		int start = this.gpx.indexOf(ROUTEPOINTBEGIN, this.gpx.indexOf(ROUTE));
		int end = this.gpx.indexOf(ENDROUTEPOINT, start);
		while (start != -1 && end != -1) {
			int lon1 = this.gpx.indexOf(LON, start) + LON.length();
			int lon2 = this.gpx.indexOf(ENDHYPHEN, lon1);

			int lat1 = this.gpx.indexOf(LAT, start) + LAT.length();
			int lat2 = this.gpx.indexOf(ENDHYPHEN, lat1);

			Vector v = new Vector<Double>();
			v.add(Double.parseDouble(this.gpx.substring(lat1, lat2)) * 1E6);
			v.add(Double.parseDouble(this.gpx.substring(lon1, lon2)) * 1E6);
			list.add(v);
			// advance
			start = this.gpx.indexOf(ROUTEPOINTBEGIN, end);
			end = this.gpx.indexOf(ENDROUTEPOINT, start);
		}
		System.out.println("parseRoute,  parsed Points: " + list.size());
	}

	private void parseTrack() {
		if (!this.gpx.contains(TRACKSEGMENT)
				|| !this.gpx.contains(ENDTRACKSEGMENT)) {
			// no track in gpx. hence return
			return;
		}

		final ISO8601TimeHelper timeHelper = new ISO8601TimeHelper();

		Executor exec = Executors.newCachedThreadPool();

		LinkedList<Vector<Object>> list = new LinkedList<>();
		int start = this.gpx.indexOf(TRACKPOINTBEGIN,
				this.gpx.indexOf(TRACKSEGMENT));
		int end = this.gpx.indexOf(ENDTRACKPOINT, start);
		while (start != -1 && end != -1) {
			// Log.v("parseTrack", gpx.substring(start, end));
			// long startTime = System.currentTimeMillis();
			// final Location loc = new Location(GPXReader.class.toString());
			final Vector<Object> v = new Vector<>();

			int lon1 = this.gpx.indexOf(LON, start) + LON.length();
			int lon2 = this.gpx.indexOf(ENDHYPHEN, lon1);
			v.add(Double.parseDouble(this.gpx.substring(lon1, lon2)));

			int lat1 = this.gpx.indexOf(LAT, start) + LAT.length();
			int lat2 = this.gpx.indexOf(ENDHYPHEN, lat1);
			v.add(Double.parseDouble(this.gpx.substring(lat1, lat2)));

			// long stopLonLat = System.currentTimeMillis();

			int ele1 = this.gpx.indexOf(ELEVATION, start) + ELEVATION.length();
			int ele2 = this.gpx.indexOf(ENDELEVATION, ele1);
			if (ele1 != -1 && ele2 != -1) {
				v.add(Double.parseDouble(this.gpx.substring(ele1, ele2)));
			}

			// long stopElevation = System.currentTimeMillis();

			final int time1 = this.gpx.indexOf(TIME, start) + TIME.length();
			final int time2 = this.gpx.indexOf(ENDTIME, time1);
			// if (time1 != -1 && time2 != -1) {
			// exec.execute(new Runnable() {
			//
			// @Override
			// public void run() {
			// slow:
			// long start = System.currentTimeMillis();
			// long time = timeHelper
			// .getMilliseconds(GPXReader.this.gpx.substring(
			// time1, time2));
			// faster:
			long time = timeHelper.getMillisecondsThreadSafe(GPXReader.this.gpx
					.substring(time1, time2));
			if (time != -1) {
				v.add(time);
			}
			// Log.v("TimeParse", ""
			// + (System.currentTimeMillis() - start));
			// }
			// });
			// }
			// long stopTime = System.currentTimeMillis();
			list.add(v);

			// advance
			start = this.gpx.indexOf(TRACKPOINTBEGIN, end);
			end = this.gpx.indexOf(ENDTRACKPOINT, start);
			// long stopSearch = System.currentTimeMillis();

			// Log.v("Trackparser: Entry-ParsingTime-total:", "lonlat="
			// + (stopLonLat - startTime) + " elevation="
			// + (stopElevation - stopLonLat) + " time="
			// + (stopTime - stopElevation) + " searchtrackPt="
			// + (stopTime - stopSearch));
		}

		System.out.println("trackParser , parsed points:" + list.size());
	}

}
