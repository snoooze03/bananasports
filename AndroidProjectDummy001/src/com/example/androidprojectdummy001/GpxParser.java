package com.example.androidprojectdummy001;

public class GpxParser {

	private static final String CLOSE_TRACKPOINT = "</trkpt>";
	private static final String START_TRACKPOINT = "<trkpt";
	private static final String START_LONGITUDE = "lon=\"";
	private static final String END_ATTRIBUTE = "\"";
	private static final String START_LATITUDE = "lat=\"";

	public static GpxFile parseXml(String xml) {
		GpxFile file = new GpxFile();
		file.setXml(xml);

		int index = 0;

		while (index < xml.lastIndexOf(CLOSE_TRACKPOINT)) {
			int start = xml.indexOf(START_TRACKPOINT, index);
			int end = xml.indexOf(CLOSE_TRACKPOINT, index)
					+ CLOSE_TRACKPOINT.length();
			String entity = xml.substring(start, end);
			file.getEntries().add(parseEntity(entity));
			index = end;
		}

		return file;
	}

	private static GpxEntry parseEntity(String entity) {

		int start = entity.indexOf(START_LONGITUDE) + START_LONGITUDE.length();
		int end = entity.indexOf(END_ATTRIBUTE, start);
		String longitude = entity.substring(start, end);

		start = entity.indexOf(START_LATITUDE) + START_LONGITUDE.length();
		end = entity.indexOf(END_ATTRIBUTE, start);
		String latitude = entity.substring(start, end);

		return null;

	}

}
