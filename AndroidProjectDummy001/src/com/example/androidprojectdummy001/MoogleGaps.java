package com.example.androidprojectdummy001;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class MoogleGaps extends MapActivity {

	private void parseGpx(File gpx) {
		if (gpx == null) {
			return;
		}
		StringBuffer buf = new StringBuffer();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(gpx));
			String line = null;
			while ((line = reader.readLine()) != null) {
				buf.append(line);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String gpxAsString;
		if (!(gpxAsString = buf.toString()).isEmpty()) {
			int position = 0;
			// TODO
			while ((position = gpxAsString.indexOf("<trkpt", position)) != -1) {
				int temp = gpxAsString.indexOf(">", position);
				String subs = gpxAsString.substring(position, temp);
				Log.v(subs, subs);
				position = temp;
			}
		}

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	protected void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.maps);
		File gpx = null;
		File folder = Environment
				.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE);
		for (File file : folder.listFiles()) {
			if (file.getName().endsWith(".gpx")) {
				gpx = file;
				break;
			}
		}

		parseGpx(gpx);

		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		mapView.setSatellite(true);
	}
}
