package com.example.androidprojectdummy001;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents a gpx file (Training or simple Track)
 * 
 * @author snoooze
 * 
 */
public class GpxFile {

	private List<GpxEntry> entries;

	private String uri;

	private String xml;

	public List<GpxEntry> getEntries() {
		if (this.entries == null) {
			this.entries = new LinkedList<GpxEntry>();
		}
		return this.entries;
	}

	public String getPath() {
		return uri;
	}

	public String getXml() {
		// TODO generate from data if there are performance problems
		return xml;
	}

	public void setPath(String path) {
		this.uri = path;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}
}
