package com.example.androidprojectdummy001;

public interface GpxProvider {

	public GpxFile load();

	public void store(GpxFile gpxFile);

}
