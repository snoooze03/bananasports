package com.example.androidprojectdummy001;

import java.util.Map;

public class GpxEntry {

	private double longitude;

	private double latitude;

	private final Map<GpxFields, String> fields;

	public GpxEntry(Map<GpxFields, String> fields) {
		this.fields = fields;
	}

	public String getField(GpxFields field) {
		return fields.get(field);
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
