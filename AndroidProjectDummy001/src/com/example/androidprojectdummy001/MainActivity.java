package com.example.androidprojectdummy001;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

	public void buttonClick(View view) {
		EditText text = (EditText) findViewById(R.id.editText1);
		Log.v("editview", text.getText().toString());
	}

	public void mapsButtonClick(View view) {
		Intent i = new Intent(getApplication(), MoogleGaps.class);
		startActivity(i);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
