package com.example.androidprojectdummy001;

public enum GpxFields {

	ELEVATION("ele"), TIME("time"), HEART_RATE("gpxtpx:hr");

	private final String fieldName;

	private GpxFields(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldName() {
		return fieldName;
	}

}
